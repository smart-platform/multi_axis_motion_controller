#ifndef _MYCODE_H_
#define _MYCODE_H_

#ifdef DLLDEMO1_EXPORTS
#define EXPORTS_DEMO _declspec( dllexport )
#else
#define EXPORTS_DEMO _declspec(dllimport)
#endif

typedef unsigned short int		McCard_UINT16;
typedef unsigned long			McCard_UINT32;
typedef signed long				McCard_INT32;
typedef signed char				McCard_INT8;
typedef unsigned char			McCard_UINT8;
typedef float					McCard_FP32;
typedef void					McCard_VOID;


enum ErrInfo{
	funResOk			= 0x01,		///< 函数被成功执行
	funResErrAxisId		= 0x02,		///< 下传的轴序号出错
	funResErrOutGrpId	= 0x03,		///< 下传的输出的序号出错
	funResErrInGrpId	= 0x04,		///< 下传的输入的序号出错
	funResErrInterGrpId	= 0x05,		///< 下传的中断输入的序号出错
	funResOpenPortErr	= 0x80,		///< 打开串口失败
	funCallbackOk		= 0x81,		///< 中品回调函数成功
	funCallbackErr		= 0x82,		///< 串口回调函数失败
	funResErr
};

#define MAX_AXIS_NUM		0x06	///< DLL 支持的最大轴数
#define PARA_CNT_PER_AXIS	0x10	///< 每个轴支持的参数个数		

#ifdef MccDllUseCallBack
/*
typedef 定义的是回调函数的原型，下面引出的DLL函数是为回调函数赋值，在定义回调函数时，调用规则为__stdcall 
，在调用该函数时，定义函数的原型也要声明为 __stdcall，否则会报错
*/
typedef McCard_UINT16(__stdcall *pFun_SendFrame)(const McCard_UINT8 *pBuf, McCard_UINT32 dwLen);
typedef McCard_UINT16(__stdcall *pFun_ReceiveFrame)(McCard_UINT32 dwLen, McCard_UINT8 *pBuf);
typedef McCard_VOID(__stdcall *pFun_Delay)(McCard_VOID);
extern "C" EXPORTS_DEMO McCard_VOID MoCtrCard_GetSendFrameCallBack(pFun_SendFrame mfun_CallBaskSendFrame);
extern "C" EXPORTS_DEMO McCard_VOID MoCtrCard_GetDelayCallBack(pFun_Delay mfun_CallBackDelay);
extern "C" EXPORTS_DEMO McCard_VOID MoCtrCard_GetReceiveFrameCallBack(pFun_ReceiveFrame mfun_CallBackReceiveFrame);
#endif

// MCDLL 库函数
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_Initial(McCard_UINT8 ComPort);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_Unload();
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetAxisPos(McCard_UINT8 AxisId, McCard_FP32 ResPos[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetAxisSpd(McCard_UINT8 AxisId, McCard_FP32 ResSpd[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetRunState(McCard_INT32 ResStt[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetAdVal(McCard_UINT8 AxisId, McCard_INT32 ResAd[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetOutState(McCard_UINT8 OutGrpIndx, McCard_UINT32 ResOut[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetInputState(McCard_UINT8 InGrpIndx, McCard_UINT32 ResIn[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetIntInputState(McCard_UINT8 InGrpIndx, McCard_UINT32 ResIn[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetCardVersion(McCard_UINT32 ResVer[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetEncoderVal(McCard_UINT8 AxisId, McCard_INT32 EncoderPos[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_GetBoardHardInfo(McCard_UINT32 HardInfo[]);

extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_SendPara(McCard_UINT8 AxisId, McCard_UINT8 ParaIndx, McCard_FP32 ParaVal);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_MCrlAxisMove(McCard_UINT8 AxisId, McCard_INT8 SpdDir);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_MCrlAxisRelMove(McCard_UINT8 AxisId, McCard_FP32 DistCmnd, McCard_FP32 VCmnd, McCard_FP32 ACmnd);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_MCrlAxisAbsMove(McCard_UINT8 AxisId, McCard_FP32 PosCmnd, McCard_FP32 VCmnd, McCard_FP32 ACmnd);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_SeekZero(McCard_UINT8 AxisId, McCard_FP32 VCmnd, McCard_FP32 ACmnd);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_CancelSeekZero(McCard_UINT8 AxisId);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_ReStartAxisMov(McCard_UINT8 AxisId);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_PauseAxisMov(McCard_UINT8 AxisId);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_StopAxisMov(McCard_UINT8 AxisId, McCard_FP32 fAcc);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_ChangeAxisMovPara(McCard_UINT8 AxisId, McCard_FP32 fVel, McCard_FP32 fAcc);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_EmergencyStopAxisMov(McCard_UINT8 AxisId);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_QuiteMotionControl(McCard_VOID);

extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_SetOutput(McCard_UINT8 OutputIndex, McCard_UINT8 OutputVal);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_SetJoyStickEnable(McCard_UINT8 AxisId, McCard_UINT8 bEnable);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_SetAxisRealtiveInputPole(McCard_UINT8 AxisId, McCard_UINT8 InputType, McCard_UINT8 OpenOrClose);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_ReadPara(McCard_UINT8 AxisId, McCard_UINT8 ParaIndx, McCard_FP32 ParaVal[]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_ResetCoordinate(McCard_UINT8 AxisId, McCard_FP32 PosRest);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_SaveSystemParaToROM(McCard_VOID);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_SetEncoderPos(McCard_UINT8 AxisId, McCard_INT32 EncoderPos);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_RstZ(McCard_UINT8 AxisId);
extern "C" EXPORTS_DEMO McCard_UINT32 MoCtrCard_GetDLLVersion(McCard_VOID);

extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_MCrlGroupAbsMove(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fPos[MAX_AXIS_NUM], McCard_FP32 fSpd);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_MCrlGroupRelMove(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fPos[MAX_AXIS_NUM], McCard_FP32 fSpd);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_MCrlGroupAbsMovePTP(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fPos[MAX_AXIS_NUM], McCard_FP32 fSpd[MAX_AXIS_NUM]);
extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_MCrlGroupRelMovePTP(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fDist[MAX_AXIS_NUM], McCard_FP32 fSpd[MAX_AXIS_NUM]);

extern "C" EXPORTS_DEMO McCard_UINT16 MoCtrCard_Test(McCard_INT32 TestVar[]);

#endif