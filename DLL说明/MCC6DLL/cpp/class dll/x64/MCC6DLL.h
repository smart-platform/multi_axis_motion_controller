#ifndef _MYCODE_H_
#define _MYCODE_H_

#ifdef DLLDEMO1_EXPORTS
#define EXPORTS_DEMO _declspec( dllexport )
#else
#define EXPORTS_DEMO _declspec(dllimport)
#endif

typedef unsigned short int		McCard_UINT16;
typedef unsigned long			McCard_UINT32;
typedef signed long				McCard_INT32;
typedef signed char				McCard_INT8;
typedef unsigned char			McCard_UINT8;
typedef float					McCard_FP32;
typedef void					McCard_VOID;


enum ErrInfo{
	funResOk			= 0x01,		///< 函数被成功执行
	funResErrAxisId		= 0x02,		///< 下传的轴序号出错
	funResErrOutGrpId	= 0x03,		///< 下传的输出的序号出错
	funResErrInGrpId	= 0x04,		///< 下传的输入的序号出错
	funResErrInterGrpId	= 0x05,		///< 下传的中断输入的序号出错
	funResOpenPortErr	= 0x80,		///< 打开串口失败
	funCallbackOk		= 0x81,		///< 中品回调函数成功
	funCallbackErr		= 0x82,		///< 串口回调函数失败
	funResErr
};

#define MAX_AXIS_NUM		0x06	///< DLL 支持的最大轴数
#define PARA_CNT_PER_AXIS	0x10	///< 每个轴支持的参数个数		

#ifdef MccDllUseCallBack
/*
typedef 定义的是回调函数的原型，下面引出的DLL函数是为回调函数赋值，在定义回调函数时，调用规则为__stdcall 
，在调用该函数时，定义函数的原型也要声明为 __stdcall，否则会报错
*/
typedef McCard_UINT16(__stdcall *pFun_SendFrame)(const McCard_UINT8 *pBuf, McCard_UINT32 dwLen);
typedef McCard_UINT16(__stdcall *pFun_ReceiveFrame)(McCard_UINT32 dwLen, McCard_UINT8 *pBuf);
typedef McCard_VOID(__stdcall *pFun_Delay)(McCard_VOID);
extern "C" EXPORTS_DEMO McCard_VOID MoCtrCard_GetSendFrameCallBack(pFun_SendFrame mfun_CallBaskSendFrame);
extern "C" EXPORTS_DEMO McCard_VOID MoCtrCard_GetDelayCallBack(pFun_Delay mfun_CallBackDelay);
extern "C" EXPORTS_DEMO McCard_VOID MoCtrCard_GetReceiveFrameCallBack(pFun_ReceiveFrame mfun_CallBackReceiveFrame);
#endif


class myNetSocket;
class CSerialPort;

class EXPORTS_DEMO MoCtrCard
{
public:
	MoCtrCard();
	~MoCtrCard();

	// MCDLL 库函数
	McCard_UINT16 MoCtrCard_GetAxisPos(McCard_UINT8 AxisId, McCard_FP32 ResPos[]);
	McCard_UINT16 MoCtrCard_GetAxisSpd(McCard_UINT8 AxisId, McCard_FP32 ResSpd[]);
	McCard_UINT16 MoCtrCard_GetRunState(McCard_INT32 ResStt[]);
	McCard_UINT16 MoCtrCard_GetAdVal(McCard_UINT8 AxisId, McCard_INT32 ResAd[]);
	McCard_UINT16 MoCtrCard_GetOutState(McCard_UINT8 OutGrpIndx, McCard_UINT32 ResOut[]);
	McCard_UINT16 MoCtrCard_GetInputState(McCard_UINT8 InGrpIndx, McCard_UINT32 ResIn[]);
	McCard_UINT16 MoCtrCard_GetIntInputState(McCard_UINT8 InGrpIndx, McCard_UINT32 ResIn[]);
	McCard_UINT16 MoCtrCard_GetCardVersion(McCard_UINT32 ResVer[]);
	McCard_UINT16 MoCtrCard_GetEncoderVal(McCard_UINT8 AxisId, McCard_INT32 EncoderPos[]);
	McCard_UINT16 MoCtrCard_GetBoardHardInfo(McCard_UINT32 HardInfo[]);

	McCard_UINT16 MoCtrCard_SendPara(McCard_UINT8 AxisId, McCard_UINT8 ParaIndx, McCard_FP32 ParaVal);
	McCard_UINT16 MoCtrCard_MCrlAxisMove(McCard_UINT8 AxisId, McCard_INT8 SpdDir);
	McCard_UINT16 MoCtrCard_MCrlAxisRelMove(McCard_UINT8 AxisId, McCard_FP32 DistCmnd, McCard_FP32 VCmnd, McCard_FP32 ACmnd);
	McCard_UINT16 MoCtrCard_MCrlAxisAbsMove(McCard_UINT8 AxisId, McCard_FP32 PosCmnd, McCard_FP32 VCmnd, McCard_FP32 ACmnd);
	McCard_UINT16 MoCtrCard_SeekZero(McCard_UINT8 AxisId, McCard_FP32 VCmnd, McCard_FP32 ACmnd);
	McCard_UINT16 MoCtrCard_CancelSeekZero(McCard_UINT8 AxisId);
	McCard_UINT16 MoCtrCard_ReStartAxisMov(McCard_UINT8 AxisId);
	McCard_UINT16 MoCtrCard_PauseAxisMov(McCard_UINT8 AxisId);
	McCard_UINT16 MoCtrCard_StopAxisMov(McCard_UINT8 AxisId, McCard_FP32 fAcc);
	McCard_UINT16 MoCtrCard_ChangeAxisMovPara(McCard_UINT8 AxisId, McCard_FP32 fVel, McCard_FP32 fAcc);
	McCard_UINT16 MoCtrCard_EmergencyStopAxisMov(McCard_UINT8 AxisId);
	McCard_UINT16 MoCtrCard_QuiteMotionControl(McCard_VOID);

	McCard_UINT16 MoCtrCard_SetOutput(McCard_UINT8 OutputIndex, McCard_UINT8 OutputVal);
	McCard_UINT16 MoCtrCard_SetJoyStickEnable(McCard_UINT8 AxisId, McCard_UINT8 bEnable);
	McCard_UINT16 MoCtrCard_SetAxisRealtiveInputPole(McCard_UINT8 AxisId, McCard_UINT8 InputType, McCard_UINT8 OpenOrClose);
	McCard_UINT16 MoCtrCard_EnableCompensateFunction(McCard_UINT8 AxisId);
	McCard_UINT16 MoCtrCard_DisableCompensateFunction(McCard_UINT8 AxisId);
	McCard_UINT16 MoCtrCard_ReadPara(McCard_UINT8 AxisId, McCard_UINT8 ParaIndx, McCard_FP32 ParaVal[]);
	McCard_UINT16 MoCtrCard_ResetCoordinate(McCard_UINT8 AxisId, McCard_FP32 PosRest);
	McCard_UINT16 MoCtrCard_SaveSystemParaToROM(McCard_VOID);
	McCard_UINT16 MoCtrCard_SetEncoderPos(McCard_UINT8 AxisId, McCard_INT32 EncoderPos);
	McCard_UINT16 MoCtrCard_RstZ(McCard_UINT8 AxisId);
	McCard_UINT32 MoCtrCard_GetDLLVersion(McCard_VOID);

	McCard_UINT16 MoCtrCard_MCrlGroupAbsMove(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fPos[MAX_AXIS_NUM], McCard_FP32 fSpd);
	McCard_UINT16 MoCtrCard_MCrlGroupRelMove(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fPos[MAX_AXIS_NUM], McCard_FP32 fSpd);
	McCard_UINT16 MoCtrCard_MCrlGroupAbsMovePTP(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fPos[MAX_AXIS_NUM], McCard_FP32 fSpd[MAX_AXIS_NUM]);
	McCard_UINT16 MoCtrCard_MCrlGroupRelMovePTP(McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fDist[MAX_AXIS_NUM], McCard_FP32 fSpd[MAX_AXIS_NUM]);
	McCard_UINT16 MoCtrCard_StartSimpleHarmonicMove(McCard_UINT8 AxisId, McCard_UINT8 bRelAbs, McCard_FP32 fA, McCard_FP32 fW, McCard_FP32 fPhase, McCard_UINT32 nCycle);
	McCard_UINT16 MoCtrCard_StopSimpleHarmonicMove(McCard_UINT8 AxisId);

	McCard_UINT16 MoCtrCard_Test(McCard_INT32 TestVar[]);

	/// <summary>
	/// 设置控制卡相对运动，在运动过程中可以在指定位置产生脉冲信号
	/// </summary>
	/// <param name="AxisId">轴号，0-X轴，1-Y轴，2-Z轴，3-A轴，4-B轴，5-C轴</param>
	/// <param name="spdDir">运动方向，0-正向，1-负向</param>
	/// <param name="outputIndex">输出序号，0-15</param>
	/// <param name="fDist">运动的距离，正数</param>
	/// <param name="fDistHead">运动的头距离，该距离内是不产生脉冲的，正数</param>
	/// <param name="fDistTail">运动的尾距离，该距离内是不产生脉冲的，正数</param>
	/// <param name="fVel">运动的指令速度</param>
	/// <param name="fDistPulse">产生脉冲的间隔</param>
	/// <param name="nPluseWidth">产生脉冲的宽度</param>
	/// <param name="nMode">脉冲宽度模式，0：周期，1：us</param>
	/// <returns>函数执行是否成功</returns>
	McCard_UINT16 MoCtrCard_MCrlAxisRelMoveAndPulse(McCard_UINT8 AxisId, McCard_UINT8 spdDir, McCard_UINT8 outputIndex, float fDist, float fDistHead, float fDistTail, float fVel, float fDistPulse, McCard_UINT8 nPluseWidth, McCard_UINT8 nMode);
	
	/// <summary>
	/// 设置控制卡相对运动，在运动过程中可以在指定位置产生脉冲信号
	/// </summary>
	/// <param name="AxisId">轴号，0-X轴，1-Y轴，2-Z轴，3-A轴，4-B轴，5-C轴</param>
	/// <returns>函数执行是否成功</returns>
	McCard_UINT16 MoCtrCard_StopAxisRelMoveAndPulse(McCard_UINT8 AxisId);

	/// <summary>
	/// 获取脉冲式运动已经触发的脉冲数
	/// </summary>
	/// <param name="AxisId">轴号，0-X轴，1-Y轴，2-Z轴，3-A轴，4-B轴，5-C轴</param>
	/// <param name="nCnt">已经触发的脉冲数</param>
	/// <returns>函数执行是否成功</returns>
	McCard_UINT16 MoCtrCard_GetPulseMoveCount(McCard_UINT8 AxisId, McCard_INT32 nCnt[]);

	/// <summary>
	/// 获取脉冲式运动触发脉冲数的位置
	/// </summary>
	/// <param name="AxisId">轴号，0-X轴，1-Y轴，2-Z轴，3-A轴，4-B轴，5-C轴</param>
	/// <param name="nCnt">触发脉冲产生的位置</param>
	/// <returns>函数执行是否成功</returns>
	McCard_UINT16 MoCtrCard_GetPulseMovePos(McCard_UINT8 AxisId, McCard_FP32 fPos[]);

	// TCPIP 接口
	McCard_UINT16 MoCtrCard_Net_Initial(McCard_INT8 *pIPAddr, McCard_INT32 nPort);
	McCard_UINT16 MoCtrCard_GetCommState(McCard_INT32 CommStt[]);

	// RS232 接口
	McCard_UINT16 MoCtrCard_Initial(McCard_UINT8 ComPort);
	McCard_UINT16 MoCtrCard_Unload();

private:
	CRITICAL_SECTION   mSemCommSync;    ///< 互斥通讯操作 
	McCard_UINT8 nCmndCnt;				///< 指令计数器，每下发指令自增1
	enum ePortLinkType nPortType;		///< 物理链路类型
	myNetSocket* cTCPSocket;				///< 网络接口对像
	CSerialPort* cRSPort;				///< RS232接口
	void fun_clearSendFrameBuffer(struct stMcCmndType *pMcCmnd);
	void fun_AutoCheckSendBuff(struct stMcCmndType *pInfo);
	McCard_UINT16 fun_AutoCheckGetBuff(const McCard_UINT8 *mBufRec, McCard_UINT32 mLen, McCard_UINT8 **pResInfo);
	McCard_UINT16 fun_SendReceiveAndDecodeFrame(struct stMcCmndType *pStMcCmnd, McCard_UINT32 dwLenWanted, McCard_UINT8 *pResBuf);
	McCard_UINT16 MoCtrCard_FillGCodeCmndByCmndInfo(struct stMcCmndType *pCmnd, McCard_UINT8 bAxisEn[MAX_AXIS_NUM], McCard_FP32 fDist[MAX_AXIS_NUM], McCard_FP32 fSpd[MAX_AXIS_NUM], McCard_FP32 fAcc[MAX_AXIS_NUM]);
	
	// 收发函数
	McCard_UINT16	fun_CallBackSendFrame(const McCard_UINT8 *pBuf, McCard_UINT32 dwLen);
	McCard_UINT16	fun_CallBackReceiveFrame(McCard_UINT32 dwLen, McCard_UINT8 *pBuf);
	McCard_VOID		fun_CallBackDelay(McCard_VOID);
};

#endif