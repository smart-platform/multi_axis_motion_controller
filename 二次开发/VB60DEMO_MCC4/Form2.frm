VERSION 5.00
Begin VB.Form Form2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MotorCtrCard DEMO"
   ClientHeight    =   3795
   ClientLeft      =   4215
   ClientTop       =   3915
   ClientWidth     =   5835
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3795
   ScaleWidth      =   5835
   Begin VB.TextBox txtLog 
      Height          =   1815
      Left            =   0
      MultiLine       =   -1  'True
      TabIndex        =   8
      Text            =   "Form2.frx":0000
      Top             =   1920
      Width           =   5775
   End
   Begin VB.Timer Timer1 
      Left            =   240
      Top             =   120
   End
   Begin VB.TextBox txtComId 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   4800
      TabIndex        =   7
      Text            =   "1"
      Top             =   810
      Width           =   855
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "关闭"
      Height          =   480
      Left            =   4680
      TabIndex        =   5
      Top             =   1320
      Width           =   990
   End
   Begin VB.CommandButton cmdComOpen 
      Caption         =   "打开"
      Height          =   480
      Left            =   3480
      TabIndex        =   4
      Top             =   1320
      Width           =   990
   End
   Begin VB.CommandButton cmdXNeg 
      Caption         =   "XNeg"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   1560
      TabIndex        =   3
      Top             =   1320
      Width           =   990
   End
   Begin VB.CommandButton cmdXPos 
      Caption         =   "xPos"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   240
      TabIndex        =   2
      Top             =   1320
      Width           =   990
   End
   Begin VB.TextBox txtXPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   720
      TabIndex        =   1
      Top             =   720
      Width           =   1815
   End
   Begin VB.Label lblComId 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "串口号："
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3600
      TabIndex        =   6
      Top             =   840
      Width           =   1140
   End
   Begin VB.Shape LinkStt 
      Height          =   495
      Left            =   5040
      Shape           =   3  'Circle
      Top             =   120
      Width           =   615
   End
   Begin VB.Shape Run 
      Height          =   495
      Left            =   4320
      Shape           =   3  'Circle
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblXPos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "X:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   300
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mccard As MotCtrCard

Private Sub PrintfLogInfo(mInfo As String)
    txtLog.Text = txtLog.Text + mInfo + vbCrLf
End Sub

Private Sub cmdClose_Click()
    Call mccard.MoCtrCard_Unload
    
End Sub

Private Sub cmdComOpen_Click()
If mccard.FunResOk = mccard.MoCtrCard_Initial(CByte(txtComId.Text)) Then
    PrintfLogInfo ("联接运动控制卡成功！")
    
    Timer1.Enabled = True
Else
    PrintfLogInfo ("联接运动控制卡失败！")

End If
End Sub

Private Sub cmdXNeg_Click()
If mccard.FunResOk = mccard.MoCtrCard_MCrlAxisRelMove(0, -10#, 0#, 0#) Then
    PrintfLogInfo ("向运动控制卡发指令成功！")

Else
    PrintfLogInfo ("向运动控制卡发指令失败！")
End If
End Sub

Private Sub cmdXPos_Click()
If mccard.FunResOk = mccard.MoCtrCard_MCrlAxisRelMove(0, 10#, 0#, 0#) Then
    PrintfLogInfo ("向运动控制卡发指令成功！")

Else
    PrintfLogInfo ("向运动控制卡发指令失败！")
End If
End Sub

Private Sub Form_Load()
    QieHuan = True
    Run.Shape = 3
    Run.FillStyle = 0
    Run.FillColor = vbBlack
    
    LinkStt.Shape = 3
    LinkStt.FillStyle = 0
    LinkStt.FillColor = vbBlack

    Set mccard = New MotCtrCard
    
    Timer1.Enabled = False
    Timer1.Interval = 300
    
    txtXPos.Item(0).Text = 10000
    txtComId.Text = 1
    txtLog.Text = ""
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Call mccard.MoCtrCard_Unload
End Sub


Private Sub Timer1_Timer()
Dim tmpRunStt(1) As Long
If mccard.FunResOk = mccard.MoCtrCard_GetRunState(tmpRunStt) Then
    If tmpRunStt(0) And &H1 Then
        Run.FillColor = vbRed
    Else
        Run.FillColor = vbBlack
    End If
    LinkStt.FillColor = vbRed
Else
    LinkStt.FillColor = vbBlack
End If

Dim tmpPos(4) As Single
If mccard.FunResOk = mccard.MoCtrCard_GetAxisPos(255, tmpPos) Then
    txtXPos.Item(0).Text = Str(tmpPos(0))
End If

End Sub

