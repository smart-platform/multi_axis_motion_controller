clc;
clear;
%判断当前系统是否支持.net
NET.isNETSupported;
%加载DLL
try
%NET.addAssembly('C:\Program Files\MATLAB\R2010b\bin\MCC4DLL.dll')
NET.addAssembly('C:\Program Files\MATLAB\R2016a\bin\MCC6DLL.dll')
catch ex
   ex 
end

%声明一个对像
classObj = SerialPortLibrary.SPLibClass();
%MoCtrCard_OpenSpDebugForm 是打开调试数据的窗口
%classObj.MoCtrCard_OpenSpDebugForm();

classObj.MoCtrCard_Unload();
%初始化板卡
classObj.MoCtrCard_Initial('COM3')

%在调用函数时，特别是带有返回值的函数，数组的维度要够
ttt = ones(1,4,'int32');
classObj.MoCtrCard_Test(ttt);

%控制轴运动
classObj.MoCtrCard_MCrlAxisRelMove(0, -100, 10, 0.5);

%调用查询指令时，按下面的方面法进
classObj.MoCtrCard_GetAxisPos(255, classObj.gbAxisPos);
classObj.gbAxisPos(1)
classObj.gbAxisPos(2)
classObj.gbAxisPos(3)
classObj.gbAxisPos(4)
classObj.gbAxisPos(5)
classObj.gbAxisPos(6)
%查询运动状态
classObj.MoCtrCard_GetRunState(ttt);	
classObj.gbRunState

%查询输入
classObj.MoCtrCard_GetInputState(0, ttt);	
classObj.gbInput

classObj.MoCtrCard_Unload();