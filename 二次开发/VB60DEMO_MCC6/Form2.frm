VERSION 5.00
Begin VB.Form Form2 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "MotorCtrCard DEMO"
   ClientHeight    =   5265
   ClientLeft      =   4215
   ClientTop       =   3915
   ClientWidth     =   10365
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5265
   ScaleWidth      =   10365
   Begin VB.CommandButton bnStop 
      Caption         =   "停止"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   8760
      TabIndex        =   28
      Top             =   4320
      Width           =   1335
   End
   Begin VB.CommandButton bnChangeObject 
      Caption         =   "切换目标"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   7320
      TabIndex        =   27
      Top             =   4320
      Width           =   1335
   End
   Begin VB.CommandButton Command4 
      Caption         =   "测试2"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   8760
      TabIndex        =   26
      Top             =   3480
      Width           =   1335
   End
   Begin VB.CommandButton Command3 
      Caption         =   "测试1"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Index           =   0
      Left            =   7320
      TabIndex        =   25
      Top             =   3480
      Width           =   1335
   End
   Begin VB.TextBox txTargetPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   6
      Left            =   8640
      TabIndex        =   23
      Text            =   "50"
      Top             =   3000
      Width           =   1455
   End
   Begin VB.TextBox txTargetPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   5
      Left            =   8640
      TabIndex        =   21
      Text            =   "15"
      Top             =   2520
      Width           =   1455
   End
   Begin VB.TextBox txTargetPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   4
      Left            =   8640
      TabIndex        =   19
      Text            =   "12"
      Top             =   2040
      Width           =   1455
   End
   Begin VB.TextBox txTargetPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   3
      Left            =   8640
      TabIndex        =   17
      Text            =   "10"
      Top             =   1560
      Width           =   1455
   End
   Begin VB.TextBox txTargetPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   2
      Left            =   8640
      TabIndex        =   15
      Text            =   "0"
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox txTargetPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   1
      Left            =   8640
      TabIndex        =   13
      Text            =   "-10.00"
      Top             =   600
      Width           =   1455
   End
   Begin VB.TextBox txTargetPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Index           =   0
      Left            =   8640
      TabIndex        =   11
      Text            =   "-12.00"
      Top             =   120
      Width           =   1455
   End
   Begin VB.CommandButton Command2 
      Caption         =   "测试2号卡"
      Height          =   615
      Left            =   3480
      TabIndex        =   10
      Top             =   2040
      Width           =   1095
   End
   Begin VB.CommandButton Command1 
      Caption         =   "轴组直线"
      Height          =   480
      Left            =   120
      TabIndex        =   9
      Top             =   1920
      Width           =   990
   End
   Begin VB.TextBox txtLog 
      Height          =   1815
      Left            =   0
      MultiLine       =   -1  'True
      TabIndex        =   8
      Text            =   "Form2.frx":0000
      Top             =   3120
      Width           =   5775
   End
   Begin VB.Timer Timer1 
      Left            =   240
      Top             =   120
   End
   Begin VB.TextBox txtComId 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   4800
      TabIndex        =   7
      Text            =   "1"
      Top             =   810
      Width           =   855
   End
   Begin VB.CommandButton cmdClose 
      Caption         =   "关闭"
      Height          =   480
      Left            =   4680
      TabIndex        =   5
      Top             =   1320
      Width           =   990
   End
   Begin VB.CommandButton cmdComOpen 
      Caption         =   "打开"
      Height          =   480
      Left            =   3480
      TabIndex        =   4
      Top             =   1320
      Width           =   990
   End
   Begin VB.CommandButton cmdXNeg 
      Caption         =   "XNeg"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   1440
      TabIndex        =   3
      Top             =   1320
      Width           =   990
   End
   Begin VB.CommandButton cmdXPos 
      Caption         =   "xPos"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      Left            =   120
      TabIndex        =   2
      Top             =   1320
      Width           =   990
   End
   Begin VB.TextBox txtXPos 
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   720
      TabIndex        =   1
      Top             =   720
      Width           =   1815
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "关节点7:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   6
      Left            =   7320
      TabIndex        =   24
      Top             =   3060
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "关节点6:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   5
      Left            =   7320
      TabIndex        =   22
      Top             =   2520
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "关节点5:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   4
      Left            =   7320
      TabIndex        =   20
      Top             =   2100
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "关节点4:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   3
      Left            =   7320
      TabIndex        =   18
      Top             =   1560
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "关节点3:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   2
      Left            =   7320
      TabIndex        =   16
      Top             =   1140
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "关节点2:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   1
      Left            =   7320
      TabIndex        =   14
      Top             =   600
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "关节点1:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Index           =   0
      Left            =   7320
      TabIndex        =   12
      Top             =   180
      Width           =   1155
   End
   Begin VB.Label lblComId 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "串口号："
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   3600
      TabIndex        =   6
      Top             =   840
      Width           =   1140
   End
   Begin VB.Shape LinkStt 
      Height          =   495
      Left            =   5040
      Shape           =   3  'Circle
      Top             =   120
      Width           =   615
   End
   Begin VB.Shape Run 
      Height          =   495
      Left            =   4320
      Shape           =   3  'Circle
      Top             =   120
      Width           =   615
   End
   Begin VB.Label lblXPos 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "X:"
      BeginProperty Font 
         Name            =   "宋体"
         Size            =   14.25
         Charset         =   134
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   240
      TabIndex        =   0
      Top             =   720
      Width           =   300
   End
End
Attribute VB_Name = "Form2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim mccard As MotCtrCard
Dim Mccard2 As MotCtrCard

Dim flgTargetPos As Boolean         '位置模式
Dim flgMoveMode As Integer          '运动模式，0无测试，1组合运动，2分立运动

'运动指令变量
Dim gbAxisId(6) As Byte
Dim gbCmndPos(6) As Single
Dim gbCmndSpd(6) As Single
Dim gbCmndAcc(6) As Single
Dim gbRunState(1) As Long
Dim gbCmndPosIndx As Integer

Private Sub PrintfLogInfo(mInfo As String)
    txtLog.Text = txtLog.Text + mInfo + vbCrLf
End Sub

Private Sub bnChangeObject_Click()
If flgTargetPos Then
    txTargetPos(0).Text = "-12"
    txTargetPos(1).Text = "-10"
    txTargetPos(2).Text = "0"
    txTargetPos(3).Text = "10"
    txTargetPos(4).Text = "12"
    txTargetPos(5).Text = "15"
    txTargetPos(6).Text = "50"
    
    flgTargetPos = False
Else
    txTargetPos(0).Text = "0.0"
    txTargetPos(1).Text = "0.02"
    txTargetPos(2).Text = "1.0"
    txTargetPos(3).Text = "8.0"
    txTargetPos(4).Text = "-8.0"
    txTargetPos(5).Text = "-1.0"
    txTargetPos(6).Text = "-0.02"
    
    flgTargetPos = True
End If

End Sub

Private Sub bnStop_Click()
flgMoveMode = 0
mccard.MoCtrCard_EmergencyStopAxisMov (255)
End Sub

Private Sub cmdClose_Click()
    Call mccard.MoCtrCard_Unload
    
End Sub

Private Sub cmdComOpen_Click()
If mccard.FunResOk = mccard.MoCtrCard_Initial(CByte(txtComId.Text)) Then
    PrintfLogInfo ("联接运动控制卡成功！")
    
    Timer1.Enabled = True
Else
    PrintfLogInfo ("联接运动控制卡失败！")

End If


'If mccard.FunResOk = Mccard2.MoCtrCard_Initial("3") Then
'    PrintfLogInfo ("联接运动控制卡2成功！")
'Else
'    PrintfLogInfo ("联接运动控制卡2失败！")
'End If

End Sub

Private Sub cmdXNeg_Click()
If mccard.FunResOk = mccard.MoCtrCard_MCrlAxisRelMove(0, -10#, 0#, 0#) Then
    PrintfLogInfo ("向运动控制卡发指令成功！")

Else
    PrintfLogInfo ("向运动控制卡发指令失败！")
End If
End Sub

Private Sub cmdXPos_Click()
If mccard.FunResOk = mccard.MoCtrCard_MCrlAxisRelMove(0, 10#, 0#, 0#) Then
    PrintfLogInfo ("向运动控制卡发指令成功！")

Else
    PrintfLogInfo ("向运动控制卡发指令失败！")
End If
End Sub

Private Sub Command1_Click()
Dim tmpAxisEn(6) As Byte
Dim tmpPos(6) As Single
Dim tmpSpd(6) As Single
Dim tmpAcc(6) As Single

If mccard.FunResOk = mccard.MoCtrCard_MCrlGroupMoveLineAbsolute(tmpAxisEn, tmpPos, tmpSpd, tmpAcc) Then
    PrintfLogInfo ("发送轴组直线运动指令失败")
Else
    PrintfLogInfo ("发送轴组直线运动指令成功")
End If
    
End Sub

Private Sub Command2_Click()
Dim tmpAxisEn(6) As Byte
Dim tmpPos(6) As Single
Dim tmpSpd(6) As Single
Dim tmpAcc(6) As Single

If mccard.FunResOk = mccard.MoCtrCard_MCrlGroupMoveLineAbsolute(tmpAxisEn, tmpPos, tmpSpd, tmpAcc) Then
    PrintfLogInfo ("发送轴组直线运动指令失败")
Else
    PrintfLogInfo ("发送轴组直线运动指令成功")
End If

If Mccard2.FunResOk = Mccard2.MoCtrCard_MCrlGroupMoveLineAbsolute(tmpAxisEn, tmpPos, tmpSpd, tmpAcc) Then
    PrintfLogInfo ("发送轴组2直线运动指令失败")
Else
    PrintfLogInfo ("发送轴组2直线运动指令成功")
End If

End Sub

Private Sub Command3_Click(Index As Integer)
gbCmndPosIndx = 0
gbCmndPos(0) = CSng(txTargetPos(gbCmndPosIndx).Text)
gbCmndPos(1) = CSng(txTargetPos(gbCmndPosIndx).Text)
gbCmndPos(2) = CSng(txTargetPos(gbCmndPosIndx).Text)
gbCmndPos(3) = CSng(txTargetPos(gbCmndPosIndx).Text)
gbCmndPos(4) = CSng(txTargetPos(gbCmndPosIndx).Text)
gbCmndPos(5) = CSng(txTargetPos(gbCmndPosIndx).Text)

'启动测试
If mccard.FunResErr = mccard.MoCtrCard_GetRunState(gbRunState) Then
    PrintfLogInfo ("获取运动状态失败，终止测试过程！")
    flgMoveMode = 0
Else
    If mccard.FunResErr = mccard.MoCtrCard_MCrlGroupMoveLineAbsolute(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc) Then
        PrintfLogInfo ("发送组合运动失败！")
    Else
        flgMoveMode = 1
    End If
End If

End Sub

Private Sub Command4_Click()
flgMoveMode = 0

PrintfLogInfo ("此种测试方式未实现")
End Sub

Private Sub Form_Load()
    QieHuan = True
    Run.Shape = 3
    Run.FillStyle = 0
    Run.FillColor = vbBlack
    
    LinkStt.Shape = 3
    LinkStt.FillStyle = 0
    LinkStt.FillColor = vbBlack

    Set mccard = New MotCtrCard
    Set Mccard2 = New MotCtrCard
    
    Timer1.Enabled = False
    Timer1.Interval = 300
    
    txtXPos.Item(0).Text = 10000
    txtComId.Text = 1
    txtLog.Text = ""
    
    flgTargetPos = False
    flgMoveMode = 0
    
    '指定指令相关变量
    gbAxisId(0) = 1
    gbAxisId(1) = 1
    gbAxisId(2) = 1
    gbAxisId(3) = 1
    gbAxisId(4) = 1
    gbAxisId(5) = 1
    
    gbCmndSpd(0) = 10#
    gbCmndSpd(1) = 10#
    gbCmndSpd(2) = 10#
    gbCmndSpd(3) = 10#
    gbCmndSpd(4) = 10#
    gbCmndSpd(5) = 10#
    gbCmndSpd(6) = 10#
    
    gbCmndAcc(0) = 1#
    gbCmndAcc(1) = 1#
    gbCmndAcc(2) = 1#
    gbCmndAcc(3) = 1#
    gbCmndAcc(4) = 1#
    gbCmndAcc(5) = 1#
    gbCmndAcc(6) = 1#
End Sub
Private Sub Form_Unload(Cancel As Integer)
    Call mccard.MoCtrCard_Unload
End Sub


Private Sub Timer1_Timer()
Dim tmpRunStt(1) As Long
If mccard.FunResOk = mccard.MoCtrCard_GetRunState(tmpRunStt) Then
    If tmpRunStt(0) And &H1 Then
        Run.FillColor = vbRed
    Else
        Run.FillColor = vbBlack
    End If
    LinkStt.FillColor = vbRed
    
    '自动测试过程程序
    If flgMoveMode > 0 Then
        If (tmpRunStt(0) & &H80000000 <> gbRunState(0) & &H80000000) Then
            '测试方法1
            If (1 = flgMoveMode) Then
                '测试指令号
                gbCmndPosIndx = gbCmndPosIndx + 1
                
                If (gbCmndPosIndx > 6) Then
                    '测试完成，退出测试状态
                    flgMoveMode = 0
                    PrintfLogInfo ("测试完成，退出测试过程！")
                Else
                    ' 赋值指令位置
                    gbCmndPos(0) = CSng(txTargetPos(gbCmndPosIndx).Text)
                    gbCmndPos(1) = CSng(txTargetPos(gbCmndPosIndx).Text)
                    gbCmndPos(2) = CSng(txTargetPos(gbCmndPosIndx).Text)
                    gbCmndPos(3) = CSng(txTargetPos(gbCmndPosIndx).Text)
                    gbCmndPos(4) = CSng(txTargetPos(gbCmndPosIndx).Text)
                    gbCmndPos(5) = CSng(txTargetPos(gbCmndPosIndx).Text)
                    
                    '发送测试指令
                    If mccard.FunResErr = mccard.MoCtrCard_MCrlGroupMoveLineAbsolute(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc) Then
                        PrintfLogInfo ("发送组合运动指令失败，终止测试过程！")
                        flgMoveMode = 0
                    Else
                        flgMoveMode = 1
                    End If
                    
                    '输出测试过程
                    PrintfLogInfo ("当前测试到")
                    PrintfLogInfo (CStr(gbCmndPosIndx))
                    
                End If
            Else  '测试方法2
            
            
            
            
            End If
            
            gbRunState(0) = tmpRunStt(0)
        End If
    End If
    
    
    
    
    
    
Else
    LinkStt.FillColor = vbBlack
End If

Dim tmpPos(4) As Single
If mccard.FunResOk = mccard.MoCtrCard_GetAxisPos(255, tmpPos) Then
    txtXPos.Item(0).Text = Str(tmpPos(0))
End If






End Sub

