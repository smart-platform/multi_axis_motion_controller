VERSION 5.00
Object = "{648A5603-2C6E-101B-82B6-000000000014}#1.1#0"; "MSCOMM32.OCX"
Begin VB.Form Form1 
   Caption         =   "Form1"
   ClientHeight    =   2280
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5880
   LinkTopic       =   "Form1"
   ScaleHeight     =   2280
   ScaleWidth      =   5880
   StartUpPosition =   3  '窗口缺省
   Visible         =   0   'False
   Begin MSCommLib.MSComm MSComm1 
      Left            =   1920
      Top             =   480
      _ExtentX        =   1005
      _ExtentY        =   1005
      _Version        =   393216
      DTREnable       =   -1  'True
   End
   Begin VB.Timer TransferTimer 
      Interval        =   100
      Left            =   1200
      Top             =   480
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Event NotifyTimer()
Event NotifyComm()

Private Sub MSComm1_OnComm()
    Select Case MSComm1.CommEvent
        Case comEvReceive:
            RaiseEvent NotifyComm
        Case Else
        
        End Select
End Sub

Private Sub TransferTimer_Timer()
    RaiseEvent NotifyTimer
End Sub
'MSComm控件提供了两种处理通信的方式：一种为事件驱动方式，该方式相当于一般程序设计中的中断方式。当串口发生事件或错误时，MSComm控件会产生OnComm事件，用户程序可以捕获该事件进行相应处理。本文的两个例子均采用该方式。另一种为查询方式，在用户程序中设计定时或不定时查询 MSComm控件的某些属性是否发生变化，从而确定相应处理。在程序空闲时间较多时可以采用该方式。
'常用属性和方法
'利用MSComm控件实现计算机通信的关键是理解并正确设置MSComm控件众多属性和方法?以下是MSComm控件的常用属性和方法:
'●Commport：设置或返回串口号。
'●Settings：以字符串的形式设置或返回串口通信参数。
'●Portopen：设置或返回串口状态。
'●InputMode：设置或返回接收数据的类型。
'●Inputlen：设置或返回一次从接收缓冲区中读取字节数。
'●InBufferSize：设置或返回接收缓冲区的大小，缺省值为1024字节。
'●InBufferCount：设置或返回接收缓冲区中等待计算机接收的字符数。
'●Input：从接收缓冲区中读取数据并清空该缓冲区，该属性设计时无效，运行时只读。
'●OutBufferSize：设置或返回发送缓冲区的大小，缺省值为512字节。
'●OutBufferCount：设置或返回发送缓冲区中等待计算机发送的字符数。
'●Output：向发送缓冲区发送数据，该属性设计时无效，运行时只读。
'●Rthreshold：该属性为一阀值。当接收缓冲区中字符数达到该值时，MSComm控件设置Commevent属性为ComEvReceive，并产生OnComm事件。用户可在OnComm事件处理程序中进行相应处理。若Rthreshold属性设置为0，则不产生OnComm事件。例如用户希望接收缓冲区中达到一个字符就接收一个字符，可将Rthreshold设置为1。这样接收缓冲区中接收到一个字符，就产生一次OnComm事件。
'●Sthreshold：该属性亦为一阀值。当发送缓冲区中字符数小于该值时，MSComm控件设置Commevent属性为ComEvSend，并产生 OnComm事件。若Sthreshold属性设置为0，则不产生OnComm事件。要特别注意的是仅当发送缓冲区中字符数小于该值的瞬间才产生 OnComm事件，其后就不再产生OnComm事件。例如Sthreshold设置为3，仅当发送缓冲区中字符数从3降为2时，MSComm控件设置 Commevent属性为ComEvSend，同时产生OnComm事件，如发送缓冲区中字符始终为2，则不会再产生OnComm事件。这就避免了发送缓冲区中数据未发送完就反复发生OnComm事件。
'●CommEvent：这是一个非常重要的属性。该属性设计时无效，运行时只读。一旦串口发生通信事件或产生错误，依据产生的事件和错误，MSComm控件为CommEvent属性赋不同的代码，同时产生OnComm事件。用户程序就可在OnComm事件处理程序中针对不同的代码，进行相应的处理。 CommEvent属性的代码、常数及含义参见表1及表2。
'表1 CommEvent通信事件 代码 常数 含义
'1 ComEvReceive 接受到Rthreshold个字符。该事件将持续产生，直到用Input属性从接受缓冲区中读取并删除字符。
'2 ComEvSend 发送缓冲区中数据少于Sthreshold个，说明串口已经发送了一些数据，程序可以用Output属性继续发送数据。
'3 ComEvCTS Clear To Send信号线状态发生变化。
'4 ComEvDSR Data Set Ready信号线状态从1变到0。
'5 ComEvCD Carrier Detect信号线状态发生变化。
'6 comEvRing 检测到振铃信号?
'7 comEvEOF 接受到文件结束符?
'
'
'表2 CommEvent通信错误 代码 常数 含义
'1001 ComEvntBreak 接受到一个中断信号?
'1002 ComEvntCTSTO Clear To Send信号超时。
'1003 ComEvntDSRTO Data Set Ready信号超时。
'1004 ComEvntFrame 帧错误?
'1006 ComEvntOverrun 串口超速?
'1007 ComEvntCDTO 载波检测超时?
'1008 ComEvntRxOver 接受缓冲区溢出，缓冲区中已没有空间。
'1009 ComEvntRxParity 奇偶校验错?
'1010 ComEvntTxFull 发送缓冲区溢出，缓冲区中已没有空间。
'1011 ComEvntDCB 检索串口的设备控制块时发生错误?
