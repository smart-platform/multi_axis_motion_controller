﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.bnMoveLeft = New System.Windows.Forms.Button()
        Me.lbPos = New System.Windows.Forms.Label()
        Me.bnOpenPort = New System.Windows.Forms.Button()
        Me.cbPortList1 = New System.Windows.Forms.ComboBox()
        Me.rbtInfo = New System.Windows.Forms.RichTextBox()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.bnRefresh = New System.Windows.Forms.Button()
        Me.bnCloseAllPort = New System.Windows.Forms.Button()
        Me.bnOpenAllPort = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbPortList4 = New System.Windows.Forms.ComboBox()
        Me.cbPortList3 = New System.Windows.Forms.ComboBox()
        Me.cbPortList2 = New System.Windows.Forms.ComboBox()
        Me.bnResetPos = New System.Windows.Forms.Button()
        Me.bnStopMove = New System.Windows.Forms.Button()
        Me.bnStart = New System.Windows.Forms.Button()
        Me.lbCmndPos = New System.Windows.Forms.ListBox()
        Me.lbRunStt = New System.Windows.Forms.Label()
        Me.bnMoveRight = New System.Windows.Forms.Button()
        Me.cbAxisChoose = New System.Windows.Forms.ComboBox()
        Me.bnClosePort = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.bnCtrlAll = New System.Windows.Forms.Button()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'bnMoveLeft
        '
        Me.bnMoveLeft.Location = New System.Drawing.Point(12, 74)
        Me.bnMoveLeft.Name = "bnMoveLeft"
        Me.bnMoveLeft.Size = New System.Drawing.Size(74, 36)
        Me.bnMoveLeft.TabIndex = 0
        Me.bnMoveLeft.Text = "<-"
        Me.bnMoveLeft.UseVisualStyleBackColor = True
        '
        'lbPos
        '
        Me.lbPos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbPos.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbPos.Location = New System.Drawing.Point(12, 9)
        Me.lbPos.Name = "lbPos"
        Me.lbPos.Size = New System.Drawing.Size(534, 36)
        Me.lbPos.TabIndex = 2
        Me.lbPos.Text = "0.000"
        Me.lbPos.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'bnOpenPort
        '
        Me.bnOpenPort.Location = New System.Drawing.Point(596, 190)
        Me.bnOpenPort.Name = "bnOpenPort"
        Me.bnOpenPort.Size = New System.Drawing.Size(66, 36)
        Me.bnOpenPort.TabIndex = 3
        Me.bnOpenPort.Text = "打开"
        Me.bnOpenPort.UseVisualStyleBackColor = True
        '
        'cbPortList1
        '
        Me.cbPortList1.FormattingEnabled = True
        Me.cbPortList1.Location = New System.Drawing.Point(596, 164)
        Me.cbPortList1.Name = "cbPortList1"
        Me.cbPortList1.Size = New System.Drawing.Size(138, 20)
        Me.cbPortList1.TabIndex = 4
        '
        'rbtInfo
        '
        Me.rbtInfo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.rbtInfo.Location = New System.Drawing.Point(0, 0)
        Me.rbtInfo.Name = "rbtInfo"
        Me.rbtInfo.Size = New System.Drawing.Size(737, 123)
        Me.rbtInfo.TabIndex = 5
        Me.rbtInfo.Text = ""
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnCtrlAll)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnRefresh)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnCloseAllPort)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnOpenAllPort)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cbPortList4)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cbPortList3)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cbPortList2)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnResetPos)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnStopMove)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnStart)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lbCmndPos)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lbRunStt)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnMoveRight)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cbAxisChoose)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnClosePort)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnOpenPort)
        Me.SplitContainer1.Panel1.Controls.Add(Me.bnMoveLeft)
        Me.SplitContainer1.Panel1.Controls.Add(Me.cbPortList1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.lbPos)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.rbtInfo)
        Me.SplitContainer1.Size = New System.Drawing.Size(737, 444)
        Me.SplitContainer1.SplitterDistance = 317
        Me.SplitContainer1.TabIndex = 6
        '
        'bnRefresh
        '
        Me.bnRefresh.Location = New System.Drawing.Point(596, 274)
        Me.bnRefresh.Name = "bnRefresh"
        Me.bnRefresh.Size = New System.Drawing.Size(138, 36)
        Me.bnRefresh.TabIndex = 22
        Me.bnRefresh.Text = "刷新端口"
        Me.bnRefresh.UseVisualStyleBackColor = True
        '
        'bnCloseAllPort
        '
        Me.bnCloseAllPort.Location = New System.Drawing.Point(668, 232)
        Me.bnCloseAllPort.Name = "bnCloseAllPort"
        Me.bnCloseAllPort.Size = New System.Drawing.Size(66, 36)
        Me.bnCloseAllPort.TabIndex = 21
        Me.bnCloseAllPort.Text = "关闭所有"
        Me.bnCloseAllPort.UseVisualStyleBackColor = True
        '
        'bnOpenAllPort
        '
        Me.bnOpenAllPort.Location = New System.Drawing.Point(596, 232)
        Me.bnOpenAllPort.Name = "bnOpenAllPort"
        Me.bnOpenAllPort.Size = New System.Drawing.Size(66, 36)
        Me.bnOpenAllPort.TabIndex = 20
        Me.bnOpenAllPort.Text = "打开所有"
        Me.bnOpenAllPort.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(537, 141)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(53, 12)
        Me.Label4.TabIndex = 19
        Me.Label4.Text = "控制器2:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(537, 115)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(53, 12)
        Me.Label3.TabIndex = 18
        Me.Label3.Text = "控制器3:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(537, 89)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(53, 12)
        Me.Label2.TabIndex = 17
        Me.Label2.Text = "控制器4:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(537, 167)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(53, 12)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "控制器1:"
        '
        'cbPortList4
        '
        Me.cbPortList4.FormattingEnabled = True
        Me.cbPortList4.Location = New System.Drawing.Point(596, 86)
        Me.cbPortList4.Name = "cbPortList4"
        Me.cbPortList4.Size = New System.Drawing.Size(138, 20)
        Me.cbPortList4.TabIndex = 15
        '
        'cbPortList3
        '
        Me.cbPortList3.FormattingEnabled = True
        Me.cbPortList3.Location = New System.Drawing.Point(596, 112)
        Me.cbPortList3.Name = "cbPortList3"
        Me.cbPortList3.Size = New System.Drawing.Size(138, 20)
        Me.cbPortList3.TabIndex = 14
        '
        'cbPortList2
        '
        Me.cbPortList2.FormattingEnabled = True
        Me.cbPortList2.Location = New System.Drawing.Point(596, 138)
        Me.cbPortList2.Name = "cbPortList2"
        Me.cbPortList2.Size = New System.Drawing.Size(138, 20)
        Me.cbPortList2.TabIndex = 13
        '
        'bnResetPos
        '
        Me.bnResetPos.Location = New System.Drawing.Point(389, 103)
        Me.bnResetPos.Name = "bnResetPos"
        Me.bnResetPos.Size = New System.Drawing.Size(74, 36)
        Me.bnResetPos.TabIndex = 12
        Me.bnResetPos.Text = "清零"
        Me.bnResetPos.UseVisualStyleBackColor = True
        '
        'bnStopMove
        '
        Me.bnStopMove.Location = New System.Drawing.Point(389, 61)
        Me.bnStopMove.Name = "bnStopMove"
        Me.bnStopMove.Size = New System.Drawing.Size(74, 36)
        Me.bnStopMove.TabIndex = 11
        Me.bnStopMove.Text = "停止"
        Me.bnStopMove.UseVisualStyleBackColor = True
        '
        'bnStart
        '
        Me.bnStart.Location = New System.Drawing.Point(12, 270)
        Me.bnStart.Name = "bnStart"
        Me.bnStart.Size = New System.Drawing.Size(74, 36)
        Me.bnStart.TabIndex = 10
        Me.bnStart.Text = "测试"
        Me.bnStart.UseVisualStyleBackColor = True
        '
        'lbCmndPos
        '
        Me.lbCmndPos.FormattingEnabled = True
        Me.lbCmndPos.ItemHeight = 12
        Me.lbCmndPos.Items.AddRange(New Object() {"-12", "-10", "0", "10", "12", "15", "50"})
        Me.lbCmndPos.Location = New System.Drawing.Point(12, 116)
        Me.lbCmndPos.Name = "lbCmndPos"
        Me.lbCmndPos.Size = New System.Drawing.Size(154, 148)
        Me.lbCmndPos.TabIndex = 9
        '
        'lbRunStt
        '
        Me.lbRunStt.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbRunStt.Font = New System.Drawing.Font("宋体", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(134, Byte))
        Me.lbRunStt.Location = New System.Drawing.Point(552, 9)
        Me.lbRunStt.Name = "lbRunStt"
        Me.lbRunStt.Size = New System.Drawing.Size(182, 36)
        Me.lbRunStt.TabIndex = 8
        Me.lbRunStt.Text = "0000"
        Me.lbRunStt.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'bnMoveRight
        '
        Me.bnMoveRight.Location = New System.Drawing.Point(92, 74)
        Me.bnMoveRight.Name = "bnMoveRight"
        Me.bnMoveRight.Size = New System.Drawing.Size(74, 36)
        Me.bnMoveRight.TabIndex = 7
        Me.bnMoveRight.Text = "->"
        Me.bnMoveRight.UseVisualStyleBackColor = True
        '
        'cbAxisChoose
        '
        Me.cbAxisChoose.FormattingEnabled = True
        Me.cbAxisChoose.Items.AddRange(New Object() {"X", "Y", "Z", "A", "B", "C", "ALL"})
        Me.cbAxisChoose.Location = New System.Drawing.Point(12, 48)
        Me.cbAxisChoose.Name = "cbAxisChoose"
        Me.cbAxisChoose.Size = New System.Drawing.Size(154, 20)
        Me.cbAxisChoose.TabIndex = 6
        '
        'bnClosePort
        '
        Me.bnClosePort.Location = New System.Drawing.Point(668, 190)
        Me.bnClosePort.Name = "bnClosePort"
        Me.bnClosePort.Size = New System.Drawing.Size(66, 36)
        Me.bnClosePort.TabIndex = 5
        Me.bnClosePort.Text = "关闭"
        Me.bnClosePort.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'bnCtrlAll
        '
        Me.bnCtrlAll.Location = New System.Drawing.Point(389, 270)
        Me.bnCtrlAll.Name = "bnCtrlAll"
        Me.bnCtrlAll.Size = New System.Drawing.Size(74, 36)
        Me.bnCtrlAll.TabIndex = 23
        Me.bnCtrlAll.Text = "控制所有"
        Me.bnCtrlAll.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(737, 444)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents bnMoveLeft As System.Windows.Forms.Button
    Friend WithEvents lbPos As Label
    Friend WithEvents bnOpenPort As Button
    Friend WithEvents cbPortList1 As ComboBox
    Friend WithEvents rbtInfo As RichTextBox
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents Timer1 As Timer
    Friend WithEvents bnClosePort As Button
    Friend WithEvents bnMoveRight As Button
    Friend WithEvents cbAxisChoose As ComboBox
    Friend WithEvents lbRunStt As Label
    Friend WithEvents bnStopMove As Button
    Friend WithEvents bnStart As Button
    Friend WithEvents lbCmndPos As ListBox
    Friend WithEvents bnResetPos As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents cbPortList4 As ComboBox
    Friend WithEvents cbPortList3 As ComboBox
    Friend WithEvents cbPortList2 As ComboBox
    Friend WithEvents bnCloseAllPort As Button
    Friend WithEvents bnOpenAllPort As Button
    Friend WithEvents bnRefresh As Button
    Friend WithEvents bnCtrlAll As Button
End Class
