﻿
Imports SerialPortLibrary
'Imports MCC4DLL.SpDataShow

Public Class Form1
    'Dim teset As New MCC4DLL.SpDataShow
    'Public teset = New MCC4DLL.SpDataShow

    'Dim dd As SPLibClass
    '新建一个控制卡变量
    Public MccCard1 = New SPLibClass
    Public MccCard2 = New SPLibClass
    Public MccCard3 = New SPLibClass
    Public MccCard4 = New SPLibClass

    '运动指令变量
    Dim gbAxisId(6) As Byte
    Dim gbCmndPos(6) As Single
    Dim gbCmndSpd(6) As Single
    Dim gbCmndAcc(6) As Single
    Dim gbRunState(1) As UInt32
    Dim gbCmndPosIndx As Integer
    Dim flgMoveMode As Integer          '运动模式，0无测试，1组合运动，2分立运动

    Private Sub fun_PrintLogInfo(ByVal sInfo As String)
        rbtInfo.Text += DateTime.Now.ToString + vbTab + sInfo + vbNewLine
    End Sub

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim portNames() As String = System.IO.Ports.SerialPort.GetPortNames()
        cbPortList1.DataSource = portNames
        cbPortList2.DataSource = portNames
        cbPortList3.DataSource = portNames
        cbPortList4.DataSource = portNames

        Timer1.Interval = 200
        Timer1.Enabled = True

        cbAxisChoose.SelectedIndex = 0

        '指定指令相关变量
        gbAxisId(0) = 1
        gbAxisId(1) = 1
        gbAxisId(2) = 1
        gbAxisId(3) = 1
        gbAxisId(4) = 1
        gbAxisId(5) = 1

        gbCmndSpd(0) = 10.0#
        gbCmndSpd(1) = 10.0#
        gbCmndSpd(2) = 10.0#
        gbCmndSpd(3) = 10.0#
        gbCmndSpd(4) = 10.0#
        gbCmndSpd(5) = 10.0#
        gbCmndSpd(6) = 10.0#

        gbCmndAcc(0) = 1.0#
        gbCmndAcc(1) = 1.0#
        gbCmndAcc(2) = 1.0#
        gbCmndAcc(3) = 1.0#
        gbCmndAcc(4) = 1.0#
        gbCmndAcc(5) = 1.0#
        gbCmndAcc(6) = 1.0#

        flgMoveMode = 0
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles bnMoveLeft.Click
        Dim tmpAxis As Byte
        tmpAxis = cbAxisChoose.SelectedIndex
        If tmpAxis > 5 Then
            gbCmndPos(0) = -10.0
            gbCmndPos(1) = -10.0
            gbCmndPos(2) = -10.0
            gbCmndPos(3) = -10.0
            gbCmndPos(4) = -10.0
            gbCmndPos(5) = -10.0

            If (MccCard1.FunResErr = MccCard1.MoCtrCard_MCrlGroupRelMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc)) Then
                fun_PrintLogInfo("发送运动指令失败!")
            End If
        Else

            If (MccCard1.FunResErr = MccCard1.MoCtrCard_MCrlAxisRelMove(tmpAxis, -10.0, 10.0, 0.5)) Then
                fun_PrintLogInfo("发送运动指令失败!")
            End If
        End If
    End Sub

    Private Sub Form1_FormClosed(sender As Object, e As FormClosedEventArgs) Handles MyBase.FormClosed
        Call MccCard1.MoCtrCard_Unload()
    End Sub

    Private Sub bnOpenPort_Click(sender As Object, e As EventArgs) Handles bnOpenPort.Click
        If (MccCard1.FunResErr = MccCard1.MoCtrCard_Initial(cbPortList1.Text)) Then
            fun_PrintLogInfo("初始化板卡失败！")
        End If

    End Sub

    Private Sub bnClosePort_Click(sender As Object, e As EventArgs) Handles bnClosePort.Click
        If (MccCard1.FunResErr = MccCard1.MoCtrCard_Unload()) Then
            fun_PrintLogInfo("关闭板卡失败！")
        End If
    End Sub

    Private Sub cbPortList_MouseClick(sender As Object, e As MouseEventArgs) Handles cbPortList1.MouseClick
        Dim portNames() As String = System.IO.Ports.SerialPort.GetPortNames()
        cbPortList1.DataSource = portNames
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        Dim tmpRunStt(1) As UInt32
        If MccCard1.FunResOK = MccCard1.MoCtrCard_GetRunState(tmpRunStt) Then
            lbRunStt.Text = tmpRunStt(0).ToString("x")
        End If


        If MccCard1.CommLinkEnable Then
            Dim tmpPos(MccCard1.MaxAxisNum) As Single

            '查位置
            If (MccCard1.FunResOK = MccCard1.MoCtrCard_GetAxisPos(&HFF, tmpPos)) Then
                Dim tmpI As Int16
                lbPos.Text = ""
                For tmpI = 0 To MccCard1.MaxAxisNum - 1
                    lbPos.Text += String.Format("{0:d1} :{1:000.000}  ", tmpI, tmpPos(tmpI))

                Next
            End If

            '自动测试过程程序
            If flgMoveMode > 0 Then
                If (tmpRunStt(0) & &H80000000 <> gbRunState(0) & &H80000000) Then
                    '测试方法1
                    '测试指令号
                    gbCmndPosIndx = gbCmndPosIndx + 1

                    If (gbCmndPosIndx > 6) Then
                        '测试完成，退出测试状态
                        fun_PrintLogInfo("测试完成，退出测试过程！")
                        flgMoveMode = 0
                    Else
                        ' 赋值指令位置
                        gbCmndPos(0) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
                        gbCmndPos(1) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
                        gbCmndPos(2) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
                        gbCmndPos(3) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
                        gbCmndPos(4) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
                        gbCmndPos(5) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())

                        '发送测试指令
                        If MccCard1.FunResErr = MccCard1.MoCtrCard_MCrlGroupAbsMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc) Then
                            fun_PrintLogInfo("发送组合运动指令失败，终止测试过程！")
                            flgMoveMode = 0
                        End If

                        '输出测试过程
                        fun_PrintLogInfo(String.Format("当前测试到 {0} 行!", gbCmndPosIndx))

                    End If

                    gbRunState(0) = tmpRunStt(0)
                End If
            End If
        End If
    End Sub

    Private Sub bnMoveRight_Click(sender As Object, e As EventArgs) Handles bnMoveRight.Click
        Dim tmpAxis As Byte
        tmpAxis = cbAxisChoose.SelectedIndex
        If tmpAxis > 5 Then
            gbCmndPos(0) = 10.0
            gbCmndPos(1) = 10.0
            gbCmndPos(2) = 10.0
            gbCmndPos(3) = 10.0
            gbCmndPos(4) = 10.0
            gbCmndPos(5) = 10.0

            If (MccCard1.FunResErr = MccCard1.MoCtrCard_MCrlGroupRelMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc)) Then
                fun_PrintLogInfo("发送运动指令失败!")
            End If
        Else

            If (MccCard1.FunResErr = MccCard1.MoCtrCard_MCrlAxisRelMove(tmpAxis, 10.0, 10.0, 0.5)) Then
                fun_PrintLogInfo("发送运动指令失败!")
            End If
        End If
    End Sub

    Private Sub bnStart_Click(sender As Object, e As EventArgs) Handles bnStart.Click
        gbCmndPosIndx = 0
        gbCmndPos(0) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
        gbCmndPos(1) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
        gbCmndPos(2) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
        gbCmndPos(3) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
        gbCmndPos(4) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())
        gbCmndPos(5) = Single.Parse(lbCmndPos.Items.Item(gbCmndPosIndx).ToString())

        '启动测试
        If MccCard1.FunResErr = MccCard1.MoCtrCard_GetRunState(gbRunState) Then
            fun_PrintLogInfo("获取运动状态失败，终止测试过程！")
        Else
            If MccCard1.FunResErr = MccCard1.MoCtrCard_MCrlGroupAbsMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc) Then
                fun_PrintLogInfo("发送组合运动失败！")
            Else
                flgMoveMode = 1
            End If
        End If
    End Sub

    Private Sub bnResetPos_Click(sender As Object, e As EventArgs) Handles bnResetPos.Click
        '复位
        If MccCard1.FunResErr = MccCard1.MoCtrCard_ResetCoordinate(&HFF, 0.0F) Then
            fun_PrintLogInfo("设置轴位置失败！")
        End If
    End Sub

    Private Sub bnRefresh_Click(sender As Object, e As EventArgs) Handles bnRefresh.Click
        Dim portNames() As String = System.IO.Ports.SerialPort.GetPortNames()
        cbPortList1.DataSource = portNames
        cbPortList2.DataSource = portNames
        cbPortList3.DataSource = portNames
        cbPortList4.DataSource = portNames
    End Sub

    Private Sub bnOpenAllPort_Click(sender As Object, e As EventArgs) Handles bnOpenAllPort.Click
        If (MccCard1.FunResErr = MccCard1.MoCtrCard_Initial(cbPortList1.Text)) Then
            fun_PrintLogInfo("初始化板卡失败！")
        End If

        If (MccCard2.FunResErr = MccCard1.MoCtrCard_Initial(cbPortList2.Text)) Then
            fun_PrintLogInfo("初始化板卡失败！")
        End If

        If (MccCard3.FunResErr = MccCard1.MoCtrCard_Initial(cbPortList3.Text)) Then
            fun_PrintLogInfo("初始化板卡失败！")
        End If

        If (MccCard4.FunResErr = MccCard1.MoCtrCard_Initial(cbPortList4.Text)) Then
            fun_PrintLogInfo("初始化板卡失败！")
        End If
    End Sub

    Private Sub bnCloseAllPort_Click(sender As Object, e As EventArgs) Handles bnCloseAllPort.Click
        If (MccCard1.FunResErr = MccCard1.MoCtrCard_Unload()) Then
            fun_PrintLogInfo("关闭板卡失败！")
        End If

        If (MccCard2.FunResErr = MccCard2.MoCtrCard_Unload()) Then
            fun_PrintLogInfo("关闭板卡失败！")
        End If

        If (MccCard3.FunResErr = MccCard3.MoCtrCard_Unload()) Then
            fun_PrintLogInfo("关闭板卡失败！")
        End If

        If (MccCard4.FunResErr = MccCard4.MoCtrCard_Unload()) Then
            fun_PrintLogInfo("关闭板卡失败！")
        End If
    End Sub

    Private Sub bnCtrlAll_Click(sender As Object, e As EventArgs) Handles bnCtrlAll.Click
        gbCmndPos(0) = 10.0
        gbCmndPos(1) = 10.0
        gbCmndPos(2) = 10.0
        gbCmndPos(3) = 10.0
        gbCmndPos(4) = 10.0
        gbCmndPos(5) = 10.0

        Call MccCard1.MoCtrCard_MCrlGroupRelMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc)
        Call MccCard2.MoCtrCard_MCrlGroupRelMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc)
        Call MccCard3.MoCtrCard_MCrlGroupRelMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc)
        Call MccCard4.MoCtrCard_MCrlGroupRelMove(gbAxisId, gbCmndPos, gbCmndSpd, gbCmndAcc)
    End Sub
End Class
