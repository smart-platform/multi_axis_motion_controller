#ifndef __SPI_H
#define __SPI_H
#include "sys.h"

#define	SPI_FLASH_CS PBout(6)	///< 位带操作，FLASH片选信号，低有效	

///< void SPI1_Init(void)
///<	初始化SPI1用到的IO端口
///< @param[I]	无
///< @param[O]	无
///< @return	无
///< @see
///< @note		将SPI1重映射到PB3、4、5、PA15端口上；PA15不用做SPI片选信号；利用PB6进行片选
void SPI1_Init(void); 

///< void SPI1_ReadWriteByte(void)
///<	通过发送一个数据，从SPI设备上读取一个数据
///< @param[I]	TxData		发出的数据
///< @param[O]	无
///< @return	uint16_t	SPI设备上的数据
///< @see
///< @note
uint8_t SPI1_ReadWriteByte(uint8_t TxData);

#endif
