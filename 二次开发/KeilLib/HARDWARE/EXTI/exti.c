#include "exti.h"
#include "led.h"
#include "key.h"
#include "delay.h"
#include "usart.h"

void EXTINT_Init(void){
 	EXTI_InitTypeDef EXTI_InitStructure;
 	NVIC_InitTypeDef NVIC_InitStructure;

  	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE);	//使能复用功能时钟	
	
	//GPIOA.8	  中断线以及中断初始化配置 上升沿触发 
 	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSource8); 	
	
  	EXTI_InitStructure.EXTI_Line=EXTI_Line8;	//GPIOA.8
  	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;	
  	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;	//上升沿触发
  	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  	EXTI_Init(&EXTI_InitStructure);	 	//根据EXTI_InitStruct中指定的参数初始化外设EXTI寄存器	
	
  	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;				//使能PA8所在的外部中断通道
  	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;	//抢占优先级2，
  	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;			//子优先级3
  	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;					//使能外部中断通道
  	NVIC_Init(&NVIC_InitStructure); 	
}

void EXTI9_5_IRQHandler(void){
	if(EXTI_GetITStatus(EXTI_Line8) != RESET){	//判断是否真的发生外部中断
		//user code
		EXTI_ClearITPendingBit(EXTI_Line8);  //清除LINE4上的中断标志位  			
	}

}



