 #include "adc.h"
															   
void  Adc_Init(void)
{ 	
	GPIO_InitTypeDef  GPIO_InitStructure;
	ADC_InitTypeDef  ADC_InitStructure; 
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC| RCC_APB2Periph_ADC1 | RCC_APB2Periph_AFIO, ENABLE );   
	//RCC_APB2Periph_GPIOx,x=GPIOx
	RCC_ADCCLKConfig(RCC_PCLK2_Div6);   //72M/6=12,ADC最大时间不能超过14M    
	//PA0--ADC_IN0  PA1--ADC_IN1  PA2--ADC_IN2  PA3--ADC_IN3  PA4--ADC_IN4  PA5--ADC_IN5  PA6--ADC_IN6  PA7--ADC_IN7  
	//PB0--ADC_IN8  PB1--ADC_IN9  PC0--ADC_IN10  PC1--ADC_IN11  PC2--ADC_IN12  PC3--ADC_IN13  PC4--ADC_IN14  PC5--ADC_IN15                   
	//PA0~3  4路作为模拟通道输入引脚                         
	GPIO_InitStructure.GPIO_Pin		= GPIO_Pin_0|GPIO_Pin_1|GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5;	 //PC0/1/2/3/4/5 作为模拟通道输入引脚
	GPIO_InitStructure.GPIO_Mode	= GPIO_Mode_AIN;                
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	//------------------------------------ADC设置--------------------------------------------------------
	ADC_DeInit(ADC1);														//将外设 ADC1 的全部寄存器重设为缺省值
	ADC_InitStructure.ADC_Mode					= ADC_Mode_Independent;		//ADC工作模式:ADC1和ADC2工作在独立模式
	ADC_InitStructure.ADC_ScanConvMode			= ENABLE;					//多信道扫描模式
	ADC_InitStructure.ADC_ContinuousConvMode	= ENABLE;					//模数转换工作在连续转换模式
	ADC_InitStructure.ADC_ExternalTrigConv		= ADC_ExternalTrigConv_None;//外部触发转换关闭
	ADC_InitStructure.ADC_DataAlign				= ADC_DataAlign_Right;		//ADC数据右对齐
	ADC_InitStructure.ADC_NbrOfChannel			= 6;						//此处开6个信道（可开的为1~16）
	ADC_Init(ADC1, &ADC_InitStructure);										//根据ADC_InitStruct中指定的参数初始化外设ADCx的寄存器

	//ADC常规信道配置
	//ADC1,ADC通道x,规则采样顺序值为y,采样时间为239.5周期
	ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 1, ADC_SampleTime_239Cycles5 );                
	ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 2, ADC_SampleTime_239Cycles5 );
	ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 3, ADC_SampleTime_239Cycles5 );                
	ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 4, ADC_SampleTime_239Cycles5 );
	ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 5, ADC_SampleTime_239Cycles5 );
	ADC_RegularChannelConfig(ADC1, ADC_Channel_15, 6, ADC_SampleTime_239Cycles5 );

	// 开启ADC的DMA支持（要实现DMA功能，还需独立配置DMA通道等参数）
	ADC_DMACmd(ADC1, ENABLE);						//使能ADC1的DMA传输         
	ADC_Cmd(ADC1, ENABLE);							//使能指定的ADC1
	ADC_ResetCalibration(ADC1);						//复位指定的ADC1的校准寄存器
	while(ADC_GetResetCalibrationStatus(ADC1));     //获取ADC1复位校准寄存器的状态,设置状态则等待
	ADC_StartCalibration(ADC1);						//开始指定ADC1的校准状态
	while(ADC_GetCalibrationStatus(ADC1));          //获取指定ADC1的校准程序,设置状态则等待
}				  
