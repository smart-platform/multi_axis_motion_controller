#ifndef __LCD19264_H
#define __LCD19264_H		

#include "sys.h"
//#include "stdlib.h"

#define VENDER_WEINA	1				// 微纳光科
#define VENDER_XINHE	2				// 信和光电
#define VENDER_JIANGYUN	3			// 江云光电
#define VENDER_CUSTOM	4				// 自定义
#define VENDER_ZHONGTIANGUANGZHENG	5				// 中天光正
#define VENDER_TIANYUEZHONGDA				6				// 天悦众达
#define VENDER_ZHONGCHUANGJIASHENG	7				// 中创嘉恒

#define VENDERID		VENDER_JIANGYUN

//-----------------LCD19264端口定义----------------  
/*
DATA[7:0]:PF[7:0]
RS:PE5
E:PC13
CS[3:1]:PE[2:4]s
RW:PE6
*/

#define LCD19264_E_Clr()	GPIO_ResetBits(GPIOB, GPIO_Pin_1);			//拉低E脚
#define LCD19264_E_Set()	GPIO_SetBits(GPIOB, GPIO_Pin_1);			//拉高E脚

#define LCD19264_RW_Clr()	__NOP;										//拉高RW
#define LCD19264_RW_Set()	__NOP;;										//拉高RW

#define LCD19264_RS_Clr()	GPIO_ResetBits(GPIOB, GPIO_Pin_0);			//拉低RS脚
#define LCD19264_RS_Set()	GPIO_SetBits(GPIOB, GPIO_Pin_0);			//拉高RS脚

#define LCD19264_CS1_Clr()	GPIO_ResetBits(GPIOB, GPIO_Pin_2);			//拉低CS1脚
#define LCD19264_CS1_Set()	GPIO_SetBits(GPIOB, GPIO_Pin_2);			//拉高CS1脚

#define LCD19264_CS2_Clr()	GPIO_ResetBits(GPIOB, GPIO_Pin_12);			//拉低CS2脚
#define LCD19264_CS2_Set()	GPIO_SetBits(GPIOB, GPIO_Pin_12);			//拉高CS2脚

#define LCD19264_CS3_Clr()	GPIO_ResetBits(GPIOB, GPIO_Pin_13);			//拉低CS3脚
#define LCD19264_CS3_Set()	GPIO_SetBits(GPIOB, GPIO_Pin_13);			//拉高CS3脚

//下面这个宏很重要！ 在同一排GPIO中既有输入又有输出的时候需要注意这个问题
//#define BITS_IN_OUT_MASK	0xC000		//高两位作为上拉输入 时钟保持高电平

#define LCD19264_CMD  0	//写命令
#define LCD19264_DATA 1	//写数据

#define FONT_CHN_SIZE 16	//16*16汉字
#define FONT_CHR_SIZE 8		//16*08字符

void LCD19264_DATAOUT(uint8_t x);

void LCD19264_Init(void);
void LCD19264_Clear(uint8_t clearbyte);
void Choose_IC(uint8_t n);
void Un_Choose_IC(void);
void LCD19264_WR_Byte(uint8_t dat, uint8_t cmd);

uint8_t LCD19264_ReadPoint(uint8_t x, uint8_t y);
void LCD19264_DrawPoint(uint8_t x, uint8_t y, uint8_t mode);
void LCD19264_Refresh_Gram(void);
void LCD19264_startup_screen(void);
uint8_t LCD19264_ReadPoint(uint8_t x, uint8_t y);

extern const unsigned char startup_screen_code[1536];
#endif  
