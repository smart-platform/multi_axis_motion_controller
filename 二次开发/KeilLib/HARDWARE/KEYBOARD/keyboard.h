#ifndef __KEYBOARD_H
#define __KEYBOARD_H

#include "sys.h"


//定义符号键键值
// 定义按键
#define 	KeyX		10
#define		KeyY		20
#define		KeyZ    	30
#define		KeyT    	40
#define		KeyCon  	44
#define 	Key1		11
#define 	Key2    	12
#define  	Key3		13
#define 	Key4    	14
#define		Key5		21
#define		Key6		22
#define		Key7		23
#define		Key8		24
#define		Key9		31
#define		Key0		32
#define		KeyDot		33
#define		KeyClear	34
#define		KeyNP		41
#define		KeySave		42
#define 	KeyZero		43
#define		KeyUp		3
#define		KeyDwn		1
#define		KeyStartStop	2
#define		KeyLft		4
#define		KeyRgh		0

#define	SYSLED					PAout(8)	//PA8

#ifndef NULL
#define NULL		0
#endif

void KeyboardInit(void);
u8 KeyHandle(void);		//主函数中调用
void KeyScan(void);			//定时中断函数中调用
void LED_Init(void);		//LED IO初始化
void JoyStick_Init(void);	//joyStick_en初始化

#endif



