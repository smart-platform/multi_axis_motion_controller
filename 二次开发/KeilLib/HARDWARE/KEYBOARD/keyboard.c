#include "stm32f10x.h"
#include "keyboard.h"
#include "sys.h" 
#include "delay.h"

//5*5按键输入
#define KEY_IN_0	GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_6)
#define KEY_IN_1	GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_7)
#define KEY_IN_2	GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_8)
#define KEY_IN_3	GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_9)
#define KEY_IN_4	GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_10)
#define KEY_IN_5	GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_11)
#define KEY_IN_6	GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_12)

//5*5按键输出
#define SET_KEY_OUT_0	GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_SET)
#define SET_KEY_OUT_1	GPIO_WriteBit(GPIOE, GPIO_Pin_2, Bit_SET)
#define SET_KEY_OUT_2	GPIO_WriteBit(GPIOE, GPIO_Pin_3, Bit_SET)
#define SET_KEY_OUT_3	GPIO_WriteBit(GPIOE, GPIO_Pin_4, Bit_SET)
#define SET_KEY_OUT_4	GPIO_WriteBit(GPIOE, GPIO_Pin_5, Bit_SET)
#define SET_KEY_OUT_5	GPIO_WriteBit(GPIOE, GPIO_Pin_6, Bit_SET)

#define CLR_KEY_OUT_0	GPIO_WriteBit(GPIOB, GPIO_Pin_8, Bit_RESET)
#define CLR_KEY_OUT_1	GPIO_WriteBit(GPIOE, GPIO_Pin_2, Bit_RESET)
#define CLR_KEY_OUT_2	GPIO_WriteBit(GPIOE, GPIO_Pin_3, Bit_RESET)
#define CLR_KEY_OUT_3	GPIO_WriteBit(GPIOE, GPIO_Pin_4, Bit_RESET)
#define CLR_KEY_OUT_4	GPIO_WriteBit(GPIOE, GPIO_Pin_5, Bit_RESET)
#define CLR_KEY_OUT_5	GPIO_WriteBit(GPIOE, GPIO_Pin_6, Bit_RESET)

//按键消抖掩码 0000,1111 共耗时4*5 = 20ms
#define KEY_MASK	0x0F

const uint8_t KeyCodeMap[5][5] = {
	{KeyRgh, KeyX, KeyY, KeyZ, KeyT}, 
	{KeyDwn, Key1, Key5, Key9, KeyNP}, 
	{KeyStartStop, Key2, Key6, Key0, KeySave},
	{KeyUp, Key3, Key7, KeyDot, KeyZero},
	{KeyLft, Key4, Key8, KeyClear, KeyCon}
};
/*
const uint8_t KeyCodeMap[5][5] = {
	{'A', 'B', 'C', 'D', 'E'}, 
	{'7', '8', '9', 'F', 'G'}, 
	{'4', '5', '6', 'H', 'I'},
	{'1', '2', '3', 'J', 'K'},
	{'L', '0', 'M', 'N', 'O'}
};
B	8	5	2	0
C	9	6	3	M		1
					L	4	A
D	F	H	J	N		7
E	G	I	K	O
*/
/*
const uint8_t KeyCodeMap[5][5] = {
	{0,1,2,3,4},
	{5,6,7,8,9},
	{10,11,12,13,14},
	{15,16,17,18,19},
	{20,21,22,23,24}
};
*/
uint8_t KeySta[5][5] = {		//保存按键的当前状态
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF},
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
	};

//矩阵键盘初始化
void KeyboardInit(void){
 	GPIO_InitTypeDef GPIO_InitStructure;
	
	//初始化KEY_IN0-->PC6  	KEY_IN1-->PC7  	KEY_IN2-->PC8  	KEY_IN3-->PC9  	KEY_IN4-->PC10 	KEY_IN5-->PC11为上拉输入
	//初始化KEY_OUT0-->PB8  KEY_OUT1-->PE2  KEY_OUT2-->PE3  KEY_OUT3-->PE4  KEY_OUT4-->PE5  KEY_OUT5-->PE6
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	//GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE); 

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC|RCC_APB2Periph_GPIOE|RCC_APB2Periph_GPIOB, ENABLE);			//使能PORTB PORTC PORTE

	//设置输入按键
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9|GPIO_Pin_10|GPIO_Pin_11;		//PC6-PC11上拉输入
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;															//设置成上拉输入
 	GPIO_Init(GPIOC, &GPIO_InitStructure);

	//设置输出按键
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6;		//PE2-PE6
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;											//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;											//IO口速度为50MHz
	GPIO_Init(GPIOE, &GPIO_InitStructure);														//根据设定参数初始化
	GPIO_SetBits(GPIOE, GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4|GPIO_Pin_5|GPIO_Pin_6);				//输出高
	
	GPIO_InitStructure.GPIO_Pin  = GPIO_Pin_8;				//PB8
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;		//推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		//IO口速度为50MHz
	GPIO_Init(GPIOB, &GPIO_InitStructure);					//根据设定参数初始化
	GPIO_SetBits(GPIOB, GPIO_Pin_8);						//输出高
}

//main函数中调用的按键动作执行入口
u8 KeyHandle(void){
	uint8_t i, j;
	uint8_t tmpkey = 0xFF;
	static uint8_t backup[5][5] = {
		{1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1},
		{1, 1, 1, 1, 1}
	};
	
	for(i=0; i<5; i++){
		for(j=0; j<5; j++){
			if(backup[i][j] != KeySta[i][j]){
				//说明按键状态有改变
				if(backup[i][j] == 0){
					//按键弹起动作
					//KeyAction(KeyCodeMap[i][j]);
					tmpkey = KeyCodeMap[i][j];
				}
				else{
					//按键按下时无动作
				}
				backup[i][j] = KeySta[i][j];
			}
		}
	}

	return tmpkey;
}


//键盘扫描函数 在1ms定时中断内调用
void KeyScan(void){
	uint8_t i;
	static uint8_t keyout = 0;				//矩阵键盘扫描输出计数器
	static uint8_t keybuf[5][5] = {			//按键扫描缓冲区 保存一段时间内的按键值
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF}, 
		{0xFF, 0xFF, 0xFF, 0xFF, 0xFF}
	};
	uint8_t KEY_IN_0_DATA = KEY_IN_0;
	uint8_t KEY_IN_1_DATA = KEY_IN_1;
	uint8_t KEY_IN_2_DATA = KEY_IN_2;
	uint8_t KEY_IN_3_DATA = KEY_IN_3;
	uint8_t KEY_IN_4_DATA = KEY_IN_4;

	//printf("%d,%d,%d,%d,%d \r\n", KEY_IN_0_DATA, KEY_IN_1_DATA, KEY_IN_2_DATA, KEY_IN_3_DATA, KEY_IN_4_DATA);
	//把输入的5个按键移入键盘缓冲区
	keybuf[keyout][0] = (keybuf[keyout][0] << 1) | KEY_IN_0_DATA;
	keybuf[keyout][1] = (keybuf[keyout][1] << 1) | KEY_IN_1_DATA;
	keybuf[keyout][2] = (keybuf[keyout][2] << 1) | KEY_IN_2_DATA;
	keybuf[keyout][3] = (keybuf[keyout][3] << 1) | KEY_IN_3_DATA;
	keybuf[keyout][4] = (keybuf[keyout][4] << 1) | KEY_IN_4_DATA;
	
	//消抖后更新按键状态 5ms更新一次按键值
	for(i=0; i<5; i++){		//共有5个输入按键 所以循环5次
		if((keybuf[keyout][i] & KEY_MASK) == 0x00){
			//连续4次扫描值为0 即4*5=20ms内都只检测到按键按下 则可认为按键确实是按下状态
			KeySta[keyout][i] = 0;
		}
		else if((keybuf[keyout][i] & KEY_MASK) == KEY_MASK){
			//连续4次扫描值为1 即4*5=20ms内都只检测到按键弹起状态 则可认为按键确实是弹起状态
			KeySta[keyout][i] = 1;
		}
	}
	
	//执行下一次按键输出
	keyout++;
	if(5 == keyout){
		keyout = 0;
	}
	
	//更新要拉低的KEY_OUT口
	switch(keyout){
		case 0:	
			SET_KEY_OUT_4;
			CLR_KEY_OUT_0;
			break;
		case 1:
			SET_KEY_OUT_0;
			CLR_KEY_OUT_1;
			break;
		case 2:
			SET_KEY_OUT_1;
			CLR_KEY_OUT_2;
			break;
		case 3:
			SET_KEY_OUT_2;
			CLR_KEY_OUT_3;
			break;
		case 4:
			SET_KEY_OUT_3;
			CLR_KEY_OUT_4;
			break;
		default:
			break;
	}
	
}

//LED IO初始化
void LED_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);	//使能PC端口时钟
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;				 //LED2-->PC.10 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP; 		 //推挽输出
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;		 //IO口速度为50MHz
	GPIO_Init(GPIOA, &GPIO_InitStructure);					 //根据设定参数初始化GPIOC.10
	GPIO_SetBits(GPIOA,GPIO_Pin_8);							//PC.10 输出高
}

// 模拟量控制使能位初始化
void JoyStick_Init(void)
{
	GPIO_InitTypeDef  GPIO_InitStructure;

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);		//使能PC端口时钟
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;					//JOYSTICK_EN-->PC.13 端口配置
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU; 				//输入模式
	GPIO_Init(GPIOC, &GPIO_InitStructure);						//根据设定参数初始化GPIOC.10
}
