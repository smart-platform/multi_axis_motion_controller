/*
 * FILE								: usb_mass.c
 * DESCRIPTION						: This file is usb_mass file.
 * Author							: XiaomaGee@Gmail.com
 * Copyright						:
 *
 * History
 * --------------------
 * Rev								: 0.00
 * Date								: 07/24/2012
 *
 * create.
 * --------------------
 */

//---------------- Include files ------------------------//
#include "usb_mass.h"
#include "sys.h"

//---------------- Function Prototype -------------------//
int initialize(void);
extern int USB_cable(void);
extern void CTR_HP(void);
extern void USB_Istr(void);

//-----------------Function------------------------------//
/*
 * Name								: USB_HP_CAN1_TX_IRQHandler /  USB_LP_CAN1_RX0_IRQHandler
 * Description						: ---
 * Author							: XiaomaGee.
 *
 * History
 * ----------------------
 * Rev								: 0.00
 * Date								: 03/05/2012
 *
 * create.
 * ----------------------
 */
void USB_HP_CAN1_TX_IRQHandler(void)
{
	CTR_HP();
}


void USB_LP_CAN1_RX0_IRQHandler(void)
{
	USB_Istr();
}

/*
 * Name								: USB_HP_CAN1_TX_IRQHandler /  USB_LP_CAN1_RX0_IRQHandler
 * Description						: ---
 * Author							: XiaomaGee.
 *
 * History
 * ----------------------
 * Rev								: 0.00
 * Date								: 03/05/2012
 *
 * create.
 * ----------------------
 */
void USB_initialize(void)
{
	USB_cable();
}
