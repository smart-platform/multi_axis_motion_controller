/*------------------------------------------------------
   FILE NAME   : usb_mass.h
   DESCRIPTION : usb_mass driver file header
   VERSION     : 0.0.0 (C)XiaomaGee
   AUTHOR      : XiaomaGee
   CREATE DATE : 2010-6-3
   MODIFY DATE :
   LOGS        :-\
   --------------------------------------------------------*/
#ifndef __usb_mass_h__
#define __usb_mass_h__

void USB_initialize(void);
#endif //__usb_mass_h__
