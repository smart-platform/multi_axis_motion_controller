#ifndef __FSMC_H
#define __FSMC_H
#include "sys.h"

#define	DEBUG_FPGA_ARM_COM		0

#define CS0_BASE                    (0x60000000 + (0 << 23))
#define CS1_BASE                    (0x60000000 + (1 << 23))
#define CS2_BASE                    (0x60000000 + (2 << 23))
#define CS3_BASE                    (0x60000000 + (3 << 23))
#define fpga_write(offset, data) 	*((volatile unsigned short int*)(CS0_BASE + (offset << 1))) = data
#define fpga_read(offset)       	*((volatile unsigned short int*)(CS0_BASE + (offset << 1))) 


#define FPGA_WREG_CTRL_0	0xFF		///< FPGA���ƼĴ���0
#define FPGA_WREG_CTRL_1	0xFE		///< FPGA���ƼĴ���1

#define FPGA_WREG_WID_L_0	0x00		///< 0�����һ���������
#define	FPGA_WREG_NUM_L_0	0x01		///< 0�����һ���������
#define FPGA_WREG_WID_H_0	0x02		///< 0����ڶ����������
#define	FPGA_WREG_NUM_H_0	0x03		///< 0����ڶ����������
#define FPGA_WREG_WID_L_1	0x04		///< 1�����һ���������
#define	FPGA_WREG_NUM_L_1	0x05		///< 1�����һ���������
#define FPGA_WREG_WID_H_1	0x06		///< 1����ڶ����������
#define	FPGA_WREG_NUM_H_1	0x07		///< 1����ڶ����������
#define FPGA_WREG_WID_L_2	0x08		///< 2�����һ���������
#define	FPGA_WREG_NUM_L_2	0x09		///< 2�����һ���������
#define FPGA_WREG_WID_H_2	0x0A		///< 2����ڶ����������
#define	FPGA_WREG_NUM_H_2	0x0B		///< 2����ڶ����������
#define FPGA_WREG_WID_L_3	0x0C		///< 3�����һ���������
#define	FPGA_WREG_NUM_L_3	0x0D		///< 3�����һ���������
#define FPGA_WREG_WID_H_3	0x0E		///< 3����ڶ����������
#define	FPGA_WREG_NUM_H_3	0x0F		///< 3����ڶ����������
#define FPGA_WREG_WID_L_4	0x10		///< 4�����һ���������
#define	FPGA_WREG_NUM_L_4	0x11		///< 4�����һ���������
#define FPGA_WREG_WID_H_4	0x12		///< 4����ڶ����������
#define	FPGA_WREG_NUM_H_4	0x13		///< 4����ڶ����������
#define FPGA_WREG_WID_L_5	0x14		///< 5�����һ���������
#define	FPGA_WREG_NUM_L_5	0x15		///< 5�����һ���������
#define FPGA_WREG_WID_H_5	0x16		///< 5����ڶ����������
#define	FPGA_WREG_NUM_H_5	0x17		///< 5����ڶ����������
#define FPGA_WREG_OUT_0		0x18		///< 0-15���
#define FPGA_WREG_OUT_1		0x19		///< 0-15���
#define FPGA_WREG_OUT_2		0x1A		///< 0-15���
#define FPGA_WREG_OUT_3		0x1B		///< 0-15���
#define FPGA_WREG_ECPOS_L_0	0x1C		///< 0�����������16λ
#define FPGA_WREG_ECPOS_H_0	0x1D		///< 0�����������16λ
#define FPGA_WREG_ECPOS_L_1	0x1E		///< 0�����������16λ
#define FPGA_WREG_ECPOS_H_1	0x1F		///< 0�����������16λ
#define FPGA_WREG_ECPOS_L_2	0x20		///< 0�����������16λ
#define FPGA_WREG_ECPOS_H_2	0x21		///< 0�����������16λ
#define FPGA_WREG_ECPOS_L_3	0x22		///< 0�����������16λ
#define FPGA_WREG_ECPOS_H_3	0x23		///< 0�����������16λ
#define FPGA_WREG_ECPOS_L_4	0x24		///< 0�����������16λ
#define FPGA_WREG_ECPOS_H_4	0x25		///< 0�����������16λ
#define FPGA_WREG_ECPOS_L_5	0x26		///< 0�����������16λ
#define FPGA_WREG_ECPOS_H_5	0x27		///< 0�����������16λ

#define FPGA_WREG_TST_0		0x28		///< 0�Ų��ԼĴ���
#define FPGA_WREG_TST_1		0x29		///< 1�Ų��ԼĴ���

//���Ĵ���
#define FPGA_RREG_ENCODER_L_0	0x00	///< 0���������������16λ
#define FPGA_RREG_ENCODER_H_0	0x01	///< 0���������������16λ
#define FPGA_RREG_ENCODER_L_1	0x02	///< 1���������������16λ
#define FPGA_RREG_ENCODER_H_1	0x03	///< 1���������������16λ
#define FPGA_RREG_ENCODER_L_2	0x04	///< 2���������������16λ
#define FPGA_RREG_ENCODER_H_2	0x05	///< 2���������������16λ
#define FPGA_RREG_ENCODER_L_3	0x06	///< 3���������������16λ
#define FPGA_RREG_ENCODER_H_3	0x07	///< 3���������������16λ
#define FPGA_RREG_ENCODER_L_4	0x08	///< 4���������������16λ
#define FPGA_RREG_ENCODER_H_4	0x09	///< 4���������������16λ
#define FPGA_RREG_ENCODER_L_5	0x0A	///< 5���������������16λ
#define FPGA_RREG_ENCODER_H_5	0x0B	///< 5���������������16λ

#define FPGA_RREG_NUM_L_0		0x0C	///< 0����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_H_0		0x0D	///< 0����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_L_1		0x0E	///< 1����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_H_1		0x0F	///< 1����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_L_2		0x10	///< 2����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_H_2		0x11	///< 2����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_L_3		0x12	///< 3����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_H_3		0x13	///< 3����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_L_4		0x14	///< 4����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_H_4		0x15	///< 4����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_L_5		0x16	///< 5����ʵ�������16�Ĵ���
#define FPGA_RREG_NUM_H_5		0x17	///< 5����ʵ�������16�Ĵ���

#define FPGA_RREG_INPUT_0		0x18	///< 00-15�ŵ�ƽ����Ĵ���
#define FPGA_RREG_INPUT_1		0x19	///< 16-31�ŵ�ƽ����Ĵ���
#define FPGA_RREG_INPUT_2		0x1A	///< 32-47�ŵ�ƽ����Ĵ���
#define FPGA_RREG_INPUT_3		0x1B	///< 48-63�ŵ�ƽ����Ĵ���
#define FPGA_RREG_EDGE_INPUT_0	0x1C	///< 00-15�ű����ź�����Ĵ�����Z����[----------543210]
#define FPGA_RREG_EDGE_INPUT_1	0x1D	///< 16-31�ű����ź�����Ĵ���
#define FPGA_RREG_EDGE_INPUT_2	0x1E	///< 32-47�ű����ź�����Ĵ���
#define FPGA_RREG_EDGE_INPUT_3	0x1F	///< 48-63�ű����ź�����Ĵ���

#define FPGA_RREG_PUL_CUR_0		0x20	///< 0���ᵱǰ�岹�����ڵ�������
#define FPGA_RREG_PUL_CUR_1		0x21	///< 1���ᵱǰ�岹�����ڵ�������
#define FPGA_RREG_PUL_CUR_2		0x22	///< 2���ᵱǰ�岹�����ڵ�������
#define FPGA_RREG_PUL_CUR_3		0x23	///< 3���ᵱǰ�岹�����ڵ�������
#define FPGA_RREG_PUL_CUR_4		0x24	///< 4���ᵱǰ�岹�����ڵ�������
#define FPGA_RREG_PUL_CUR_5		0x25	///< 5���ᵱǰ�岹�����ڵ�������

#define FPGA_RREG_TST_0			0xFE	///< 0�Ų��ԼĴ�״̬��
#define FPGA_RREG_TST_1			0xFF	///< 1�Ų��ԼĴ�״̬��
//FPGAһ�ſ��ƼĴ���
#define FPGA_BIT_RESET			15		///< FPGA���ܸ�λ
#define FPGA_BIT_PULEN_0		0		///< 0����ʹ��λ
#define FPGA_BIT_PULEN_1		1		///< 1����ʹ��λ
#define FPGA_BIT_PULEN_2		2		///< 2����ʹ��λ
#define FPGA_BIT_PULEN_3		3		///< 3����ʹ��λ
#define FPGA_BIT_PULEN_4		4		///< 4����ʹ��λ
#define FPGA_BIT_PULEN_5		5		///< 5����ʹ��λ

#define FPGA_PULEN_0			(1 << FPGA_BIT_PULEN_0)
#define FPGA_PULEN_1			(1 << FPGA_BIT_PULEN_1)
#define FPGA_PULEN_2			(1 << FPGA_BIT_PULEN_2)
#define FPGA_PULEN_3			(1 << FPGA_BIT_PULEN_3)
#define FPGA_PULEN_4			(1 << FPGA_BIT_PULEN_4)
#define FPGA_PULEN_5			(1 << FPGA_BIT_PULEN_5)
#define FPGA_PULDISEN_0			(0 << FPGA_BIT_PULEN_0)
#define FPGA_PULDISEN_1			(0 << FPGA_BIT_PULEN_1)
#define FPGA_PULDISEN_2			(0 << FPGA_BIT_PULEN_2)
#define FPGA_PULDISEN_3			(0 << FPGA_BIT_PULEN_3)
#define FPGA_PULDISEN_4			(0 << FPGA_BIT_PULEN_4)
#define FPGA_PULDISEN_5			(0 << FPGA_BIT_PULEN_5)
#define FPGA_RESET_EN			(0 << FPGA_BIT_RESET)
#define FPGA_RESET_DISEN		(1 << FPGA_BIT_RESET)

//FPGA���ſ��ƼĴ���
#define	FPGA_BIT_ENCODER_RST_0	0		///< ��λ0�ű�����λ�����ñ�����Ϊ����ֵ������Ч
#define	FPGA_BIT_ENCODER_RST_1	1		///< ��λ1�ű�����λ�����ñ�����Ϊ����ֵ������Ч
#define	FPGA_BIT_ENCODER_RST_2	2		///< ��λ2�ű�����λ�����ñ�����Ϊ����ֵ������Ч
#define	FPGA_BIT_ENCODER_RST_3	3		///< ��λ3�ű�����λ�����ñ�����Ϊ����ֵ������Ч
#define	FPGA_BIT_ENCODER_RST_4	4		///< ��λ4�ű�����λ�����ñ�����Ϊ����ֵ������Ч
#define	FPGA_BIT_ENCODER_RST_5	5		///< ��λ5�ű�����λ�����ñ�����Ϊ����ֵ������Ч
#define	FPGA_BIT_Z_RST			7		///< ��λZ�����λ
#define FPGA_BIT_PUL_RST_0		8		///< ��λ0�����ڲ����������λ
#define FPGA_BIT_PUL_RST_1		9		///< ��λ1�����ڲ����������λ
#define FPGA_BIT_PUL_RST_2		10		///< ��λ2�����ڲ����������λ
#define FPGA_BIT_PUL_RST_3		11		///< ��λ3�����ڲ����������λ
#define FPGA_BIT_PUL_RST_4		12		///< ��λ4�����ڲ����������λ
#define FPGA_BIT_PUL_RST_5		13		///< ��λ5�����ڲ����������λ
#define FPGA_BIT_IO_RST			15		///< ��λ���������������Ϊ0xFFFF��������Ϊ0x0000

void LED_Init(void);//��ʼ��
void FSMC_Init(void);
		 	
void InterploateFun(void);		
void PlatformTestFun(void);

#endif
