#include "FSMC.h"
#include "sys.h"	 
#include "stdlib.h"  
#include "math.h"
#include "stdio.h"
#include "delay.h"

 
#define 	PERIODPULSE		24950

#if DEBUG_FPGA_ARM_COM
uint16_t Pulse0Num1 = 0;
uint16_t Pulse0Num2 = 0;
uint16_t Pulse0Wid1 = 0xFFFF;		///< 200 0.1ms
uint16_t Pulse0Wid2 = 0xFFFF;
uint16_t Pulse1Num1 = 0;
uint16_t Pulse1Num2 = 0;
uint16_t Pulse1Wid1 = 0xFFFF;		///< 200 0.1ms
uint16_t Pulse1Wid2 = 0xFFFF;
uint16_t Pulse2Num1 = 0;
uint16_t Pulse2Num2 = 0;
uint16_t Pulse2Wid1 = 0xFFFF;		///< 200 0.1ms
uint16_t Pulse2Wid2 = 0xFFFF;
uint16_t Pulse3Num1 = 0;
uint16_t Pulse3Num2 = 0;
uint16_t Pulse3Wid1 = 0xFFFF;		///< 200 0.1ms
uint16_t Pulse3Wid2 = 0xFFFF;
uint16_t Pulse4Num1 = 0;
uint16_t Pulse4Num2 = 0;
uint16_t Pulse4Wid1 = 0xFFFF;		///< 200 0.1ms
uint16_t Pulse4Wid2 = 0xFFFF;
uint16_t Pulse5Num1 = 0;
uint16_t Pulse5Num2 = 0;
uint16_t Pulse5Wid1 = 0xFFFF;		///< 200 0.1ms
uint16_t Pulse5Wid2 = 0xFFFF;
#endif
/// 初始化ARM与FPGA的通讯通道FSMC
void FSMC_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	FSMC_NORSRAMInitTypeDef  FSMC_NORSRAMInitStructure;
    FSMC_NORSRAMTimingInitTypeDef  readWriteTiming; 
	FSMC_NORSRAMTimingInitTypeDef  writeTiming;

    RCC_AHBPeriphClockCmd(RCC_AHBPeriph_FSMC, ENABLE);	//使能FSMC时钟
	//RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOD|RCC_APB2Periph_GPIOE|RCC_APB2Periph_AFIO, ENABLE);//使能PORTB,D,E,G以及AFIO复用功能时钟
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_GPIOD|RCC_APB2Periph_GPIOE|RCC_APB2Periph_AFIO, ENABLE);//使能PORTB,D,E,G以及AFIO复用功能时钟
	//PORTB复用推挽输出，地址锁存功能
	GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_7;			//PORTD复用推挽输出  
 	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AF_PP; 		//复用推挽输出   
 	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
 	GPIO_Init(GPIOB, &GPIO_InitStructure); 	
	
 	//PORTD复用推挽输出  
	GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_All;			//PORTD复用推挽输出  
 	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AF_PP; 		//复用推挽输出   
 	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
 	GPIO_Init(GPIOD, &GPIO_InitStructure); 
	  
	//PORTE复用推挽输出  
	GPIO_InitStructure.GPIO_Pin 	= GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10 | GPIO_Pin_11 | GPIO_Pin_12 | GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;				 //PORTE复用推挽输出  
 	GPIO_InitStructure.GPIO_Mode 	= GPIO_Mode_AF_PP; 		 //复用推挽输出   
 	GPIO_InitStructure.GPIO_Speed 	= GPIO_Speed_50MHz;
 	GPIO_Init(GPIOE, &GPIO_InitStructure); 
	
	
	readWriteTiming.FSMC_AddressSetupTime 		= 1;	 			//地址建立时间（ADDSET）为2个HCLK 1/36M=27ns
    readWriteTiming.FSMC_AddressHoldTime 		= 1;				//地址保持时间（ADDHLD）模式A未用到	
    readWriteTiming.FSMC_DataSetupTime 			= 1;		 		//数据保存时间为16个HCLK,因为液晶驱动IC的读数据的时候，速度不能太快，尤其对1289这个IC。
    readWriteTiming.FSMC_BusTurnAroundDuration 	= 1;
    readWriteTiming.FSMC_CLKDivision 			= 0x01;
    readWriteTiming.FSMC_DataLatency 			= 1;
    readWriteTiming.FSMC_AccessMode 			= FSMC_AccessMode_A;	 //模式A 
    

	writeTiming.FSMC_AddressSetupTime 			= 1;	 				//地址建立时间（ADDSET）为1个HCLK  
    writeTiming.FSMC_AddressHoldTime 			= 1;	 				//地址保持时间（A		
    writeTiming.FSMC_DataSetupTime 				= 1;		 			//数据保存时间为4个HCLK	
    writeTiming.FSMC_BusTurnAroundDuration 		= 1;
    writeTiming.FSMC_CLKDivision 				= 0x01;
    writeTiming.FSMC_DataLatency 				= 1;
    writeTiming.FSMC_AccessMode 				= FSMC_AccessMode_A;	 //模式A 

 
    FSMC_NORSRAMInitStructure.FSMC_Bank 					= FSMC_Bank1_NORSRAM1;					// 这里我们使用NE1 ，也就对应BTCR[6],[7]。
    //FSMC_NORSRAMInitStructure.FSMC_DataAddressMux 			= FSMC_DataAddressMux_Disable; 			// 不复用数据地址
    FSMC_NORSRAMInitStructure.FSMC_DataAddressMux 			= FSMC_DataAddressMux_Enable; 			// 复用数据地址
	FSMC_NORSRAMInitStructure.FSMC_MemoryType 				= FSMC_MemoryType_NOR;					// FSMC_MemoryType_SRAM;  //SRAM   
    FSMC_NORSRAMInitStructure.FSMC_MemoryDataWidth 			= FSMC_MemoryDataWidth_16b;				//存储器数据宽度为16bit   
    FSMC_NORSRAMInitStructure.FSMC_BurstAccessMode 			= FSMC_BurstAccessMode_Disable;			// FSMC_BurstAccessMode_Disable; 
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalPolarity 		= FSMC_WaitSignalPolarity_Low;
	FSMC_NORSRAMInitStructure.FSMC_AsynchronousWait			= FSMC_AsynchronousWait_Disable; 
    FSMC_NORSRAMInitStructure.FSMC_WrapMode 				= FSMC_WrapMode_Disable;   
    FSMC_NORSRAMInitStructure.FSMC_WaitSignalActive 		= FSMC_WaitSignalActive_BeforeWaitState;  
    FSMC_NORSRAMInitStructure.FSMC_WriteOperation 			= FSMC_WriteOperation_Enable;			//  存储器写使能
    FSMC_NORSRAMInitStructure.FSMC_WaitSignal 				= FSMC_WaitSignal_Enable;   
    FSMC_NORSRAMInitStructure.FSMC_ExtendedMode 			= FSMC_ExtendedMode_Disable;				// 读写使用不同的时序
    FSMC_NORSRAMInitStructure.FSMC_WriteBurst 				= FSMC_WriteBurst_Disable; 
    FSMC_NORSRAMInitStructure.FSMC_ReadWriteTimingStruct 	= &readWriteTiming; 					//读写时序
    FSMC_NORSRAMInitStructure.FSMC_WriteTimingStruct 		= &writeTiming; 						//写时序

    FSMC_NORSRAMInit(&FSMC_NORSRAMInitStructure);  //初始化FSMC配置 

   	FSMC_NORSRAMCmd(FSMC_Bank1_NORSRAM1, ENABLE);  // 使能BANK1 	
}

void Fun_CaclPulseH_L(uint16_t PulseSum, uint16_t *PulseNumL, uint16_t *PulseNumH, uint16_t *PulseWidthL, uint16_t *PulseWidthH, uint8_t SpdDir)
{
	uint16_t	TmpPulseWidth = 0xFFFF;
	uint16_t	TmpPulseWidthL = 0xFFFF;
	uint16_t	TmpPulseWidthH = 0xFFFF;
	uint16_t	TmpPulseNumL = 0x00;
	uint16_t	TmpPulseNumH = 0x00;

	if(PulseSum > 0){
		TmpPulseWidth	= (uint16_t)(((float)PERIODPULSE / (float)PulseSum) - 1) + 1;
		TmpPulseWidthL	= TmpPulseWidth;
		TmpPulseWidthH 	= TmpPulseWidthL + 1;
		TmpPulseNumL	= TmpPulseWidthH * PulseSum - PERIODPULSE;
		TmpPulseNumH	= PulseSum - TmpPulseNumL;

		TmpPulseNumL	= TmpPulseNumL << 1;
		TmpPulseNumH	= TmpPulseNumH << 1;
	}
	else{
		TmpPulseNumL	= 0;
		TmpPulseNumH	= 0;
	}

	if(SpdDir > 0){
		TmpPulseNumH |= 0x8000;
		TmpPulseNumL |= 0x8000;
	}

	*PulseNumH	= TmpPulseNumH;
	*PulseNumL	= TmpPulseNumL;
	*PulseWidthL= TmpPulseWidthL;
	*PulseWidthH= TmpPulseWidthH;
}

///	void PlatformTestFun(void)
///		测试ARM与FPGA通讯功能
///		
/// 
#if DEBUG_FPGA_ARM_COM
void PlatformTestFun(void)
{
	
#define maxindex 0x1E
#define maxtesttime 1000
	uint32_t tst_addr, fortime;
	uint16_t tst_res = 0;
	uint16_t tst_var, tst_varred1, tst_varred2;

	// 测试发脉冲用到的变量
	uint32_t	rPulseCmnd1 = 0;
	uint32_t	rPulseCmnd2 = 0;
	uint32_t	rPulseCmnd3 = 0;
	uint32_t	rPulseCmnd4 = 0;
	uint32_t	rPulseCmnd5 = 0;
	uint32_t	rPulseCmnd6 = 0;
	static uint16_t pulsenumsumlast = 0;
	uint32_t 	rPulseCount1 = 0;
	uint32_t 	rPulseCount2 = 0;
	uint32_t 	rPulseCount3 = 0;
	uint32_t 	rPulseCount4 = 0;
	uint32_t 	rPulseCount5 = 0;
	uint32_t 	rPulseCount6 = 0;
	uint32_t	dat1 = 0;
	uint32_t	dat2 = 0;
	uint32_t 	PulseCount1 = 0;
	uint32_t 	PulseCount2 = 0;
	uint32_t 	PulseCount3 = 0;
	uint32_t 	PulseCount4 = 0;
	uint32_t 	PulseCount5 = 0;
	uint32_t 	PulseCount6 = 0;
	static uint16_t PulseNum1 = 0;
	static uint16_t PulseNum2 = 0;
	static uint16_t PulseWid1 = 0;		///< 200 0.1ms
	static uint16_t PulseWid2 = 0;

	/// 测试FPGA与ARM的通讯
	printf("发脉冲测试:\r\n");
	printf("步骤1->测试FPGA与ARM的通讯......\r\n");
	for(fortime = 0; fortime < maxtesttime; fortime ++){
		tst_var = (uint16_t)fortime;
		fpga_write(FPGA_WREG_TST_0, tst_var);
		fpga_write(FPGA_WREG_TST_1, tst_var + 1);
		delay_ms(1);
		tst_varred1 = fpga_read(FPGA_RREG_TST_0);
		tst_varred2 = fpga_read(FPGA_RREG_TST_1);

		if((tst_var != tst_varred1) || ((uint16_t)(tst_var + 1) != tst_varred2)){
			printf("\tERROR, read[0x%2x]=0x%4x, write[0x%2x]=0x%4x, write val[0x%4x]\r\n", FPGA_WREG_TST_0, tst_varred1, FPGA_WREG_TST_1, tst_varred2, tst_var);
			tst_res = 1;
		}

		if(0 == fortime % 100){
			printf("\t测试进行中%d%%[%06d][%06d] \r\n", fortime * 100 / maxtesttime, tst_varred1, tst_varred2);
		}
	}

	if(1 == tst_res){
		printf("\tARM与FPGA的通信测试出错 \r\n");
	}
	else{
		printf("\tARM与FPGA的通信测试成功 \r\n");
	}

	/// 步骤2->发脉冲测试
	printf("步骤2->测试发脉冲...... \r\n");
	fortime = 0;
	while(fortime < 10000){
		
		fpga_write(FPGA_WREG_CTRL_0, FPGA_PULEN_0 | FPGA_PULEN_1 | FPGA_PULEN_2 | FPGA_PULEN_3 | FPGA_PULEN_4 | FPGA_PULEN_5 | FPGA_RESET_DISEN);
		
		rPulseCmnd1 = fpga_read(FPGA_RREG_PUL_CUR_0);
		rPulseCmnd2 = fpga_read(FPGA_RREG_PUL_CUR_1);
		rPulseCmnd3 = fpga_read(FPGA_RREG_PUL_CUR_2);
		rPulseCmnd4 = fpga_read(FPGA_RREG_PUL_CUR_3);
		rPulseCmnd5 = fpga_read(FPGA_RREG_PUL_CUR_4);
		rPulseCmnd6 = fpga_read(FPGA_RREG_PUL_CUR_5);
		
		if(rPulseCmnd1 != pulsenumsumlast){
			printf("ERROR,[1]%d,%d\r\n", rPulseCmnd1, pulsenumsumlast);
		}
		
		if(rPulseCmnd2 != pulsenumsumlast){
			printf("ERROR,[2]%d,%d\r\n", rPulseCmnd2, pulsenumsumlast);
		}
		
		if(rPulseCmnd3 != pulsenumsumlast){
			printf("ERROR,[3]%d,%d\r\n", rPulseCmnd3, pulsenumsumlast);
		}
		
		if(rPulseCmnd4 != pulsenumsumlast){
			printf("ERROR,[4]%d,%d\r\n", rPulseCmnd4, pulsenumsumlast);
		}
		
		if(rPulseCmnd5 != pulsenumsumlast){
			printf("ERROR,[5]%d,%d\r\n", rPulseCmnd5, pulsenumsumlast);
		}
		
		if(rPulseCmnd6 != pulsenumsumlast){
			printf("ERROR,[6]%d,%d\r\n", rPulseCmnd6, pulsenumsumlast);
		}

		dat1 = fpga_read(FPGA_RREG_NUM_L_0);
		dat2 = fpga_read(FPGA_RREG_NUM_H_0);
		rPulseCount1 = dat2;
		rPulseCount1 = rPulseCount1 << 16;
		rPulseCount1 += dat1;
		
		dat1 = fpga_read(FPGA_RREG_NUM_L_1);
		dat2 = fpga_read(FPGA_RREG_NUM_H_1);
		rPulseCount2 = dat2;
		rPulseCount2 = rPulseCount2 << 16;
		rPulseCount2 += dat1;
		
		dat1 = fpga_read(FPGA_RREG_NUM_L_2);
		dat2 = fpga_read(FPGA_RREG_NUM_H_2);
		rPulseCount3 = dat2;
		rPulseCount3 = rPulseCount3 << 16;
		rPulseCount3 += dat1;
		
		dat1 = fpga_read(FPGA_RREG_NUM_L_3);
		dat2 = fpga_read(FPGA_RREG_NUM_H_3);
		rPulseCount4 = dat2;
		rPulseCount4 = rPulseCount4 << 16;
		rPulseCount4 += dat1;

		dat1 = fpga_read(FPGA_RREG_NUM_L_4);
		dat2 = fpga_read(FPGA_RREG_NUM_H_4);
		rPulseCount5 = dat2;
		rPulseCount5 = rPulseCount5 << 16;
		rPulseCount5 += dat1;
		
		dat1 = fpga_read(FPGA_RREG_NUM_L_5);
		dat2 = fpga_read(FPGA_RREG_NUM_H_5);
		rPulseCount6 = dat2;
		rPulseCount6 = rPulseCount6 << 16;
		rPulseCount6 += dat1;
		
		fpga_write(FPGA_WREG_CTRL_0, FPGA_PULDISEN_0 | FPGA_PULDISEN_1 | FPGA_PULDISEN_2 | FPGA_PULDISEN_3 | FPGA_PULDISEN_4 | FPGA_PULDISEN_5 | FPGA_RESET_EN);
		
		fpga_write(FPGA_WREG_CTRL_0, FPGA_RESET_DISEN);


		fpga_write(FPGA_WREG_NUM_L_0, PulseNum1);
		fpga_write(FPGA_WREG_WID_L_0, PulseWid1);
		fpga_write(FPGA_WREG_NUM_H_0, PulseNum2);
		fpga_write(FPGA_WREG_WID_H_0, PulseWid2);
		
		fpga_write(FPGA_WREG_NUM_L_1, PulseNum1);
		fpga_write(FPGA_WREG_WID_L_1, PulseWid1);
		fpga_write(FPGA_WREG_NUM_H_1, PulseNum2);
		fpga_write(FPGA_WREG_WID_H_1, PulseWid2);
		
		fpga_write(FPGA_WREG_NUM_L_2, PulseNum1);
		fpga_write(FPGA_WREG_WID_L_2, PulseWid1);
		fpga_write(FPGA_WREG_NUM_H_2, PulseNum2);
		fpga_write(FPGA_WREG_WID_H_2, PulseWid2);
	
		fpga_write(FPGA_WREG_NUM_L_3, PulseNum1);
		fpga_write(FPGA_WREG_WID_L_3, PulseWid1);
		fpga_write(FPGA_WREG_NUM_H_3, PulseNum2);
		fpga_write(FPGA_WREG_WID_H_3, PulseWid2);
		
		fpga_write(FPGA_WREG_NUM_L_4, PulseNum1);
		fpga_write(FPGA_WREG_WID_L_4, PulseWid1);
		fpga_write(FPGA_WREG_NUM_H_4, PulseNum2);
		fpga_write(FPGA_WREG_WID_H_4, PulseWid2);
		
		fpga_write(FPGA_WREG_NUM_L_5, PulseNum1);
		fpga_write(FPGA_WREG_WID_L_5, PulseWid1);
		fpga_write(FPGA_WREG_NUM_H_5, PulseNum2);
		fpga_write(FPGA_WREG_WID_H_5, PulseWid2);
		
		fpga_write(FPGA_WREG_CTRL_0, FPGA_PULEN_0 | FPGA_PULEN_1 | FPGA_PULEN_2 | FPGA_PULEN_3 | FPGA_PULEN_4 | FPGA_PULEN_5 | FPGA_RESET_DISEN);
		
		//Fun_CaclPulseH_L(fortime & 0xFF, &PulseNum1, &PulseNum2, &PulseWid1, &PulseWid2, 1);
		
		PulseCount1 += ((PulseNum1 & 0x7FFF) + (PulseNum2 & 0x7FFF));
		PulseCount2 += ((PulseNum1 & 0x7FFF) + (PulseNum2 & 0x7FFF));
		PulseCount3 += ((PulseNum1 & 0x7FFF) + (PulseNum2 & 0x7FFF));
		PulseCount4 += ((PulseNum1 & 0x7FFF) + (PulseNum2 & 0x7FFF));
		PulseCount5 += ((PulseNum1 & 0x7FFF) + (PulseNum2 & 0x7FFF));
		PulseCount6 += ((PulseNum1 & 0x7FFF) + (PulseNum2 & 0x7FFF));
		pulsenumsumlast = ((PulseNum1 & 0x7FFF) + (PulseNum2 & 0x7FFF));
		
		Fun_CaclPulseH_L(fortime & 0xFF, &PulseNum1, &PulseNum2, &PulseWid1, &PulseWid2, 1);
		fortime ++;
		
		if(0 == fortime % 500){
			printf("\t[axis1]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount1, rPulseCount1);
			printf("\t[axis2]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount2, rPulseCount2);
			printf("\t[axis3]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount3, rPulseCount3);
			printf("\t[axis4]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount4, rPulseCount4);
			printf("\t[axis5]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount5, rPulseCount5);
			printf("\t[axis6]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount6, rPulseCount6);
		}
		
		delay_ms(5);
	}

	//测试完成，将最终的各轴的数据打印出来
	printf("测试结束，打印出最终的脉冲数目 \r\n");
	fpga_write(FPGA_WREG_CTRL_0, FPGA_PULEN_0 | FPGA_PULEN_1 | FPGA_PULEN_2 | FPGA_PULEN_3 | FPGA_PULEN_4 | FPGA_PULEN_5 | FPGA_RESET_DISEN);

	dat1 = fpga_read(FPGA_RREG_NUM_L_0);
	dat2 = fpga_read(FPGA_RREG_NUM_H_0);
	rPulseCount1 = dat2;
	rPulseCount1 = rPulseCount1 << 16;
	rPulseCount1 += dat1;

	dat1 = fpga_read(FPGA_RREG_NUM_L_1);
	dat2 = fpga_read(FPGA_RREG_NUM_H_1);
	rPulseCount2 = dat2;
	rPulseCount2 = rPulseCount2 << 16;
	rPulseCount2 += dat1;

	dat1 = fpga_read(FPGA_RREG_NUM_L_2);
	dat2 = fpga_read(FPGA_RREG_NUM_H_2);
	rPulseCount3 = dat2;
	rPulseCount3 = rPulseCount3 << 16;
	rPulseCount3 += dat1;

	dat1 = fpga_read(FPGA_RREG_NUM_L_3);
	dat2 = fpga_read(FPGA_RREG_NUM_H_3);
	rPulseCount4 = dat2;
	rPulseCount4 = rPulseCount4 << 16;
	rPulseCount4 += dat1;

	dat1 = fpga_read(FPGA_RREG_NUM_L_4);
	dat2 = fpga_read(FPGA_RREG_NUM_H_4);
	rPulseCount5 = dat2;
	rPulseCount5 = rPulseCount5 << 16;
	rPulseCount5 += dat1;

	dat1 = fpga_read(FPGA_RREG_NUM_L_5);
	dat2 = fpga_read(FPGA_RREG_NUM_H_5);
	rPulseCount6 = dat2;
	rPulseCount6 = rPulseCount6 << 16;
	rPulseCount6 += dat1;

	fpga_write(FPGA_WREG_CTRL_0, FPGA_PULDISEN_0 | FPGA_PULDISEN_1 | FPGA_PULDISEN_2 | FPGA_PULDISEN_3 | FPGA_PULDISEN_4 | FPGA_PULDISEN_5 | FPGA_RESET_EN);

	printf("\t[axis1]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount1, rPulseCount1);
	printf("\t[axis2]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount2, rPulseCount2);
	printf("\t[axis3]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount3, rPulseCount3);
	printf("\t[axis4]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount4, rPulseCount4);
	printf("\t[axis5]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount5, rPulseCount5);
	printf("\t[axis6]CommandPulse=%d, SendedPulse=%d \r\n", PulseCount6, rPulseCount6);

	printf("\t发脉冲测试完成 \r\n");
}

#endif
