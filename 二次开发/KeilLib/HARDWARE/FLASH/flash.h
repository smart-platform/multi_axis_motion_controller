#ifndef __FLASH_H
#define __FLASH_H			    
#include "sys.h" 
	  
//W25X系列/Q系列芯片列表
//4Kbytes为一个Sector
//16个扇区为1个Block
//W25X16
//容量为2M字节,共有32个Block,512个Sector 
#define W25Q80 	0xEF13 	
#define W25Q16 	0xEF14
#define W25Q32 	0xEF15
#define W25Q64 	0xEF16

//指令表
#define W25X_WriteEnable		0x06 
#define W25X_WriteDisable		0x04 
#define W25X_ReadStatusReg		0x05 
#define W25X_WriteStatusReg		0x01 
#define W25X_ReadData			0x03 
#define W25X_FastReadData		0x0B 
#define W25X_FastReadDual		0x3B 
#define W25X_PageProgram		0x02 
#define W25X_BlockErase			0xD8 
#define W25X_SectorErase		0x20 
#define W25X_ChipErase			0xC7 
#define W25X_PowerDown			0xB9 
#define W25X_ReleasePowerDown	0xAB 
#define W25X_DeviceID			0xAB 
#define W25X_ManufactDeviceID	0x90 
#define W25X_JedecDeviceID		0x9F 

///< void SPI_Flash_Init(void)
///<	初始化SPI FLASH的IO口，并返回FLASH的ID值
///< @param		[I]			无
///< @param		[O]			无
///< @return	uint16_t	FLASH的ID值
///< @see
///< @note		W25Q16->0xEF64; W25Q16->0XEF14; W25Q32->0XEF15; W25Q64->0XEF16; W25Q80->0XEF13
uint16_t SPI_Flash_Init(void);

///< uint16_t  SPI_Flash_ReadID(void)
///<	返回FLASH的ID值
///< @param		[I]			无
///< @param		[O]			无
///< @return	uint16_t	FLASH的ID值
///< @see
///< @note		W25Q16->0xEF64; W25Q16->0XEF14; W25Q32->0XEF15; W25Q64->0XEF16; W25Q80->0XEF13
uint16_t  SPI_Flash_ReadID(void);

///< uint8_t	 SPI_Flash_ReadSR(void)
///<	读取状态寄存器
///< @param		[I]			无
///< @param		[O]			无
///< @return	uint8_t		FLASH状态寄存器的值
///< @see
///< @note
uint8_t	 SPI_Flash_ReadSR(void);

///< void	 SPI_FLASH_Write_SR(uint8_t sr)
///<	写状态寄存器
///< @param[I]	uint8_t		状态寄存器的值
///< @param[O]	无
///< @return	无
///< @see
///< @note
void SPI_FLASH_Write_SR(uint8_t sr); 

///< void	SPI_FLASH_Write_Enable(void)
///<	写芯片使能
///< @param[I]	无
///< @param[O]	无
///< @return	无
///< @see
///< @note 在进行FLASH写、擦除操作前，要先写能
void SPI_FLASH_Write_Enable(void);

///< void	SPI_FLASH_Write_Disable(void)
///<	写保护
///< @param[I]	无
///< @param[O]	无
///< @return	无
///< @see
///< @note 将状态寄存器中的WEL位清0，这样就不能进行芯片操作，从软件保护芯片
void SPI_FLASH_Write_Disable(void);

///< void	SPI_Flash_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite)
///<	将pBuffer缓冲区写入到指令地址
///< @param[I]	pBuffer			数据保存缓冲区
///< @param[I]	WriteAddr		写地址
///< @param[I]	NumByteToRead	写取个数
///< @param[O]	无
///< @return	无
///< @see
///< @note		该指令不带芯片擦除功能，使用者要确定要写入的地址的内容是空的
void SPI_Flash_Write_NoCheck(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);

///< void	SPI_Flash_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t NumByteToRead)
///<	读取芯片指令地址的内容至pBuffer缓冲区
///< @param[I]	pBuffer			数据保存缓冲区
///< @param[I]	WriteAddr		读地址
///< @param[I]	NumByteToRead	读取个数
///< @param[O]	无
///< @return	无
///< @see
///< @note
void SPI_Flash_Read(uint8_t* pBuffer,uint32_t ReadAddr,uint16_t NumByteToRead);

///< void	SPI_Flash_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite)
///<	将pBuffer缓冲区写入到指令地址
///< @param[I]	pBuffer			数据保存缓冲区
///< @param[I]	WriteAddr		写地址
///< @param[I]	NumByteToRead	写取个数
///< @param[O]	无
///< @return	无
///< @see
///< @note
void SPI_Flash_Write(uint8_t* pBuffer,uint32_t WriteAddr,uint16_t NumByteToWrite);

///< void	SPI_Flash_Erase_Chip(void)
///<	整片擦除
///< @param[I]	无
///< @param[O]	无
///< @return	无
///< @see
///< @note		操作时间较长
void SPI_Flash_Erase_Chip(void);

///< void	SPI_Flash_Erase_Sector(uint32_t Dst_Addr)
///<	扇区擦除
///< @param[I]	Dst_Addr	欲擦除的首地址，4096取整
///< @param[O]	无
///< @return	无
///< @see
///< @note		操作时间较长
void SPI_Flash_Erase_Sector(uint32_t Dst_Addr);

///< void	SPI_Flash_Wait_Busy(void)
///<	等待空闲
///< @param[I]	无
///< @param[O]	无
///< @return	无
///< @see
///< @note
void SPI_Flash_Wait_Busy(void);

///< void	SPI_Flash_PowerDown(void)
///<	进入掉电模式
///< @param[I]	无
///< @param[O]	无
///< @return	无
///< @see
///< @note
void SPI_Flash_PowerDown(void);

///< void	SPI_Flash_WAKEUP(void)
///<	唤醒
///< @param[I]	无
///< @param[O]	无
///< @return	无
///< @see
///< @note
void SPI_Flash_WAKEUP(void);

#endif
