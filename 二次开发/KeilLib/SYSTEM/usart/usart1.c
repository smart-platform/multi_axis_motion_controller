/**
  ******************************************************************************
    * 文件名    USART.c 
    stm32f103rbt6(LQFP64):

	PIN42--PA9--USART1_TX
	PIN43--PA10--USART1_RX
	
	PIN29--PB10--USART3_TX
	PIN30--PB11--USART3_RX

   	PIN16--PA2--USART2_TX
	PIN17--PA3--USART2_RX

  *  知识点：
  *  RXEN = 1：移位寄存器中的内容已经转移到RDR
  *  RXNEIE =1:产生中断
  *	 
	串口使用DMA进行发送，可以通过设置USART_CR3寄存器上的DMAT位激活。只要TXE位被置
起，就从配置成使用DMA外设的SRAM区装载数据到USART_DR寄存器。为USART的发送分
配一个DMA通道的步骤如下(x表示通道号)： 
1.  在DMA控制寄存器上将USART_DR寄存器地址配置成DMA传输的目的地址。在每个TXE事
件后，数据将被传送到这个地址。 
2.  在DMA控制寄存器上将存储器地址配置成DMA传输的源地址。在每个TXE事件后，数据将
从此存储器区传送到USART_DR寄存器。 
3.  在DMA控制寄存器中配置要传输的总的字节数。 
4.  在DMA寄存器上配置通道优先级。 
5.  根据应用程序的要求配置在传输完成一半还是全部完成时产生DMA中断。 
6.  在DMA寄存器上激活该通道。 
7.  当DMA控制器中指定的数据量传输完成时，DMA控制器在该DMA通道的中断向量上产生一
中断。在中断服务程序里，软件应将USART_CR3寄存器的DMA位清零。 
注意：  如果DMA被用于发送，不要使能TXEIE位。 

	串口使用DMA进行接收，可以通过设置USART_CR3寄存器的DMAR位激活。只要接收到一个字
节，数据就从USART_DR寄存器放到配置成使用DMA的SRAM区(参考DMA技术说明)。为USART的接
收分配一个DMA通道步骤如下(x表示通道号)： 
1.  通过DMA控制寄存器把USART_DR寄存器地址配置成传输的源地址。在每个RXNE事件后
    此地址上的数据将传输到存储器。 
2.  通过DMA控制寄存器把存储器地址配置成传输的目的地址。在每个RXNE事件后，数据将从
    USART_DR传输到此存储器区。 
3.  在DMA控制寄存器中配置要传输的总的字节数。 
4.  在DMA寄存器上配置通道优先级。。 
5.  根据应用程序的要求配置在传输完成一半还是全部完成时产生DMA中断。 
6.  在DMA控制寄存器上激活该通道。通用同步异步收发器(USART)  
7.  当DMA控制器中指定的传输数据量接收完成时，DMA控制器在该DMA通道的中断矢量上产生
	一中断。在中断程序里， USART_CR3寄存器的DMAR位应该被软件清零。 
注意：  如果DMA被用来接收，不要使能RXNEIE位。
 

  ******************************************************************************
 
  ****************************************************************************** 
  */ 
#include "usart.h"

u8 USART_RX_BUF[USART_RECBUF_LEN];     //接收缓冲,最大USART_REC_LEN个字节.


void USART_Config(USART_TypeDef* USARTx,u32 baudrate)
{	
	
	GPIO_InitTypeDef   GPIO_InitStructure;
    USART_InitTypeDef  USART_InitStructure;
		
	/*使能GPIOA时钟，使能串口1时钟*/
	//USART1，GPIOA~E挂在APB2总线上
	if(USARTx==USART1){
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1,ENABLE);	
		/*A9 USART1.TX 推挽输出*/ 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
		/*A10 USART1.RX 浮空输入 */ 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	
		GPIO_Init(GPIOA, &GPIO_InitStructure);
	}

	else if(USARTx==USART2){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2,ENABLE);
		/*PA2 USART2.TX复用推挽输出*/ 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOA, &GPIO_InitStructure);
		/*PA3 USART2.RX浮空输入 */ 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	
		GPIO_Init(GPIOA, &GPIO_InitStructure);
	}
	//USART2,USART3 挂在APB1总线上
	//RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2 | RCC_APB1Periph_USART3,ENABLE);
    //为了降低功耗，分别开启时钟
	else if(USARTx==USART3){
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3,ENABLE);
		/*PB10 USART3.TX推挽输出*/ 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
		GPIO_Init(GPIOB, &GPIO_InitStructure);
		/*PB11 USART3.RX浮空输入 */ 
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	
		GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	}
	
	//复用功能的引脚重映射，默认映射的引脚是PA2和PA3,若不是(如100Pin的STM32)需要加上这句。
	//GPIO_PinRemapConfig(GPIO_FullRemap_USART3, ENABLE);
	//GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);
	 
	/*配置USARTX的工作模式*/
	USART_InitStructure.USART_BaudRate		= baudrate;						//波特率      
	USART_InitStructure.USART_WordLength	= USART_WordLength_8b;			//字长8bit										
	USART_InitStructure.USART_StopBits		= USART_StopBits_1;				//停止位1位									
	USART_InitStructure.USART_Parity		= USART_Parity_No;				//无奇偶校验							
	USART_InitStructure.USART_Mode			= USART_Mode_Rx | USART_Mode_Tx;//收发使能
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;		//无硬件流控制
								    
	USART_Init(USARTx, &USART_InitStructure);			//初始化外设 USARTx	  
    USART_Cmd(USARTx, ENABLE);							//使能USART1外设
	//USART_ITConfig(USARTx,USART_IT_RXNE,ENABLE);		//使能接收中断

	//必须进行NVIC_Init(&NVIC_InitStructure)才能是串口中断接收正常工作，该初始化函数见	
	// \code\sys.c  中的NVIC_Config(void);
}

/**************************************************************


***************************************************************/
void UART0_SendByte(unsigned char c){
	USART_PutChar(USART1, c);
}


void USART_PutChar(USART_TypeDef* USARTx,unsigned char c)
{
    USART_SendData(USARTx, c);
    while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET );
}

/**************************************************************

***************************************************************/
void USART_PutStr(USART_TypeDef* USARTx,char * str)
{
    while(*str)
    {
        USART_SendData(USARTx, *str++);
        while(USART_GetFlagStatus(USARTx, USART_FLAG_TXE) == RESET);
    }
}

/************DMA方式传输***************************/
#define SRC_USART1_DR	(&(USART1->DR))		//串口接收寄存器作为源头

/**********************************************************
** 函数名:void USART_DMAToBuf1(void)
** 功能描述: 串口DMA初始化配置
** 输入参数: 无
** 输出参数: 无 
***********************************************************/
void DMA_UARTToMemConfig(void)
{
	//NVIC_InitTypeDef NVIC_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;//定义DMA结构体

	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);          //使能DMA时钟
	DMA_DeInit(DMA1_Channel5);
	DMA_InitStructure.DMA_PeripheralBaseAddr	= (u32)SRC_USART1_DR;			//源头BUF
	DMA_InitStructure.DMA_MemoryBaseAddr		= (u32)USART_RX_BUF;			//目标BUF
	DMA_InitStructure.DMA_DIR					= DMA_DIR_PeripheralSRC;		//外设作源头
	DMA_InitStructure.DMA_BufferSize			= USART_RECBUF_LEN;				//BUF大小
	DMA_InitStructure.DMA_PeripheralInc			= DMA_PeripheralInc_Disable;	//外设地址寄存器不递增
	DMA_InitStructure.DMA_MemoryInc				= DMA_MemoryInc_Enable;			//内存地址递增
	DMA_InitStructure.DMA_PeripheralDataSize	= DMA_PeripheralDataSize_Byte;	//外设字节为单位
	DMA_InitStructure.DMA_MemoryDataSize		= DMA_PeripheralDataSize_Byte;	//内存字节为单位
	DMA_InitStructure.DMA_Mode					= DMA_Mode_Circular;			//循环模式
	DMA_InitStructure.DMA_Priority				= DMA_Priority_High;			//4优先级之一的(高优先)
	DMA_InitStructure.DMA_M2M					= DMA_M2M_Disable;				//非内存到内存
	DMA_Init(DMA1_Channel5, &DMA_InitStructure);
	
	//DMA_ITConfig(DMA1_Channel5, DMA_IT_TC, ENABLE);		//DMA5传输完成中断
	USART_DMACmd(USART1,USART_DMAReq_Rx,ENABLE);			//使能串口接收DMA	

	/*
	NVIC_InitStructure.NVIC_IRQChannel			= DMA1_Channel5_IRQn;	//DMA1通道5中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority	= 0; 		//先占式优先级设为0
	NVIC_InitStructure.NVIC_IRQChannelSubPriority			= 0;		//副优先级设为0
	NVIC_InitStructure.NVIC_IRQChannelCmd					= ENABLE;	//中断向量使能
	NVIC_Init(&NVIC_InitStructure);										//初始化结构体
	*/

	DMA_Cmd(DMA1_Channel5, ENABLE);		// 使能DMA
}

#if 0
// 串口循环发送接收程序
void USART1_IRQHandler(void)
{
	 u8 str1;
     
  if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
  {
     //USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
     str1=USART_ReceiveData(USART1);
     USART_SendData(USART1, str1);
     while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET); 
     //USART_ITConfig( USART1,USART_IT_RXNE, ENABLE);
  }
}

#else 

//extern DMA_InitTypeDef DMA_InitStructure;
/**********************************************************
** 函数名:void DMA1_Channel5_IRQHandler(void)
** 功能描述: DMA中断服务程序(乒乓缓冲)
** 输入参数: 无
** 输出参数: 无 
***********************************************************/
/*
void DMA1_Channel5_IRQHandler(void)
{
	if(DMA_GetITStatus(DMA1_IT_TC5))
 	{
	    u32 DataCounter = DMA_GetCurrDataCounter(DMA1_Channel5);//获取剩余长度,一般都为0,调试用
 	    if(dma_len == DMA_GetCurrDataCounter(DMA1_Channel5)){
 			Buf_Ok=1;
 		}
 		else{
			DMA_Cmd(DMA1_Channel5, ENABLE);		// 使能DMA
 		}
		
		DMA_ClearITPendingBit(DMA1_IT_GL5);	//清除全部中断标志										     
		
		//转换可操作BUF
		if(Free_Buf_Now==BUF_NO1) //如果BUF1空闲，将DMA接收数据赋值给BUF1
		{	
			DMA_InitStructure.DMA_MemoryBaseAddr = (u32)USART1_DMA_Buf1;
			DMA_Init(DMA1_Channel5, &DMA_InitStructure);
			Free_Buf_Now=BUF_NO2;
		}
		else  //如果BUF2空闲，将DMA接收数据赋值给BUF2
		{
			DMA_InitStructure.DMA_MemoryBaseAddr = (u32)USART1_DMA_Buf2;
			DMA_Init(DMA1_Channel5, &DMA_InitStructure);
			Free_Buf_Now=BUF_NO1;
		}
	
 	}
}
*/
#endif

// 选择编译以下代码,支持printf函数,而不需要选择use MicroLIB
	  
#if 1

#pragma import(__use_no_semihosting)             
//标准库需要的支持函数                 
struct __FILE 
{ 
	int handle; 
	/* Whatever you require here. If the only file you are using is   */ 
	/* standard output using printf() for debugging, no file handling */ 
	/* is required. */ 
}; 
/* FILE is typedef’ d in stdio.h. */ 
FILE __stdout;       
//定义_sys_exit()以避免使用半主机模式    
_sys_exit(int x) 
{ 
	x = x; 
} 
/**************************************************************
 ** 函数名 :fputc
 ** 功能   :重定向c库函数printf到USART
 ** 注意   :由printf调用
 **         Example:printf("i=%d,ch=%c\n",i,ch);
 			printf("\r\n LCD驱动IC为:%x \r\n",0x55); 
            %d:按10进制有符号整数实际长度输出
			%u:按10进制无符号整数实际长度输出
			%o:按8进制数据实际长度输出
			%x:按16进制数据实际长度输出
			%c:输出一个字符
			%s:输出一个字符串
***************************************************************/
int fputc(int ch, FILE *f)
{
/* 将Printf内容发往串口1 */
  USART_SendData(USART1, (unsigned char) ch);
  while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET); 
  return (ch);
}


/*******************************************************************************
* Function Name  : int fgetc(FILE *f)
* Description    : Retargets the C library printf function to the USART.fgetc重定向
* Input          : None
* Output         : None
* Return         : 读取到的字符
*******************************************************************************/
int fgetc(FILE *f)
	{
	/* Loop until received a char */
	while(!(USART_GetFlagStatus(USART1, USART_FLAG_RXNE) == SET))
		{
		}	
	/* Read a character from the USART1 and RETURN */
	return (USART_ReceiveData(USART1));
	}

#endif
