#ifndef __USART_H
#define __USART_H
#include "stdio.h"	
#include "sys.h" 
//////////////////////////////////////////////////////////////////////////////////	 
//本程序只供学习使用，未经作者许可，不得用于其它任何用途
//Mini STM32开发板
//串口1初始化		   
//正点原子@ALIENTEK
//技术论坛:www.openedv.csom
//修改日期:2011/6/14
//版本：V1.4
//版权所有，盗版必究。
//Copyright(C) 正点原子 2009-2019
//All rights reserved
//********************************************************************************
//V1.3修改说明 
//支持适应不同频率下的串口波特率设置.
//加入了对printf的支持
//增加了串口接收命令功能.
//修正了printf第一个字符丢失的bug
//V1.4修改说明
//1,修改串口初始化IO的bug
//2,修改了USART_RX_STA,使得串口最大接收字节数为2的14次方
//3,增加了USART_REC_LEN,用于定义串口最大允许接收的字节数(不大于2的14次方)
//4,修改了EN_USART1_RX的使能方式
////////////////////////////////////////////////////////////////////////////////// 	
#define USART_REC_LEN  			100  	//定义最大接收字节数 14
#define EN_USART1_RX 			1		//使能（1）/禁止（0）串口1接收
	  	
extern u8  USART_RX_BUF[USART_REC_LEN]; //接收缓冲,最大USART_REC_LEN个字节.末字节为换行符 
extern u16 USART_RX_STA;         		//接收状态标记	
extern u8  Usart_rx_overtime;			// 串口超时标记，该标记在定时器中自增，串口中断中，对该标记进行判断

extern enum eSpComSttMachTyp eSpComStt; // 串口接收状态机
extern uint16_t u16DtLen;				// 串口欲接收的数据长度
extern uint8_t u8SpTimeOut;				// 串口超时变量
// 定义串口数据接收有限状态机
enum eSpComSttMachTyp{
	SpComHeader = 1,		///< 串口接收帧头阶段
	SpComDataLen = 2,		///< 串口接收数据长度和校验阶段
	SpComDataPhase = 3,		///< 串口接收数据阶段
	SpComDataProcs = 4,		///< 数据处理阶段
};

struct stSpComHeaderTyp{
	uint8_t u8Header;	///< 串口数据流的帧头
	uint8_t u8DataLen;	///< 数据区的长度
	uint8_t u8DtLenR;	///< 数据区的长度的反
};

//如果想串口中断接收，请不要注释以下宏定义
void uart_init(u32 bound);
void UART0_SendByte(u8 ch);
#endif


