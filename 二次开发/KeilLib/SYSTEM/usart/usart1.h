#ifndef __USART_H
#define __USART_H

#include "stm32f10x.h"
#include <stdio.h>

#define USART_RECBUF_LEN  			64  	//定义最大接收字节数 14
extern u8 USART_RX_BUF[USART_RECBUF_LEN + 5];	//接收缓冲,最大USART_REC_LEN个字节.

void USART_Config(USART_TypeDef* USARTx,u32 baudrate);
void UART_Send_Byte(char c);
void UART_Send_Enter(void);
void UART_Put_Num(unsigned long dat);
void UART_Put_Inf(char *inf,unsigned long dat);
void u32tostr(unsigned long dat,char *str);
unsigned long strtou32(char *str) ;
void DMA_UARTToMemConfig(void);
void USART1_IRQHandler(void);	
int fputc(int ch, FILE *f);    //fputc重定向
int fgetc(FILE *f); 		   //fgetc重定向
void USART_PutChar(USART_TypeDef* USARTx,unsigned char c);
void USART_PutStr(USART_TypeDef* USARTx,char * str);
void UART0_SendByte(unsigned char c);
#endif