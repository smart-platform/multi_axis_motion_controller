/*
 * FILE				: diskio.c
 * DESCRIPTION		: This file is main files.
 * Author			: XiaomaGee@Gmail.com
 * Copyright		:
 *
 * History
 * --------------------
 * Rev	: 0.00
 * Date	: 03/05/2012
 *
 * create.
 * --------------------
 */

//-----------------Include files-------------------------//
#include "..\fatfs\diskio.h"
#include "..\HARDWARE\FLASH\flash.h"

//-----------------Function-----------------------------//
/*
 * FILE				: get_fattime
 * DESCRIPTION		: This file is main files.
 * Author			: XiaomaGee@Gmail.com
 * Copyright		:
 *
 * History
 * --------------------
 * Rev	: 0.00
 * Date	: 03/05/2012
 *
 * create.
 * --------------------
 */
unsigned long int get_fattime(void)
{
	return 0;
}

/*
 * FILE				: disk_initialize
 * DESCRIPTION		: This file is main files.
 * Author			: XiaomaGee@Gmail.com
 * Copyright		:
 *
 * History
 * --------------------
 * Rev	: 0.00
 * Date	: 03/05/2012
 *
 * create.
 * --------------------
 */
DSTATUS disk_initialize(BYTE drv)
{
	return RES_OK;
}

/*
 * FILE				: disk_status
 * DESCRIPTION		: This file is main files.
 * Author			: XiaomaGee@Gmail.com
 * Copyright		:
 *
 * History
 * --------------------
 * Rev	: 0.00
 * Date	: 03/05/2012
 *
 * create.
 * --------------------
 */
DSTATUS disk_status(BYTE drv)
{
	return RES_OK;
}

/*
 * FILE				: disk_ioctl
 * DESCRIPTION		: This file is main files.
 * Author			: XiaomaGee@Gmail.com
 * Copyright		:
 *
 * History
 * --------------------
 * Rev	: 0.00
 * Date	: 03/05/2012
 *
 * create.
 * --------------------
 */
DRESULT disk_ioctl(BYTE drv, BYTE ctrl, void *buff)
{
	if (ctrl == GET_SECTOR_SIZE){
		*(unsigned long int*)buff = 4096;
	}

	return RES_OK;
}

/*
 * FILE				: disk_read
 * DESCRIPTION		: This file is main files.
 * Author			: XiaomaGee@Gmail.com
 * Copyright		:
 *
 * History
 * --------------------
 * Rev	: 0.00
 * Date	: 03/05/2012
 *
 * create.
 * --------------------
 */
DRESULT disk_read(BYTE drv, BYTE *buff, DWORD sector, BYTE count)
{
	int i;

	for (i = 0; i < count; i++) {
		//w25qxx.readn((sector + i) * 4096, (char*)buff + i * 4096, 4096);
		SPI_Flash_Read((u8*)buff + i * 4096,(sector + i) * 4096,4096);
	}

	return RES_OK;
}

/*
 * FILE				: disk_write
 * DESCRIPTION		: This file is main files.
 * Author			: XiaomaGee@Gmail.com
 * Copyright		:
 *
 * History
 * --------------------
 * Rev	: 0.00
 * Date	: 03/05/2012
 *
 * create.
 * --------------------
 */
DRESULT disk_write(BYTE drv, const BYTE* buff, DWORD sector, BYTE count)
{
	int i;

	for (i = 0; i < count; i++) {
		//SPI_Flash_Write(u8* pBuffer,u32 WriteAddr,u16 NumByteToWrite);//д��flash
		SPI_Flash_Write((u8*)buff + i * 4096, (sector + i) * 4096, 4096);
	}

	return RES_OK;
}
