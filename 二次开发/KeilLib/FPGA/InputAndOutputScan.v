module InputAndOutputScan_model(
	clk, 				//时钟信号，频率2M
	rst_n, 			//复位信号
	Output0, 		//输出00-15
	Output1,			//输出16-31
	Output2,			//输出32-47
	Output3,			//输出48-63
	Input0,			//输入00-15
	Input1,			//输入16-31
	Input2,			//输入32-47
	Input3,			//输入48-63
	EncodeAdr,		//编码地址
	dbus,				//双向数据线
	clkout			
);

//输入
input clk;
input rst_n;
input [15:0]	Output0;
input [15:0]	Output1;
input [15:0]	Output2;
input [15:0]	Output3;

//输出
output reg [15:0]	Input0;
output reg [15:0]	Input1;
output reg [15:0]	Input2;
output reg [15:0]	Input3;
output wire [6:0]	EncodeAdr;		//0x00-0x0F 输出；0x10-0x1F 输入 0x1F默认值
inout[15:0] dbus;
output clkout;

//寄存器变量
reg[8:0]	ClkDiv;						// 0x10周期大致为19us, 0x40周期大致为66us
reg[6:0]	adrReg;						// 内部地址计数器
reg[15:0]	OutputBuf;				// 输出缓冲
reg[7:0]	countflg;

assign clkout = ClkDiv[0];
assign dbus = (countflg < 8'h90) ? 16'hzzzz : OutputBuf;		//小于0x10时，即0x00-0x0F时输出；其余为读输入，设为高阻态
assign EncodeAdr = adrReg;


always @(posedge clk or negedge rst_n) begin
	if(!rst_n) begin
		adrReg <= 7'b111_1100;
		OutputBuf <= 16'hffff;
		
		Input0 <= 16'hffff;
		Input1 <= 16'hffff;
		Input2 <= 16'hffff;
		Input3 <= 16'hffff;
		
		countflg <= 8'h0;
	end
	else begin
		countflg <= countflg + 1'b1;
		if (countflg < 8'h10) begin
			adrReg <= 7'b110_1000;
		end
		else if(countflg < 8'h30) begin
			//Input0[15:8] <= dbus[7:0];
			Input0[15] <= dbus[0];
			Input0[14] <= dbus[1];
			Input0[13] <= dbus[2];
			Input0[12] <= dbus[3];
			Input0[11] <= dbus[4];
			Input0[10] <= dbus[5];
			Input0[9] <= dbus[6];
			Input0[8] <= dbus[7];
			
			//Input1[7:0] <= dbus[15:8];
			Input1[7] <= dbus[8];
			Input1[6] <= dbus[9];
			Input1[5] <= dbus[10];
			Input1[4] <= dbus[11];
			Input1[3] <= dbus[12];
			Input1[2] <= dbus[13];
			Input1[1] <= dbus[14];
			Input1[0] <= dbus[15];		
		end
		else if(countflg < 8'h50) begin
			adrReg <= 7'b111_1100;
		end
		else if(countflg < 8'h60) begin
			adrReg <= 7'b101_0100;
		end
		else if(countflg < 8'h70) begin
			//Input0[7:0] <= dbus[7:0];
			Input0[7] <= dbus[0];
			Input0[6] <= dbus[1];
			Input0[5] <= dbus[2];
			Input0[4] <= dbus[3];
			Input0[3] <= dbus[4];
			Input0[2] <= dbus[5];
			Input0[1] <= dbus[6];
			Input0[0] <= dbus[7];
			
			//Input1[15:8] <= dbus[15:8];
			Input1[15] <= dbus[8];
			Input1[14] <= dbus[9];
			Input1[13] <= dbus[10];
			Input1[12] <= dbus[11];
			Input1[11] <= dbus[12];
			Input1[10] <= dbus[13];
			Input1[9] <= dbus[14];
			Input1[8] <= dbus[15];
		end
		else if(countflg < 8'h90) begin
			adrReg <= 7'b111_1100;
		end
		else if(countflg < 8'hA0) begin
			OutputBuf[15:0] <= Output0;
		end
		else if(countflg < 8'hB0) begin
			adrReg <= 7'b111_1111;
		end
		else if(countflg < 8'hD0) begin
			adrReg <= 7'b111_1100;
		end
	end
end

always @(posedge clk or negedge rst_n) begin
	if(!rst_n) begin
		ClkDiv <= 9'h00;
	end
	else begin
		ClkDiv <= ClkDiv + 1'b1;
	end
end

endmodule
