module encodercount_model(
	clk, 				//时钟信号
	set, 				//设置编码器位置信号
	rst_z,			//复位Z脉冲信号的输入信号
	eca, 				//编码器输入A信号
	ecb,				//编码器输入B信号
	ecz,				//编码器输入Z信号
	inposL,			//编码器的复位值
	inposH,			//编码器的复位值
	pulse_z,			//Z脉冲信号
	encoderCountL, //脉冲个数低16位
	encoderCountH	//脉冲个数高16位
);

input 	clk;
input  	set;
input 	rst_z;
input  	eca;
input  	ecb;
input  	ecz;
input  [15:0] inposL;
input  [15:0] inposH;

output reg	pulse_z;
output reg[15:0] encoderCountL;
output reg[15:0] encoderCountH;

reg[31:0] outpos;

reg[3:0]  sta;			//编码器AB信号当前状态和上一状态寄存器变量[上一状态A_上一状态B_状态A_状态B]
reg[1:0]  last_sta;	//编码器AB上一状态[上一状态A_上一状态B]
		
always@(posedge clk )//or negedge set)
	begin	
		if(set==0)
		begin
			outpos   <= {inposH, inposL};
			sta      <= {eca,ecb,eca,ecb};
			last_sta <= {eca,ecb};
		end
		else
		begin
			sta      <= {last_sta,eca,ecb};
			last_sta <= {eca,ecb};
			case (sta)
				4'b0001:outpos <= outpos + 1;
				4'b0111:outpos <= outpos + 1;
				4'b1110:outpos <= outpos + 1;
				4'b1000:outpos <= outpos + 1;
				
				4'b0010:outpos <= outpos - 1;
				4'b1011:outpos <= outpos - 1;
				4'b1101:outpos <= outpos - 1;
				4'b0100:outpos <= outpos - 1;
			endcase
		end
	end	
	
always@(posedge clk ) begin
	encoderCountL[15:0] <= outpos[15:0];
	encoderCountH[15:0] <= outpos[31:16];
end
	
//处理Z信号
reg[1:0]  sta_z;			//编码器AB信号当前状态和上一状态寄存器变量[上一状态A_上一状态B_状态A_状态B]
reg  last_sta_z;	//编码器AB上一状态[上一状态A_上一状态B]

always @(posedge clk or negedge rst_z) begin
	if(!rst_z) begin
		pulse_z 		<= 1'b0;
		sta_z 		<= {ecz, ecz};
		last_sta_z 	<= ecz;
	end
	else begin
		sta_z <= {last_sta_z, ecz};
		last_sta_z <= ecz;
		
		if (2'b10 == sta_z) begin
			pulse_z <= 1'b1;
		end
	end
end

endmodule


