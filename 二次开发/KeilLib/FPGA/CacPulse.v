module CountPulse_model(
	clk, 			//时钟信号
	rst_n, 			//复位信号
	PulseIn, 		//脉冲输入引脚
	DirIn,			//脉冲方向输入
	PulseCountL, 	//脉冲个数低16位
	PulseCountH		//脉冲个数高16位
);

input clk;
input rst_n;
input PulseIn;
input DirIn;
output reg [15:0] PulseCountL;
output reg [15:0] PulseCountH;

reg[31:0] reg_PulseCount;

always @(posedge PulseIn or negedge rst_n) begin
	if(!rst_n) begin
		reg_PulseCount <= 32'h0;
	end
	else begin
		if(DirIn) begin
			reg_PulseCount <= reg_PulseCount + 16'd2;
		end
		else begin
			reg_PulseCount <= reg_PulseCount - 16'd2;
		end
	end
end

always @(posedge clk) begin
	PulseCountL <= reg_PulseCount[15:0];
	PulseCountH <= reg_PulseCount[31:16];
end

endmodule
