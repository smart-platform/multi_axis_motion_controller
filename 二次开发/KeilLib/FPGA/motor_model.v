module motor_model1(
	clk, rst_n, ENN, input_K_1, input_Q_1,
	POUT, pulseout
);

input clk;
input rst_n;
input ENN;
input[15:0] input_K_1;
input[15:0] input_Q_1;

output POUT;
output reg [15:0] pulseout;

reg[15:0] reg_K_1;
reg[15:0] reg_Q_1;

assign POUT = reg_Q_1[0];

//-----------鏍规嵁input_K_1纭畾鑴夊-----------------
//-----------鏍规嵁input_Q_1纭畾鑴夊啿涓暟------------
always @(posedge clk or negedge rst_n) begin
	if(!rst_n) begin
		reg_K_1 <= 0;
		reg_Q_1 <= 0;
	end
	else begin
		if(ENN == 1) begin								//鍙湁鍦‥NN==1鏃舵墠寮€濮嬭鏁拌剦鍐插搴﹀拰涓暟
			if(reg_K_1 < input_K_1) begin				//褰撹剦瀹借鏁板瘎瀛樺櫒灏忎簬杈撳叆鑴夊鏃
				reg_K_1 <= reg_K_1 + 16'b1;				//瀵勫瓨鍣ㄨ繘琛岃鏁扮疮鍔
			end
			else begin									//鑴夊璁℃暟瀵勫瓨鍣ㄧ瓑浜庤緭鍏ヨ剦瀹芥椂
				reg_K_1 <= 1;
				if(reg_Q_1 < input_Q_1) begin			//鑴夊啿涓暟璁℃暟瀵勫瓨鍣ㄥ皬浜庤緭鍏ヨ剦鍐蹭釜鏁
					reg_Q_1 <= reg_Q_1 + 16'b1;			//鑴夊啿涓暟瀵勫瓨鍣ㄥ姞1锛	
				end
			end
		end
		else begin								//ENN==0鏃
			reg_K_1 <= 0;									//鑴夊璁℃暟瀵勫瓨鍣ㄦ竻闆讹紝鏂逛究涓嬫ENN鎷夐珮閲嶆柊璁＄畻鑴夊啿瀹藉害
			reg_Q_1 <= 0;									//鑴夊啿涓暟瀵勫瓨鍣ㄦ竻闆讹紝鏂逛究涓嬫ENN鎷夐珮閲嶆柊绱姞鑴夊啿涓暟
		end
	end
end

always @(posedge clk) begin
	pulseout[15:0] = reg_Q_1[15:0];
end

endmodule
