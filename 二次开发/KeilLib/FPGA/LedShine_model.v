module LedShine_model(
	clk, rst_n,
	ledout
);

input clk;		//2M杈撳叆
input rst_n;

output ledout;

reg[23:0] reg_ledout;

assign ledout = reg_ledout[23];

always @(posedge clk or negedge rst_n) begin
	if(!rst_n) begin
		reg_ledout <= 23'h0;
	end
	else begin
		reg_ledout <= reg_ledout + 1'b1;
	end
end

endmodule
