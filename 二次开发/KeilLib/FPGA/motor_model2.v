//--------------------------------------------------------------------

module motor_model(
	clk, rst_n, ENN, input_K_1, input_Q_1, input_K_2, input_Q_2, 
	POUT, DOUT, pulseout
);

input clk;					///< 时钟输入
input rst_n;				///< 复位输入信号
input ENN;
input[15:0] input_K_1;	///< 第一部分脉冲个数输入
input[15:0] input_Q_1;	///< 第一部分脉冲宽度输入
input[15:0] input_K_2;	///< 第二部分脉冲个数输入
input[15:0] input_Q_2;	///< 第二部分脉冲宽度输入

output POUT;				///< 脉冲输出
output DOUT;				///< 方向输出
output [15:0] pulseout;		///< 测试脉冲总数使用

reg[15:0] reg_K_1;		///< 内部使用的宽度计数器
reg[15:0] reg_Q_1;		///< 内部使用的个数计数器
reg[15:0] reg_K_2;		///< 内部使用的宽度计数器
reg[15:0] reg_Q_2;		///< 内部使用的个数计数器

assign POUT = reg_Q_1[0] || reg_Q_2[0];		///< 取出脉冲
assign DOUT = input_Q_1[15];						///< 取出方向
assign pulseout = reg_Q_2 + reg_Q_1;
reg finsh_flag;			///< 为1时，表明第一部发送完成，开始发第二部分

always @(posedge clk or negedge rst_n) begin
	if(!rst_n) begin
		reg_K_1 <= 0;
		reg_Q_1 <= 0;
	end
	else begin
		if(ENN == 1) begin
			if(reg_K_1 < input_K_1) begin
				reg_K_1 <= reg_K_1 + 16'b1;
			end
			else begin
				reg_K_1 <= 1;
				if(reg_Q_1 < input_Q_1[14:0]) begin
					reg_Q_1 <= reg_Q_1 + 16'b1;
				end
			end
		end	
	end
end


always @(posedge clk or negedge rst_n) begin
	if(!rst_n) begin
		reg_K_2 <= 0;
		reg_Q_2 <= 0;
	end
	else begin
		if(ENN == 1) begin
			if(finsh_flag == 1) begin
				if(reg_K_2 < input_K_2) begin
					reg_K_2 <= reg_K_2 + 16'b1;
				end
				else begin
					reg_K_2 <= 1;
					if(reg_Q_2 < input_Q_2[14:0]) begin
						reg_Q_2 <= reg_Q_2 + 16'b1;
					end
				end
			end
		end
	end
end

always @(posedge clk or negedge rst_n) begin
	if(!rst_n) begin
		finsh_flag <= 0;
	end
	else begin
		if((ENN == 1) && (reg_Q_1 == input_Q_1[14:0]))begin
			finsh_flag <= 1;
		end
	end
end

endmodule
