module DeCodeAB(
		input				iClk,
		input				iRst,
		input[1:0]  	iAB,
		input[31:0] 	iCntPos,
		input				iSetPos,
		input			pulseswitch,
		input			pulsedir,
		output[31:0]	oCntPos 
		);
	reg[1:0]	tAB0;
	reg[1:0]	tAB1;
	always @(posedge iClk)
	begin
		tAB0<=iAB;
		tAB1<=tAB0;
	end
	
	reg[31:0] tCntPos;
	reg[1:0]	tAB;
	always @(posedge iClk or negedge iRst)
	begin
		if (iRst==0)
			begin
				tAB<='h0;
				tCntPos<='h0;
			end
		else
			begin
				tAB<=tAB1;
				if (iSetPos)
					tCntPos<=iCntPos;
				else if(pulsedir==0)
					begin
						case ({tAB,tAB1})
							4'b0001: tCntPos<=tCntPos+1;
							4'b0111: tCntPos<=tCntPos+1;
							4'b1110: tCntPos<=tCntPos+1;
							4'b1000: tCntPos<=tCntPos+1;
							
							4'b0010: tCntPos<=tCntPos-1;
							4'b1011: tCntPos<=tCntPos-1;
							4'b1101: tCntPos<=tCntPos-1;
							4'b0100: tCntPos<=tCntPos-1;
						endcase
					end
				else
					begin
						case ({tAB,tAB1})
							4'b0001: tCntPos<=tCntPos-1;
							4'b0111: tCntPos<=tCntPos-1;
							4'b1110: tCntPos<=tCntPos-1;
							4'b1000: tCntPos<=tCntPos-1;
							
							4'b0010: tCntPos<=tCntPos+1;
							4'b1011: tCntPos<=tCntPos+1;
							4'b1101: tCntPos<=tCntPos+1;
							4'b0100: tCntPos<=tCntPos+1;
						endcase
					end
			end
	end
	
wire	[1:0]	tAB_neg;
reg		[1:0]	tAB_reg;
reg		[31:0]	tPos;
assign  tAB_neg =  tAB_reg & (~tAB) ;

always @(posedge iClk or negedge iRst)
	begin
		if(iRst==0)
			begin
				tAB_reg <= 0;
			end
		else
			begin
				tAB_reg <= tAB;
			end
	end
	
always @(posedge iClk or negedge iRst)
	begin
		if (iRst==0)
			tPos<='h0;
		else if (iSetPos)
			tPos<=iCntPos;
		else 
			begin
				if(tAB_neg[1] && tAB[0])
					begin
						if(pulsedir) tPos<=tPos-1;
						else tPos<=tPos+1;
					end
				else if(tAB_neg[0] && tAB[1])
					begin
						if(pulsedir) tPos<=tPos+1;
						else tPos<=tPos-1;
					end
				else
					tPos<=tPos;
			end
	end
	
	assign oCntPos = pulseswitch ?tPos:tCntPos;
	
	
endmodule
