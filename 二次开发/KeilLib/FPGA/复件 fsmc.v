//fsmc read / write ep4ce6 demo

module fsmc(
		clk,   		//时钟输入
		db,			//数据线00-15
		wrn,		//写信号，低电平有效
		rdn,		//读信号，低电平有效
		resetn,		//复位信号，低电平有效
		csn,		//片选信号，低电平有效
		addenn,		//地址使能信号，，低电平有效
		//----------6轴编码器值------------
		in00,		//1轴编码器低16位值
		in01,		//1轴编码器高16位值 
		in02,		//2轴编码器低16位值
		in03,		//2轴编码器高16位值
		in04,		//3轴编码器低16位值
		in05,		//3轴编码器高16位值
		in06,		//4轴编码器低16位值
		in07,		//4轴编码器高16位值
		in08,		//5轴编码器低16位值
		in09,		//5轴编码器高16位值
		in0A,		//6轴编码器低16位值
		in0B,		//6轴编码器高16位值
		//----------6轴脉冲值--------------
		in0C,		//1轴所发脉冲低16位
		in0D,		//1轴所发脉冲高16位
		in0E,		//2轴所发脉冲低16位
		in0F,		//2轴所发脉冲高16位
		in10,		//3轴所发脉冲低16位
		in11,		//3轴所发脉冲高16位
		in12,		//4轴所发脉冲低16位
		in13,		//4轴所发脉冲高16位
		in14,		//5轴所发脉冲低16位
		in15,		//5轴所发脉冲高16位
		in16,		//6轴所发脉冲低16位
		in17,		//6轴所发脉冲高16位
		//----------64个输入值-------------
		in18,		//00-15号输入值
		in19,		//16-31号输入值 
		in1A, 		//32-47号输入值
		in1B,		//48-63号输入值
		//----------边沿式信号输入值-------
		in1C,		//00-15号边沿输入值
		in1D,		//16-31号边沿输入值
		//----------轴控制寄存器-----------
		out00,		//1轴第一部分脉冲个数
		out01,		//1轴第一部分脉冲宽度
		out02,		//1轴第二部分脉冲个数
		out03,		//1轴第二部分脉冲宽度
		out04,		//2轴第一部分脉冲个数
		out05,		//2轴第一部分脉冲宽度
		out06,		//2轴第二部分脉冲个数
		out07,		//2轴第二部分脉冲宽度
		out08,		//3轴第一部分脉冲个数
		out09,		//3轴第一部分脉冲宽度
		out0A,		//3轴第二部分脉冲个数
		out0B,		//3轴第二部分脉冲宽度
		out0C,		//3轴第一部分脉冲个数
		out0D,		//4轴第一部分脉冲宽度
		out0E,		//4轴第二部分脉冲个数
		out0F,		//4轴第二部分脉冲宽度
		out10,		//4轴第一部分脉冲个数
		out11,		//5轴第一部分脉冲宽度
		out12,		//5轴第二部分脉冲个数
		out13,		//5轴第二部分脉冲宽度
		out14,		//5轴第一部分脉冲个数
		out15,		//6轴第一部分脉冲宽度
		out16,		//6轴第二部分脉冲个数
		out17,		//6轴第二部分脉冲宽度
		out18,		//6轴第一部分脉冲个数
		//----------通用输出寄存器----------
		out19,		//通用输出值1低16位
		out1A,		//通用输出值1高16位
		out1B,		//通用输出值2低16位
		out1C,		//通用输出值2高16位
		//----------清中断输入（边沿）信号寄存器---------
		out1D,		//清00-15号边沿信号寄存器
		out1E,		//清16-31号边沿信号寄存器
		
		//----------轴管理寄存器----------
		outFE,		//outFE.0	1号轴编码器值清0寄存器；outFE.1		2号轴编码器值清0寄存器
					//outFE.2	3号轴编码器值清0寄存器；outFE.3		4号轴编码器值清0寄存器
					//outFE.4	5号轴编码器值清0寄存器；outFE.5		6号轴编码器值清0寄存器
					//outFE.8	1号轴脉冲记数器清0寄存器；outFE.9	2号轴脉冲记数器清0寄存器
					//outFE.10	3号轴脉冲记数器清0寄存器；outFE.11	4号轴脉冲记数器清0寄存器
					//outFE.12	5号轴脉冲记数器清0寄存器；outFE.13	6号轴脉冲记数器清0寄存器
					//outFE.15	通用输入输出复位寄存器
					
		outFF		//outFF.15 	轴复位信号；
					//outFF.0 	1号轴脉冲控制寄存器；	outFF.1 	2号轴脉冲控制寄存器；
					//outFF.2 	3号轴脉冲控制寄存器；	outFF.3 	4号轴脉冲控制寄存器；
					//outFF.4 	5号轴脉冲控制寄存器；	outFF.5 	6号轴脉冲控制寄存器；		
	);
	
	input	clk;
	inout[15:0] db;
	input wrn;
	input rdn;
	input resetn;
	input csn;
	input addenn;
	input [15:0] in00;
	input [15:0] in01;
	input [15:0] in02;
	input [15:0] in03;
	input [15:0] in04;
	input [15:0] in05;
	input [15:0] in06;
	input [15:0] in07;
	input [15:0] in08;
	input [15:0] in09;
	input [15:0] in0A;
	input [15:0] in0B;
	input [15:0] in0C;
	input [15:0] in0D;
	input [15:0] in0E;
	input [15:0] in0F;
	input [15:0] in10;
	input [15:0] in11;
	input [15:0] in12;
	input [15:0] in13;
	input [15:0] in14;
	input [15:0] in15;
	input [15:0] in16;
	input [15:0] in17;
	input [15:0] in18;
	input [15:0] in19;
	input [15:0] in1A;
	input [15:0] in1B;	
	input [15:0] in1C;
	input [15:0] in1D;	
	
	output reg [15:0] out00;
	output reg [15:0] out01;
	output reg [15:0] out02;
	output reg [15:0] out03;
	output reg [15:0] out04;
	output reg [15:0] out05;
	output reg [15:0] out06;
	output reg [15:0] out07;
	output reg [15:0] out08;
	output reg [15:0] out09;
	output reg [15:0] out0A;
	output reg [15:0] out0B;
	output reg [15:0] out0C;
	output reg [15:0] out0D;
	output reg [15:0] out0E;
	output reg [15:0] out0F;
	output reg [15:0] out10;
	output reg [15:0] out11;
	output reg [15:0] out12;
	output reg [15:0] out13;
	output reg [15:0] out14;
	output reg [15:0] out15;
	output reg [15:0] out16;
	output reg [15:0] out17;
	output reg [15:0] out18;
	output reg [15:0] out19;
	output reg [15:0] out1A;
	output reg [15:0] out1B;
	output reg [15:0] out1C;
	output reg [15:0] out1D;
	output reg [15:0] out1E;
	output reg [15:0] outFE;	
	output reg [15:0] outFF;	
	
	//-----------内部值用寄存器------------
	wire rd;
	wire wr;
	wire adden;
	reg [15:0] indata;
	reg [15:0] databuf;

	assign rd = !(csn | rdn); 			//get rd pulse  ____|~~~~|______
	assign wr = !(csn | wrn); 			//get wr pulse  ____|~~~~|______
	assign adden = !(csn | addenn);		//get adden pulse __|~~~~|______
	
	assign db = rd ? indata:16'hzzzz;
	
	always @(posedge adden or negedge resetn)
	begin
		if(!resetn) begin
			databuf <= 16'hffff;
		end
		else begin
			databuf <= db;
		end
	end
	
	//write data
	always @(negedge wr or negedge resetn)
	begin
		if(!resetn)begin
			out00 <= 16'h0000;
			out01 <= 16'h0000;
			out02 <= 16'h0000;
			out03 <= 16'h0000;
			out04 <= 16'h0000;
			out05 <= 16'h0000;
			out06 <= 16'h0000;
			out07 <= 16'h0000;
			out08 <= 16'h0000;
			out09 <= 16'h0000;
			out0A <= 16'h0000;
			out0B <= 16'h0000;
			out0C <= 16'h0000;
			out0D <= 16'h0000;
			out0E <= 16'h0000;
			out0F <= 16'h0000;
			out10 <= 16'h0000;
			out11 <= 16'h0000;
			out12 <= 16'h0000;
			out13 <= 16'h0000;
			out14 <= 16'h0000;
			out15 <= 16'h0000;
			out16 <= 16'h0000;
			out17 <= 16'h0000;
			out18 <= 16'h0000;
			out19 <= 16'h0000;
			out1A <= 16'h0000;
			out1B <= 16'h0000;
			out1C <= 16'h0000;
			out1D <= 16'h0000;
			out1E <= 16'h0000;
			outFE <= 16'h0000;
			outFF <= 16'h0000;

		end	else  begin
		case (databuf)			
			16'h0000:out00 <= db;
			16'h0001:out01 <= db;
			16'h0002:out02 <= db;
			16'h0003:out03 <= db;
			16'h0004:out04 <= db;
			16'h0005:out05 <= db;
			16'h0006:out06 <= db;
			16'h0007:out07 <= db;
			16'h0008:out08 <= db;
			16'h0009:out09 <= db;
			16'h000A:out0A <= db;
			16'h000B:out0B <= db;
			16'h000C:out0C <= db;
			16'h000D:out0D <= db;
			16'h000E:out0E <= db;
			16'h000F:out0F <= db;
			16'h0010:out10 <= db;
			16'h0011:out11 <= db;
			16'h0012:out12 <= db;
			16'h0013:out13 <= db;
			16'h0014:out14 <= db;
			16'h0015:out15 <= db;
			16'h0016:out16 <= db;
			16'h0017:out17 <= db;
			16'h0018:out18 <= db;
			16'h0019:out19 <= db;
			16'h001A:out1A <= db;
			16'h001B:out1B <= db;
			16'h001C:out1C <= db;
			16'h001D:out1D <= db;
			16'h001C:outFE <= db;
			16'h001D:outFF <= db;					
			
			default:;     
		endcase           
		end
	end
			
	//red data
	always @(posedge rd or negedge resetn)
	begin
		if(!resetn)indata <= 16'h0000;
		else  begin
		case (databuf)
			/*
			16'h0000:indata <= in00;
			16'h0001:indata <= in01;
			16'h0002:indata <= in02;
			16'h0003:indata <= in03;
			16'h0004:indata <= in04;
			16'h0005:indata <= in05;
			16'h0006:indata <= in06;
            16'h0007:indata <= in07;
            16'h0008:indata <= in08;
            16'h0009:indata <= in09;
            16'h000A:indata <= in0A;
            16'h000B:indata <= in0B;
            16'h000C:indata <= in0C;
            16'h000D:indata <= in0D;
            16'h000E:indata <= in0E;
            16'h000F:indata <= in0F;
            16'h0010:indata <= in10;
            16'h0011:indata <= in11;
            16'h0012:indata <= in12;
            16'h0013:indata <= in13;
            16'h0014:indata <= in14;
            16'h0015:indata <= in15;
            16'h0016:indata <= in16;
            16'h0017:indata <= in17;
            16'h0018:indata <= in18;
            16'h0019:indata <= in19;
            16'h001A:indata <= in1A;
            16'h001B:indata <= in1B;
            16'h001C:indata <= in1C;
            16'h001D:indata <= in1D;
			default:;
			*/
			16'h0000:indata <= out00;
			16'h0001:indata <= out01;
			16'h0002:indata <= out02;
			16'h0003:indata <= out03;
			16'h0004:indata <= out04;
			16'h0005:indata <= out05;
			16'h0006:indata <= out06;
            16'h0007:indata <= out07;
            16'h0008:indata <= out08;
            16'h0009:indata <= out09;
            16'h000A:indata <= out0A;
            16'h000B:indata <= out0B;
            16'h000C:indata <= out0C;
            16'h000D:indata <= out0D;
            16'h000E:indata <= out0E;
            16'h000F:indata <= out0F;
            16'h0010:indata <= out10;
            16'h0011:indata <= out11;
            16'h0012:indata <= out12;
            16'h0013:indata <= out13;
            16'h0014:indata <= out14;
            16'h0015:indata <= out15;
            16'h0016:indata <= out16;
            16'h0017:indata <= out17;
            16'h0018:indata <= out18;
            16'h0019:indata <= out19;
            16'h001A:indata <= out1A;
            16'h001B:indata <= out1B;
            16'h001C:indata <= out1C;
            16'h001D:indata <= out1D;		        	
		endcase
		end
	end
	
endmodule
