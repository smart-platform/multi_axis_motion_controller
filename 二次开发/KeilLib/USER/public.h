#ifndef _PUBLIC_H
#define _PUBLIC_H

// 定义数据类型
#ifndef TRUE
#define TRUE  1		///< 定义TRUE数据类型
#endif

#ifndef FALSE
#define FALSE 0		///< 定义FALSE数据类型
#endif

typedef unsigned char  uint8;      ///< 无符号8位整型变量
typedef signed   char  int8;       ///< 有符号8位整型变量
typedef unsigned short uint16;     ///< 无符号16位整型变量
typedef signed   short int16;      ///< 有符号16位整型变量
typedef unsigned int   uint32;     ///< 无符号32位整型变量
typedef signed   int   int32;      ///< 有符号32位整型变量
typedef float          fp32;       ///< 单精度浮点数(32位长度)
typedef double         fp64;       ///< 双精度浮点数(64位长度)
	
// 定义公用变量
extern uint16	Timer_led;			///< 定义LED扫描时间
extern uint16	Timer_Adc;			///< 定义处理ADC扫描时间
extern uint16	Timer_Screen;		///< 定义屏扫描时间
extern uint16	Timer_Home;			///< 定义回零线程扫描时间
extern uint16	Timer_Key;			///< 定义按键扫描时间
extern uint16	Timer_Uart;			///< 定义串口扫描线程

//extern uint16	DAVal0, DAVal1, DAVal2, DAVal3;		///< AD值变量
#endif
