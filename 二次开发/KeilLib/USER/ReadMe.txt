关于中断：
中断控制器配置中断为4个抢先式优先级，4个子优先级；
用到的中断如下：
1. 串口中断：抢占优先级为3，子优先级为3
2. 红外中断：利用的是Timer4的中断，抢占优先级为2，子优先级为3
3. 定时中断：利用的是Timer3的中断，抢占优先级为0，子优先级为3
4. USB中断： 接收中断，抢占优先级为1，子优先级为1；发送中断，抢占优先级为1，子优先级为1

2015-11-18
1. 加秘位置
	1）timer配置，串口输出
	2）直线插补和圆弧插补，PC机控制
	3）“确认”键
	4）PC保存参数

2015-11-14
1. 使能内部Flash保存系统参数

2015-11-13
1. 增加读96位芯片ID的功能

2015-09-24
修改
1. 当终点位置相同时，计算的插补长度有误

2015-08-17
修改
1. 修改在联动时，未指定轴会在运动后位置清0的问题

修改2016-01-12
1. 修改读MCU ID的BUG

修改2016-01-13
1. 修改模拟量控制部分程序，增加使能位，只要当使能位为低时，才说明读入的模拟量有效，进行轴控制
2. 增加读模拟量求平均值

修改2016-03-11
1. 修改程序，利用输入接控制第各路模拟量，IN15-A3+，IN14-A3-，IN13-A2+，IN12-A2-，IN11-A1+，IN10-A1-，IN09-A0+，IN08-A0-，速度就是各个轴的高速度；利用信号极性的.6位，表示是否用输入控制

修改2016-04-21
1. 修改程序，增加绝对方式运动（带参数、无参数）
2. 增加急停止功能，可以单独停止任意轴；也可以全部停止 
3. 修改了按位置插补的功能，增加起始速度参数
3. 增加参数停止功能
4. 整理了下发的运动指令，将带参数和不带参数的，合并成一个带参数的，在函数中判断参数的值，如果为0，则取系统参数
5. 增加在线修改速度和加速度的函数

修改2016-06-07
1. 增加对6轴底板的支持
2. 在上电输出内容中，加上底板类型支持

修改2016-09-05
1. 在查询过程中，增加返回查询的指令码

修改2016-11-16
1. 修改限位时，显示的指令位置不正确的问题
2. 修改在限位时，模拟量控制时向行爬行的问题
3. 在启动时，输出控制卡板本号

修改2016-03-21
1. 修改单轴缓停止功能，区分轴号；原来的不分轴号
