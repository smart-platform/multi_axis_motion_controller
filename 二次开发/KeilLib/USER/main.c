#include "delay.h"
#include "sys.h"
#include "LCD19264.h"
#include "usart.h"
#include "keyboard.h"
#include "timer.h"
#include "remote.h"
#include "adc.h"
#include "dma.h"

// 定义扫描时间用变量


/*************************红外测试***********/
u8 key;
u8 t=0;	
u8 *str1=0;
/********************************************/

/*************** 用于eeprom测试 **************/
const uint8_t eeprom_buffer[] = "0123456789";
#define BUFFER_SIZE		sizeof(eeprom_buffer)
uint8_t read_buffer[BUFFER_SIZE] = {0};
/******************************************/

/***************ADC + DMA*******************/
#define ADC1_DR_Address    ((uint32_t)0x4001244C)	//DMA外设ADC基地址必须有
__IO u16 AD_Value[6];								//采集到的数字量的值6路
u8 i_AD_DMA;										//串口打印循环变量
/*************************************************/

int main(void)
{

	delay_init();				//延时函数初始化	
	NVIC_Configuration();		//初始化NVIC分组
	
	// 串口初始化为9600
	uart_init(256000);
	
	/* 红外接收初始化 */
	//Remote_Init();
	
	/* IIC初始化  */
	//AT24CXX_Init();	
	
	// ADC + DMA 初始化
	DMA_Config(DMA1_Channel1,(u32)ADC1_DR_Address,(u32)&AD_Value,6);	//cndtr对应所开信道个数
	Adc_Init();
	DMA_Cmd(DMA1_Channel1, ENABLE);				//启动DMA通道
	ADC_SoftwareStartConvCmd(ADC1, ENABLE);		//软件启动AD转换	
	
	// 初始化矩阵键盘
	KeyboardInit();
	LED_Init();

	/* 1Mhz的计数频率，计数到1000为1ms */
	TIM3_Int_Init(4999,71);
	
	while(1){

	}

	/* 红外扫描 进行单步调试时可进到对应的key值当中 */	
// 	while(1)
// 	{
// 		key=Remote_Scan();	
// 		if(key){
// 			switch(key)
// 			{
// 				case 0:str="ERROR";break;			   
// 				case 162:str="POWER";break;	    
// 				case 98:str="UP";break;	    
// 				case 2:str="PLAY";break;		 
// 				case 226:str="ALIENTEK";break;		  
// 				case 194:str="RIGHT";break;	   
// 				case 34:str="LEFT";break;		  
// 				case 224:str="VOL-";break;		  
// 				case 168:str="DOWN";break;		   
// 				case 144:str="VOL+";break;		    
// 				case 104:str="1";break;		  
// 				case 152:str="2";break;	   
// 				case 176:str="3";break;	    
// 				case 48:str="4";break;		    
// 				case 24:str="5";break;		    
// 				case 122:str="6";break;		  
// 				case 16:str="7";break;			   					
// 				case 56:str="8";break;	 
// 				case 90:str="9";break;
// 				case 66:str="0";break;
// 				case 82:str="DELETE";break;		 
// 			}
// 		}else{
// 			delay_ms(10);
// 		}
// 	}	
	
	/* 测试ADC+DMA 直接串口打印 */
 	//while(0)
 	//{
 	//	for(i_AD_DMA = 0; i_AD_DMA < 6; i_AD_DMA++)			//循环6次转换AD值
 	//	{
 	//		volta[i_AD_DMA] =(float) AD_Value[i_AD_DMA]/4095*3.3;
 	//	}
 	//	printf("AD1=%d\n",AD_Value[0]);	printf("\r\n volta1 = %f V \r\n",volta[0]);
 	//	printf("AD2=%d\n",AD_Value[1]);	printf("\r\n volta2 = %f V \r\n",volta[1]);
 	//	printf("AD3=%d\n",AD_Value[2]);	printf("\r\n volta3 = %f V \r\n",volta[2]);
 	//	printf("AD4=%d\n",AD_Value[3]);	printf("\r\n volta4 = %f V \r\n",volta[3]);
 	//	printf("AD5=%d\n",AD_Value[4]);	printf("\r\n volta5 = %f V \r\n",volta[4]);
 	//	printf("AD6=%d\n",AD_Value[5]);	printf("\r\n volta6 = %f V \r\n",volta[5]);
 	//	delay_ms(1000);
 	//	delay_ms(1000);
 	//}	
	
	/* 测试EEPROM */
// 	while (1)
// 	{
// 		AT24CXX_Write(0, (uint8_t *)eeprom_buffer, BUFFER_SIZE);
// 		AT24CXX_Read(0, read_buffer, BUFFER_SIZE);
// 		
// 		delay_ms(1000);
// 		delay_ms(1000);
// 		delay_ms(1000);
// 		delay_ms(1000);
// 		delay_ms(1000);
// 	}
	
}
