// MCC4DLL.cpp : 定义 DLL 应用程序的导出函数。
//
#include "MCC4DLL.h"

// 定义 DLL 版本号
#define DllVerInfo		0x20170430

// 定义操作 MCU 的功能码
#define cmndNoMdiCmnd	0xF1		///< MDI 指令，解析 G 代码运动
#define cmndNoSetPara	0xF2		///< 设参数指令
#define	cmndNoGCodeCmnd	0xF3		///< 下传 G 运动指令参数，直接传送 G 代码
#define cmndNoManualOp	0xF4		///< 手动操作指令
#define cmndNoAskPara	0xF5		///< 查询参数指令
#define cmndNoAskInfo	0xF6		///< 查询运动信息
#define	cmndNoResetPos	0xF8		///< 清位置指令
#define	cmndNoSetOutput	0xF9		///< 输出操作类指令
#define cmndNoAnaOp		0xFA		///< 模拟量操作指令
#define	cmndNoSetLev	0xFB		///< 设置与轴相关输入信号的极性的指令
#define	cmndNoSetEnc	0xFC		///< 编码器操作指令
#define cmndNoResetZ	0xFD		///< 清 Z 脉冲操作指令
#define	cmndNoSavePara	0xFE		///< 保存系统参数到 ROM 指令
 
// 定义子操作码指令
#define subCmndNoMoveInnerDist	0x00	///< 手动移动各轴，以系统参数定义
#define subCmndNoMoveRelDist	0x03	///< 增量方式移动，用下传的参数
#define subCmndNoMoveAbsDist	0x04	///< 绝对方式移动，用下传的参数
#define	subCmndNoMoveChangeVel	0x05	///< 修改当前运动参数
#define	subCmndNoEmerncyStop	0x80	///< 急停止方式运动
#define	subCmndNoParaStop		0x81	///< 参数停止试运动
#define	subCmndNoPause			0x82	///< 暂停轴运动
#define	subCmndNoRestartMove	0x90	///< 重启轴运动
#define	subCmndNoQuitMove		0x91	///< 退出轴运动
#define subCmndNoHome			0xA1	///< 回零运动
#define	subCmndNoQuitHome		0xA2	///< 取消回零运动
#define subCmndNoResetPosAbs	0x01	///< 以绝对方式重置轴位置
#define subCmndNoResetPosRel	0x02	///< 以相对方式重置轴位置
#define	subCmndNoSetOutput		0x01	///< 设输出操作

//定义查询编码
#define askInfoPosEach			0x00    ///< 查询当前位置
#define askInfoSpdEach			0x01	///< 查询当前速度
#define askInfoAnaEach			0x02	///< 查询当前轴的模拟量值
#define askInfoEncdrEach		0x03	///< 编码器的值
#define askInfoHomSttEach		0x04	///< 查询回零的状态值
#define askInfoInputL			0x05	///< 查询输入信号0-31
#define askInfoInputH			0x06	///< 查询输入信号32-63
#define askInfoOutputL			0x07	///< 查询输出信号0-31
#define askInfoOutputH			0x08	///< 查询输出信号32-63
#define askInfoIntruptL			0x09	///< 查询中断式输入信号0-31
#define askInfoIntruptH			0x0A	///< 查询中断式输入信号32-63
#define askInfoNcStt			0x0B	///< 查询各轴的运动状态.0位（为0表示运行结束），程序段是否运动完.1位（程序段执行完发生沿变）

#define askInfoPosGrp			0x80	///< 取得6个轴的当前位置值
#define askInfoSpdGrp			0x81	///< 取得6个轴的当前速度值
#define askInfoAnaGrp			0x82	///< 取得6个轴的当前模拟量的值
#define askInfoEncdrGrp			0x83	///< 取得6个轴的编码器的值
#define askInfoMcuId			0xE0	///< 取得硬件编号，3个32位数

#define askInfoVersion			0xFF	///< 查询版本号

// 定义输出操作指令码
#define	OpCodeSetOutput			0x01	///< 置输出
#define OpCodeResetOutput		0x02	///< 清输出

// 定义函数返回值，类库的函数返回值
#define McuExeResOk				0x00	///< MCU 执行指令结果

// 定义一些通讯用的数据长度宏
#define spComFrameHeaderByte	0xAA	///< PC 下发数据帧的帧头起始标志
#define	spComFrameHeaderLen		0x04	///< PC 下发数据帧的帧头长度
#define spComDataHeaderLen		0x04	///< 下发的数据据中数据区的数据头的长度
#define spComNcSttHeaderLen		0x05	///< 下位机返回的数据的数据头的长度，包括一个字节的检验合

// 定义一些常量

#define MAX_BUFLEN_REV	0x80			///< DLL 定义的最长接收缓冲区


struct stMcCmndType{
	McCard_UINT8 spComFrameHeader;		///< 数据帧头，固定为0xAA
	McCard_UINT8	spComFrameDtLen;		///< 除帧头外的数据长度
	McCard_UINT8 spComFrameDummy;		///< 占位
	McCard_UINT8 spComFrameCheck;		///< 帧头校验位，数据长度取反

	McCard_UINT8 spComDtCheck;			///< 数据区校验和
	McCard_UINT8 spComDtCmnd;			
	McCard_UINT8 spComDtLen;				///< spComDtBuf 缓冲区中有效的数据长度
	McCard_UINT8 spComDtCmndNo;

	union uPcSendFrame{
		McCard_UINT8 spComDtBuf[0x80];
		// 手动操作指令结构体
		struct{
			McCard_UINT8 subCmnd;
			McCard_UINT8 axisId;
			McCard_INT8  spdDir;
			McCard_UINT8 dummy;
			McCard_FP32 posCmnd;
			McCard_FP32 velCmnd;
			McCard_FP32 accCmnd;
		}stManOp;

		// 读参数
		struct{
			McCard_UINT8 axisId;
			McCard_UINT8 indx;
		}stReadPara, stAskNcInfo;

		// 设置参数
		struct{
			McCard_UINT8 axisId;
			McCard_UINT8 paraIndx;
			McCard_UINT8 dummy[2];
			McCard_FP32 fVal;
		}stWritePara;

		// 重置轴位置
		struct{
			McCard_UINT8	subCmnd;
			McCard_UINT8 axisId;
			McCard_UINT16 dummy;
			McCard_FP32	posCmnd;
		}stResetPos;

		// 置输出
		struct {
			McCard_UINT8	subCmnd;
			McCard_UINT8 outputIndx;
			McCard_UINT16 dummy;
		}stSetOutput;

		// 模拟量操作指令
		struct{
			McCard_UINT8 axisId;
			McCard_UINT8 bEn;
		}stAnaOp;

		// 设置与轴相关信号的极性
		struct{
			McCard_UINT8 axisId;
			McCard_UINT8 inputType;
			McCard_UINT8 bEn;
			McCard_UINT8 dummy;
		}stSetInputLev;

		// 设置编码器值指令
		struct{
			McCard_UINT8 axisId;
			McCard_UINT8 dummy;
			McCard_UINT16 dummy2;
			McCard_INT32 encoderVal;
		}stSetEncoder;

		// 下传 G 指令结构体
		struct {
			McCard_UINT16	gCode;
			McCard_UINT16	axisEn;
			McCard_FP32		PosCmnd[4];
			McCard_FP32		VelCmnd[4];
			McCard_FP32		AccCmnd[4];
			McCard_FP32		radius;
			McCard_FP32		delayTime;
		}stGCodeCmnd;
	}uMcCmnd;
};

struct stUploadNcSttType
{
	McCard_UINT8	CmndNo;		///< 回传下发的指令号，用于检测
	McCard_UINT8	SubCmndNo;	///< 回传下发的子指令号，用于测试
	McCard_UINT8	dtLen;		///< 上传的数据的长度
	McCard_UINT8	CmndStt;	///< 回传下发指令的执行结果	
	McCard_UINT8	dtBuf[MAX_BUFLEN_REV];		///< 上传数据区
};

// 全局变量
McCard_UINT8		cmmndCnt;		///< 指令计数器，每下发指令自增1

/************************************************************************/
//定义的回调函数
#ifdef MccDllUseCallBack
pFun_SendFrame fun_CallBackSendFrame;
pFun_Delay	fun_CallBackDelay;
pFun_ReceiveFrame fun_CallBackReceiveFrame;

McCard_VOID MoCtrCard_GetSendFrameCallBack(pFun_SendFrame mfun_CallBackSendFrame)
{
	fun_CallBackSendFrame = mfun_CallBackSendFrame;
}
McCard_VOID MoCtrCard_GetDelayCallBack(pFun_Delay mfun_CallBackDelay)
{
	fun_CallBackDelay = mfun_CallBackDelay;
}
McCard_VOID MoCtrCard_GetReceiveFrameCallBack(pFun_ReceiveFrame mfun_CallBackReceiveFrame)
{
	fun_CallBackReceiveFrame = mfun_CallBackReceiveFrame;
}

#else

McCard_UINT16	fun_CallBackSendFrame(const McCard_UINT8 *pBuf, McCard_UINT32 dwLen)
{
	McCard_UINT16 tmpRes = funCallbackOk;
	if(false == Fun_Com_WriteData(pBuf, dwLen)){
		tmpRes = funCallbackErr;
	}

	return tmpRes;
}
McCard_UINT16	fun_CallBackReceiveFrame(McCard_UINT32 dwLen, McCard_UINT8 *pBuf)
{
	McCard_UINT16 tmpRes = funCallbackOk;
	McCard_UINT32 tmpLenRes = 0x00;

	if(false == Fun_Com_ReadData(pBuf, dwLen, &tmpLenRes)){
		tmpRes = funCallbackErr;
	}
	else{
		if(tmpLenRes != dwLen){
			tmpRes = funCallbackErr;
			Fun_Com_FlushInputBuffer();
		}
	}

	return tmpRes;
}
McCard_VOID		fun_CallBackDelay(McCard_VOID)
{
	return;
}

#endif
//定义的回调函数结束
/************************************************************************/

/************************************************************************/
//定义的定部函数
void fun_clearSendFrameBuffer(struct stMcCmndType *pMcCmnd)
{
	// 帧头区数据填充
	pMcCmnd->spComFrameHeader = (McCard_UINT8)spComFrameHeaderByte;
	pMcCmnd->spComFrameDtLen = 0x00;
	pMcCmnd->spComFrameDummy = 0x00;
	pMcCmnd->spComFrameCheck = 0x00;

	// 数据区数据填充
	pMcCmnd->spComDtCmnd = 0x00;
	pMcCmnd->spComDtLen = 0x00;
	pMcCmnd->spComDtCheck = 0x00;
	pMcCmnd->spComDtCmndNo = ++cmmndCnt;
}


void fun_AutoCheckSendBuff(struct stMcCmndType *pInfo)
{
	McCard_UINT8 tmpI = 0;
	McCard_UINT8 tmpCheck = 0x00;
	McCard_UINT8 tmpBufLen = 0x00;
	McCard_UINT8 *pByt = &pInfo->spComDtCheck;

	pInfo->spComFrameHeader = (McCard_UINT8)spComFrameHeaderByte;
	pInfo->spComFrameDtLen	= pInfo->spComDtLen;
	pInfo->spComFrameCheck	= ~(pInfo->spComFrameDtLen);
	pInfo->spComFrameDummy	= 0x00;

	pInfo->spComDtCheck = 0x00;
	for (tmpI = 0; tmpI < pInfo->spComDtLen; tmpI ++){
		tmpCheck += *pByt;
		pByt ++;
	}
	pInfo->spComDtCheck = tmpCheck;		// 回写校验位字节
}

/**
  * @brief  对接收到的数据进行校验检测
  *   
  * @param  mBufRec		接收到的数据的头指针
  * @param  mLen		数据缓冲区中有效的数据长度
  * @param  pRefInfo	返回指向有效数据区的指针
  * @retval 0-成功；非0-失败
  */
McCard_UINT16 fun_AutoCheckGetBuff(const McCard_UINT8 *mBufRec, McCard_UINT32 mLen, McCard_UINT8 **pResInfo)
{
	McCard_UINT16 tmpRes = 0;
	McCard_UINT8  tmpCheck = 0;
	McCard_UINT16 tmpI = 0;
#if 1
	for (tmpI = 0; tmpI < mLen - 1; tmpI ++){
		tmpCheck += mBufRec[tmpI];
	}

	if(tmpCheck == mBufRec[mLen - 1]){
#else
	if(1){
#endif
		// 校验和正确的情况下，再去验证上传数据中的指令操作结果标志位
		struct stUploadNcSttType *pSttInfo = (struct stUploadNcSttType *)mBufRec;

		if(McuExeResOk == pSttInfo->CmndStt){
			*pResInfo = (McCard_UINT8 *)(mBufRec + 4);

			tmpRes = funResOk;
		}
		else{
			tmpRes = funResErr;
		}
	}
	else{
		tmpRes = funResErr;
	}

	return tmpRes;
}


McCard_UINT16 fun_SendReceiveAndDecodeFrame(struct stMcCmndType *pStMcCmnd, McCard_UINT32 dwLenWanted, McCard_UINT8 *pResBuf)
{
	ErrInfo	tmpErr					= funResOk;		// 函数操作结果状态
	McCard_UINT8	mBufRec[MAX_BUFLEN_REV] = {0x00};		// 接收数据的临时缓冲区
	McCard_UINT8*	pResInfo;

	fun_AutoCheckSendBuff(pStMcCmnd);
	tmpErr = (ErrInfo)fun_CallBackSendFrame((McCard_UINT8 *)pStMcCmnd, pStMcCmnd->spComDtLen + spComFrameHeaderLen);
	if(funCallbackOk == tmpErr){
		fun_CallBackDelay();
		tmpErr = (ErrInfo)fun_CallBackReceiveFrame(dwLenWanted + spComNcSttHeaderLen, mBufRec);
		if(funCallbackOk == tmpErr){
			tmpErr = (ErrInfo)fun_AutoCheckGetBuff(mBufRec, dwLenWanted + spComNcSttHeaderLen, &pResInfo);
			if(funResOk == tmpErr){
				if(cmndNoAskInfo == pStMcCmnd->spComDtCmnd){
					if((askInfoPosEach == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) || (askInfoSpdEach == pStMcCmnd->uMcCmnd.stAskNcInfo.indx)){
						// 读单个位置
						McCard_FP32 *pTmpRes = (McCard_FP32 *)pResBuf;
						McCard_FP32 *pTmpInfo = (McCard_FP32 *)pResInfo;

						*pTmpRes = *pResInfo;
					}
					else if((askInfoPosGrp == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) || (askInfoSpdGrp == pStMcCmnd->uMcCmnd.stAskNcInfo.indx)){
						// 读轴组位置
						McCard_FP32 *pTmpRes = (McCard_FP32 *)pResBuf;
						McCard_FP32 *pTmpInfo = (McCard_FP32 *)pResInfo;
						McCard_INT8 tmpI = 0;

						for (tmpI = 0; tmpI < MAX_AXIS_NUM; tmpI ++){
							*(pTmpRes + tmpI) = *(pTmpInfo + tmpI);
						}
					}
					else if((askInfoNcStt == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) || (askInfoAnaEach == pStMcCmnd->uMcCmnd.stAskNcInfo.indx)){
						// 读系统状态
						McCard_INT32 *pTmpRes = (McCard_INT32 *)pResBuf;
						McCard_INT32 *pTmpInfo = (McCard_INT32 *)pResInfo;

						*pTmpRes = *pTmpInfo;
					}
					else if((askInfoOutputL == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) || (askInfoOutputH == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) ||
						(askInfoInputL == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) || (askInfoInputH == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) || 
						(askInfoIntruptL == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) || (askInfoIntruptH == pStMcCmnd->uMcCmnd.stAskNcInfo.indx) ||
						(askInfoVersion == pStMcCmnd->uMcCmnd.stAskNcInfo.indx))
					{
						McCard_UINT32 *pTmpRes = (McCard_UINT32 *)pResBuf;
						McCard_UINT32 *pTmpInfo = (McCard_UINT32 *)pResInfo;

						*pTmpRes = *pTmpInfo;
					}
					else if(askInfoAnaGrp == pStMcCmnd->uMcCmnd.stAskNcInfo.indx){
						McCard_INT32 *pTmpRes = (McCard_INT32 *)pResBuf;
						McCard_INT32 *pTmpInfo = (McCard_INT32 *)pResInfo;
						McCard_INT8 tmpI = 0;

						for (tmpI = 0; tmpI < MAX_AXIS_NUM; tmpI ++){
							*(pTmpRes + tmpI) = *(pTmpInfo + tmpI);
						}
					}
					else if(askInfoMcuId == pStMcCmnd->uMcCmnd.stAskNcInfo.indx){
						McCard_UINT32 *pTmpRes = (McCard_UINT32 *)pResBuf;
						McCard_UINT32 *pTmpInfo = (McCard_UINT32 *)pResInfo;

						*pTmpRes = *pTmpInfo;
						*(pTmpRes + 1) = *(pTmpInfo + 1);
						*(pTmpRes + 2) = *(pTmpInfo + 2);
					}
				}
				else if(cmndNoAskPara == pStMcCmnd->spComDtCmnd){
					McCard_FP32 *pTmpRes	= (McCard_FP32 *)pResBuf;
					McCard_FP32 *pTmpInfo	= (McCard_FP32 *)pResInfo;

					*pTmpRes = *pTmpInfo;
				}
			}
		}
	}

	return tmpErr;
}




// 定义的内部函数结束
/************************************************************************/

McCard_UINT16 MoCtrCard_Initial(McCard_UINT8 ComPort)
{
	cmmndCnt	= 0x00;

	//cyj20170521 if(Fun_Com_InitPort(ComPort, 115200)){
	if(1){
		return	funResOk;
	}
	else{
		return	funResOpenPortErr;
	}
}

McCard_UINT16 MoCtrCard_Unload()
{
	//cyj20170521 Fun_Com_ClosePort();
	return funResOk;
}

/**
  * @brief  读控制器的位置
  *   
  * @param  AxisId		轴号
  * @param  ResPos		轴位置
  * @retval 0-成功；非0-失败
  */
McCard_UINT16 MoCtrCard_GetAxisPos(McCard_UINT8 AxisId, McCard_FP32 ResPos[])
{	
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体
	
	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = AxisId;
	
	if(AxisId < MAX_AXIS_NUM){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoPosEach;
		dtLenWanted = 4;
	}
	else if(AxisId == 0xFF){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoPosGrp;
		dtLenWanted = MAX_AXIS_NUM * 4;
	}
	else{
		tmpErr = funResErrAxisId;
	}

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResPos);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_GetAxisSpd(McCard_UINT8 AxisId, McCard_FP32 ResSpd[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = AxisId;

	if(AxisId < MAX_AXIS_NUM){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoSpdEach;
		dtLenWanted = 4;
	}
	else if(AxisId == 0xFF){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoSpdGrp;
		dtLenWanted = MAX_AXIS_NUM * 4;
	}
	else{
		tmpErr = funResErrAxisId;
	}

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResSpd);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_GetRunState(McCard_INT32 ResStt[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = 0x00;
	stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoNcStt;
	dtLenWanted = 4;

	stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
	tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResStt);

	return tmpErr;
}


McCard_UINT16 MoCtrCard_GetAdVal(McCard_UINT8 AxisId, McCard_INT32 ResAd[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = AxisId;

	if(AxisId < MAX_AXIS_NUM){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoAnaEach;
		dtLenWanted = 4;
	}
	else if(AxisId == 0xFF){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoAnaGrp;
		dtLenWanted = MAX_AXIS_NUM * 4;
	}
	else{
		tmpErr = funResErrAxisId;
	}

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResAd);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_GetOutState(McCard_UINT8 OutGrpIndx, McCard_UINT32 ResOut[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = 0x00;

	if(0 == OutGrpIndx){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoOutputL;
		dtLenWanted = 4;
	}
	else if(1 == OutGrpIndx){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoOutputH;
		dtLenWanted = 4;
	}
	else{
		tmpErr = funResErrOutGrpId;
	}

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResOut);
	}

	return tmpErr;
}

McCard_UINT16 MoCtrCard_GetInputState(McCard_UINT8 InGrpIndx, McCard_UINT32 ResIn[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = 0x00;

	if(0 == InGrpIndx){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoInputL;
		dtLenWanted = 4;
	}
	else if(1 == InGrpIndx){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoInputH;
		dtLenWanted = 4;
	}
	else{
		tmpErr = funResErrInGrpId;
	}

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResIn);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_GetIntInputState(McCard_UINT8 InGrpIndx, McCard_UINT32 ResIn[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = 0x00;

	if(0 == InGrpIndx){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoIntruptL;
		dtLenWanted = 4;
	}
	else if(1 == InGrpIndx){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoIntruptH;
		dtLenWanted = 4;
	}
	else{
		tmpErr = funResErrInterGrpId;
	}

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResIn);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_GetCardVersion(McCard_UINT32 ResVer[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = 0x00;
	stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoVersion;
	dtLenWanted = 4;

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ResVer);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_GetEncoderVal(McCard_UINT8 AxisId, McCard_INT32 EncoderPos[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = AxisId;

	if(AxisId < MAX_AXIS_NUM){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoEncdrEach;
		dtLenWanted = 4;
	}
	else if(0xFF == AxisId){
		stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoEncdrGrp;
		dtLenWanted = MAX_AXIS_NUM * 4;
	}
	else{
		tmpErr = funResErrAxisId;
	}

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)EncoderPos);
	}

	return tmpErr;
}

McCard_UINT16 MoCtrCard_GetBoardHardInfo(McCard_UINT32 HardInfo[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskInfo;
	stMcCmnd.uMcCmnd.stAskNcInfo.axisId = 0x00;
	stMcCmnd.uMcCmnd.stAskNcInfo.indx = askInfoMcuId;
	dtLenWanted = 12;

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAskNcInfo);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)HardInfo);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_SendPara(McCard_UINT8 AxisId, McCard_UINT8 ParaIndx, McCard_FP32 ParaVal)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoSetPara;
	stMcCmnd.uMcCmnd.stWritePara.axisId = AxisId;
	stMcCmnd.uMcCmnd.stWritePara.paraIndx = ParaIndx;
	stMcCmnd.uMcCmnd.stWritePara.fVal = ParaVal;
	stMcCmnd.uMcCmnd.stWritePara.dummy[0] = 0x00;
	stMcCmnd.uMcCmnd.stWritePara.dummy[1] = 0x00;
	dtLenWanted = 0;	

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stWritePara);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_ReadPara(McCard_UINT8 AxisId, McCard_UINT8 ParaIndx, McCard_FP32 ParaVal[])
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoAskPara;
	stMcCmnd.uMcCmnd.stReadPara.axisId = AxisId;
	stMcCmnd.uMcCmnd.stReadPara.indx = ParaIndx;
	
	dtLenWanted = 4;	

	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stReadPara);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, (McCard_UINT8 *)ParaVal);
	}

	return tmpErr;
}



McCard_UINT16 MoCtrCard_MCrlAxisMove(McCard_UINT8 AxisId, McCard_INT8 SpdDir)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd = (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoMoveInnerDist;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= SpdDir;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= 0.0;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= 0.0;
	
	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}

McCard_UINT16 MoCtrCard_MCrlAxisRelMove(McCard_UINT8 AxisId, McCard_FP32 DistCmnd, McCard_FP32 VCmnd, McCard_FP32 ACmnd)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoMoveRelDist;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= DistCmnd;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= VCmnd;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= ACmnd;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}

McCard_UINT16 MoCtrCard_MCrlAxisAbsMove(McCard_UINT8 AxisId, McCard_FP32 PosCmnd, McCard_FP32 VCmnd, McCard_FP32 ACmnd)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoMoveAbsDist;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= PosCmnd;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= VCmnd;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= ACmnd;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}

McCard_UINT16 MoCtrCard_SeekZero(McCard_UINT8 AxisId, McCard_FP32 VCmnd, McCard_FP32 ACmnd)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoHome;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= VCmnd;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= ACmnd;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_CancelSeekZero(McCard_UINT8 AxisId)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoQuitHome;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= 0.0f;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_ReStartAxisMov(McCard_UINT8 AxisId)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoRestartMove;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= 0.0f;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_PauseAxisMov(McCard_UINT8 AxisId)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoPause;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= 0.0f;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_StopAxisMov(McCard_UINT8 AxisId, McCard_FP32 fAcc)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoParaStop;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= fAcc;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_ChangeAxisMovPara(McCard_UINT8 AxisId, McCard_FP32 fVel, McCard_FP32 fAcc)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoMoveChangeVel;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= fVel;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= fAcc;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_EmergencyStopAxisMov(McCard_UINT8 AxisId)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoEmerncyStop;
	stMcCmnd.uMcCmnd.stManOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= 0.0f;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_QuiteMotionControl(McCard_VOID)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoManualOp;
	stMcCmnd.uMcCmnd.stManOp.subCmnd	= subCmndNoQuitMove;
	stMcCmnd.uMcCmnd.stManOp.axisId		= 0;
	stMcCmnd.uMcCmnd.stManOp.spdDir		= 0;
	stMcCmnd.uMcCmnd.stManOp.dummy		= 0;
	stMcCmnd.uMcCmnd.stManOp.posCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.velCmnd	= 0.0f;
	stMcCmnd.uMcCmnd.stManOp.accCmnd	= 0.0f;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}

McCard_UINT16 MoCtrCard_SaveSystemParaToROM(McCard_VOID)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoSavePara;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen;
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_SetOutput(McCard_UINT8 OutputIndex, McCard_UINT8 OutputVal)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd					= (McCard_UINT8)cmndNoSetOutput;
	stMcCmnd.uMcCmnd.stSetOutput.subCmnd	= (OutputVal > 0) ? 1 : 2;
	stMcCmnd.uMcCmnd.stSetOutput.outputIndx	= OutputIndex;
	stMcCmnd.uMcCmnd.stSetOutput.dummy		= 0;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stSetOutput);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_SetJoyStickEnable(McCard_UINT8 AxisId, McCard_UINT8 bEnable)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoAnaOp;
	stMcCmnd.uMcCmnd.stAnaOp.axisId		= AxisId;
	stMcCmnd.uMcCmnd.stAnaOp.bEn		= bEnable;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stAnaOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_SetAxisRealtiveInputPole(McCard_UINT8 AxisId, McCard_UINT8 InputType, McCard_UINT8 OpenOrClose)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd					= (McCard_UINT8)cmndNoSetLev;
	stMcCmnd.uMcCmnd.stSetInputLev.axisId	= AxisId;
	stMcCmnd.uMcCmnd.stSetInputLev.bEn		= OpenOrClose;
	stMcCmnd.uMcCmnd.stSetInputLev.inputType= InputType;
	stMcCmnd.uMcCmnd.stSetInputLev.dummy	= 0;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stSetInputLev);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_SetEncoderPos(McCard_UINT8 AxisId, McCard_INT32 EncoderPos)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd					= (McCard_UINT8)cmndNoSetEnc;
	stMcCmnd.uMcCmnd.stSetEncoder.axisId	= AxisId;
	stMcCmnd.uMcCmnd.stSetEncoder.dummy		= 0;
	stMcCmnd.uMcCmnd.stSetEncoder.dummy2	= 0;
	stMcCmnd.uMcCmnd.stSetEncoder.encoderVal= EncoderPos;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stSetEncoder);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}

McCard_UINT16 MoCtrCard_ResetCoordinate(McCard_UINT8 AxisId, McCard_FP32 PosRest)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd				= (McCard_UINT8)cmndNoResetPos;
	stMcCmnd.uMcCmnd.stResetPos.subCmnd= subCmndNoResetPosAbs;
	stMcCmnd.uMcCmnd.stResetPos.axisId	= AxisId;
	stMcCmnd.uMcCmnd.stResetPos.dummy	= 0;
	stMcCmnd.uMcCmnd.stResetPos.posCmnd= PosRest;

	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen + sizeof(stMcCmnd.uMcCmnd.stManOp);
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT16 MoCtrCard_RstZ(McCard_UINT8 AxisId)
{
	ErrInfo				tmpErr		= funResOk;
	McCard_UINT8		dtLenWanted	= 0;			// 需要上传的数据长度，不包括数据头和校验位（5）
	struct stMcCmndType stMcCmnd;					// 临时下传指令结构体

	fun_clearSendFrameBuffer(&stMcCmnd);
	stMcCmnd.spComDtCmnd			= (McCard_UINT8)cmndNoResetZ;
	
	dtLenWanted = 0;	
	if(funResOk == tmpErr){
		stMcCmnd.spComDtLen = spComDataHeaderLen;
		tmpErr = (ErrInfo)fun_SendReceiveAndDecodeFrame(&stMcCmnd, dtLenWanted, 0);
	}

	return tmpErr;
}


McCard_UINT32 MoCtrCard_GetDLLVersion(McCard_VOID)
{
	return DllVerInfo;
}


McCard_UINT16 MoCtrCard_Test(McCard_INT32 TestVar[])
{
	TestVar[0] = 1;
	TestVar[1] = 2;
	TestVar[2] = 3;
	TestVar[3] = 4;

	return 55;
}
