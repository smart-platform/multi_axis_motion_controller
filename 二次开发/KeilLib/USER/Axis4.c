#include "stdio.h"
#include "string.h"
#include "timer.h"
#include "delay.h"
#include "sys.h"
#include "usart.h"	  
#include "MCC4DLL.h"
/* 
*********************************************************************************************************
** 函数名称 ：main()
** 函数功能 ：使用定时器实现1秒钟定时，控制LED9闪烁。中断方式。
** 调试说明 ：需要将跳线JP11连接BEEP。
*********************************************************************************************************
*/

McCard_UINT16 mFun_SendFrame(const McCard_UINT8 *pBuf, McCard_UINT32 dwLen)
{
	McCard_UINT8 tmpI = 0;
	for (tmpI = 0; tmpI < dwLen; tmpI ++){
		printf("%x ", pBuf[tmpI]);
	}
	
	printf("\n");
	return 0;
}

McCard_UINT16 mpFun_ReceiveFrame(McCard_UINT32 dwLen, McCard_UINT8 *pBuf)
{
	return 0;
}
McCard_VOID mpFun_Delay(McCard_VOID)
{
	delay_ms(200);
}

int main(void)
{
	//初始化相关硬件
	delay_init();				// 延时函数初始化
	NVIC_Configuration();		// 初始化NVIC分组
	//uart_init(115200);			// 串口初始化为115200
	delay_ms(1000);
	//TIM3_Int_Init(4999,71);		// 1Mhz的计数频率，计数到1000为1ms

	MoCtrCard_GetSendFrameCallBack(mFun_SendFrame);
	MoCtrCard_GetDelayCallBack(mpFun_Delay);
	MoCtrCard_GetReceiveFrameCallBack(mpFun_ReceiveFrame);

	while(1){
		McCard_FP32 tmpPos[6];
		MoCtrCard_GetAxisPos(0, tmpPos);
	}
}