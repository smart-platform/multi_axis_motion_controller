﻿using System.Text.RegularExpressions;
//#define DEBUGOUT
// #define	FIFO_LENGTH 80
// #define FIFOFULL		1
// #define FIFOEMPTY		2
// #define	FIFONOTEMPTY	3
// #define	FIFONOTFULL		4

namespace SerialPortLibrary
{

    using System;
    using System.Text;
    using System.IO.Ports;
    using System.Threading;
    using System.Diagnostics;
    using System.Runtime.InteropServices;           // 包括这个，使用ClassInterface

    enum UartReceiveSttType { DataArrived, NoDataToRead, PortNotOpen, OtherEx };
    enum UartSendCmmndResType { NodataToRead, PortNotOpen, SendTimesBig5, CmmndOK, CmnndParaErr, NotSupportCmmnd, OtherEx };

    public interface ITestClass
    {
        System.UInt16 MoCtrCard_Initial(System.String SerialPortName);
        System.UInt32 MoCtrCard_GetBoardHardInfo(System.UInt32[] HardInfo);
        System.UInt16 MoCtrCard_Unload();
        System.UInt16 MoCtrCard_GetAxisPos(System.Byte AxisId, System.Single[] ResPos);
        System.UInt16 MoCtrCard_GetAxisSpd(System.Byte AxisId, System.Single[] ResSpd);
        System.UInt16 MoCtrCard_GetRunState(System.Int32[] ResStt);
        System.UInt16 MoCtrCard_GetAdVal(System.Byte AxisId, System.Int32[] ResAd);
        System.UInt16 MoCtrCard_GetOutState(System.Byte OutGrpIndx, System.UInt32[] ResOut);
        System.UInt16 MoCtrCard_GetInputState(System.Byte InGrpIndx, System.UInt32[] ResIn);
        System.UInt16 MoCtrCard_SendPara(System.Byte AxisId, System.Byte ParaIndx, System.Single ParaVal);
        System.UInt16 MoCtrCard_MCrlAxisMove(System.Byte AxisId, System.SByte SpdDir);
        System.UInt16 MoCtrCard_MCrlAxisRelMove(System.Byte AxisId, System.Single DistCmnd, System.Single VCmnd, System.Single ACmnd);
        System.UInt16 MoCtrCard_MCrlAxisRelMove(System.Byte AxisId, System.Single DistCmnd);
        System.UInt16 MoCtrCard_MCrlAxisAbsMove(System.Byte AxisId, System.Single PosCmnd, System.Single VCmnd, System.Single ACmnd);
        System.UInt16 MoCtrCard_MCrlAxisAbsMove(System.Byte AxisId, System.Single PosCmnd);
        System.UInt16 MoCtrCard_SeekZero(System.Byte AxisId, System.Single VCmnd, System.Single ACmnd);
        System.UInt16 MoCtrCard_SeekZero(System.Byte AxisId);
        System.UInt16 MoCtrCard_CancelSeekZero(System.Byte AxisId);
        System.UInt16 MoCtrCard_ResetCoordinate(System.Byte AxisId, System.Single RestPos);
        System.UInt16 MoCtrCard_ReStartAxisMov(System.Byte AxisId);
        System.UInt16 MoCtrCard_PauseAxisMov(System.Byte AxisId);
        System.UInt16 MoCtrCard_StopAxisMov();

        System.UInt16 MoCtrCard_ChangeAxisMovPara(System.Byte AxisId, System.Single fVel, System.Single fAcc);
        System.UInt16 MoCtrCard_EmergencyStopAxisMov(System.Byte AxisId);
        System.UInt16 MoCtrCard_ReadPara(System.Byte AxisId, System.Byte ParaIdndx, System.Single[] ParaVal);
        System.UInt16 MoCtrCard_SaveSystemParaToROM();
        System.UInt16 MoCtrCard_SetOutput(System.Byte OutputIndex, System.Boolean OutputVal);
        System.UInt16 MoCtrCard_SetJoyStickEnable(System.Byte AxisId, System.Byte bEnable);
        System.UInt16 MoCtrCard_SetAxisRealtiveInputPole(System.Byte AxisId, System.Byte InputType, System.Byte OpenOrClose);
        System.UInt16 MoCtrCard_QuiteMotionControl();
        System.UInt32 MoCtrCard_GetDLLVersion();
        System.UInt32 MoCtrCard_GetCardVersion(System.UInt32[] ResVersion);
        System.UInt32 MoCtrCard_GetEncoderVal(System.Byte AxisId, System.Int32[] EncoderPos);
        System.UInt32 MoCtrCard_SetEncoderPos(System.Byte AxisId, System.Int32 EncoderPos);
        System.UInt32 MoCtrCard_RstZ(System.UInt16 AxisIndex);
        System.UInt16 MoCtrCard_Test(System.Int32[] EncoderPos);
    }

    [ClassInterface(ClassInterfaceType.None)]
    public class SPLibClass : ITestClass
    {
        //DLL的版本号
        private const UInt32 DLL_VER = 0x20160421;          //修改日期

        // 上位机操作MCU的指令码
        private const byte CMMND_MDICMND = 0xF1;	///< MDI指令，解析g代码运动
        private const byte CMMND_SETPARA = 0xF2;	///< 设参数
        private const byte CMMND_GCODECMND = 0xF3;	///< 下传G运动指令运动参数
        private const byte CMMND_MANUAL_OP = 0xF4;	///< 手动操作
        private const byte CMMND_ASKPARA = 0xF5;	///< 查询参数
        private const byte CMMND_ASKMOVEINFO = 0xF6;	///< 查询运动信息
        private const byte CMMND_SYSTEM_OP = 0xF7;	///< 系统操作指令，联接USB等
        private const byte CMMND_RESETPOS = 0xF8;	///< 清轴位置指令
        private const byte CMMND_SETOUTPUT = 0xF9;	///< 输出操作类指令
        private const byte CMMND_ANALOGOP = 0xFA;	///< 模拟量操作指令
        private const byte CMMND_SETINPUTLEV = 0xFB;	///< 与轴相关的输入信号的级性原点，限位
        private const byte CMMND_SETENCODER = 0xFC;	///< 编码器操作指令
        private const byte CMMND_RESETZ = 0xFD;	///< 清Z脉冲信号
        private const byte CMMND_SAVESYSPARA = 0xFE;	///< 保存系统参数

        // 定义子操作码
        private const byte MANOP_MOVEINNERDIST = 0x00;	///< 手动移动各个轴参数规定的距离
        private const byte MANOP_MOVERELDIST = 0x03;	///< 增量方式移动，用下传的速度和加速度参数
        private const byte MANOP_MOVEABSDIST = 0x04;	///< 约对方式移动，用下传的速度和加速度参数
        private const byte MANOP_MOVECHAGEVEL = 0x05;	///< 修改当前运动参数                                                        ///
        private const byte MANOP_EMGERNCYSTOP = 0x80;	///< 急停方式运动
        private const byte MANOP_PARASTOP = 0x81;	///< 参数停止方式运动
        private const byte MANOP_PAUSE = 0x82;	///< 轴暂停
        private const byte MANOP_RESTARTMOVE = 0x90;	///< 重启轴的运动
        private const byte MANOP_QUITEMOVE = 0x91;	///< 退出轴运动
        private const byte MANOP_HOMING = 0xA1;	///< 回零运动，用下传的速度和加速度参数
        private const byte MANOP_HOMINGCANCLE = 0xA2;	///< 取消回零运动并停止
        private const byte RESETPOS_ABS = 0x01;	///< 以绝对方式重置轴位置
        private const byte RESETPOS_REL = 0x02;	///< 以相对方式重置轴位置
        private const byte MANAGE_SETOUTPUT = 0x01;	///< 设输出操作


        private const byte CMND_ENAXISMOVE = 0xF1;          //使能轴运动
        private const byte CMND_SETPARA = 0xF2;             //设参数
        private const byte CMND_SETGCODEPARA = 0xF3;        //下传G运动指令运动参数
        private const byte CMND_MANUAL_OP = 0xF4;           //手动操作
        private const byte CMND_ASKPARA = 0xF5;             //查询系统参数
        private const byte CMND_ASKMOVEINFO = 0xF6;         //查询运动信息
        private const byte CMND_ASKLASTCMMNDSTT = 0xF7;     //查询上一次指令执行的状态
        private const byte CMND_NOTSUPPORTCMND = 0xFE;      //不支持的指令
        private const byte CMND_DECODECMND = 0xFF;          //解析指令


        //enum eIntrpltInfoIndexTyp
        private const byte AskInfo_Pos_Each = 0x00;	    ///< 查询当前位置
        private const byte AskInfo_Spd_Each = 0x01;	    ///< 查询当前速度
        private const byte AskInfo_Ana_Each = 0x02;	    ///< 查询当前轴的模拟量值
        private const byte AskInfo_Encdr_Each = 0x03;	///< 编码器的值
        private const byte AskInfo_HomStt_Each = 0x04;	///< 查询回零的状态值
        private const byte AskInfo_Input_L = 0x05;	    ///< 查询输入信号0-31
        private const byte AskInfo_Input_H = 0x06;	    ///< 查询输入信号32-63
        private const byte AskInfo_Output_L = 0x07;	    ///< 查询输出信号0-31
        private const byte AskInfo_Output_H = 0x08;	    ///< 查询输出信号32-63
        private const byte AskInfo_Intrupt_L = 0x09;	///< 查询中断式输入信号0-31
        private const byte AskInfo_Intrupt_H = 0x0A;	///< 查询中断式输入信号32-63
        private const byte AskInfo_NcStt = 0x0B;	    ///< 查询各轴的运动状态.0位（为0表示运行结束），程序段是否运动完.1位（程序段执行完发生沿变）

        private const byte AskInfo_Pos_Grp = 0x80;	    ///< 取得6个轴的当前位置值
        private const byte AskInfo_Spd_Grp = 0x81;	    ///< 取得6个轴的当前速度值
        private const byte AskInfo_Ana_Grp = 0x82;	    ///< 取得6个轴的当前模拟量的值
        private const byte AskInfo_Encdr_Grp = 0x83;	///< 取得6个轴的编码器的值
        private const byte AskInfo_McuId = 0xE0;        ///< 取得硬件编号，3个32位数

        private const byte AskInfo_Version = 0xFF;	    ///< 查询版本号

        // enum eIoOperateTyp
        private const byte OutputSet = 1;	            ///< 置输出
        private const byte OutputReset = 2;		        ///< 清输出


        //指令执行结果
        private const byte CMND_EXECUTE_OK = 0x00;          //指令执行成功，其它非0值不成功，为错误ID号
        private const byte CMND_EXECUTE_ERR = 0xAA;         //指令执行不成功

        private const byte MCU_CMND_EXECUTE_OK = 0x00;      // 指令执行成功，其它非0值不成功，为错误ID号


        //指令执行相关信息
        private const byte CMND_INFO_NOTHING = 0x0;
        private const byte CMND_INFO_SYSRUNNING = 0x1;
        private const byte CMND_INFO_INDEXERROR = 0x2;      //索引错误
        private const byte CMND_INFO_CMNDCODEERR = 0x3;     //指令码错误
        private const byte CMND_INFO_OUTPUTINDEX = 0x4;     //输出口序号有误
        private const byte CMND_INFO_OPCMNDINDEX = 0x5;     //手动操作码有误
        private const byte CMND_INFO_JIAOYANHE = 0x6;       //校验和错误
        private const byte CMND_INFO_INVALIDCMND = 0x7;     //下传指令格式有误

        //定义串口数据的帧头和帧尾巴
        private const byte SPCOMFRAMEHEADR = 0xAA;          ///< 定义串行数据通讯帧头
        private const byte SPCOMFRAMEHEADRLEN = 0x04;       ///< 串行数据通讯帧头的数据的长度
        private const byte SPCOMDATAHEADRLEN = 0x04;        ///< 串行数据通讯数据区的数据头的长度
        private const byte SPCOMNCSTTHEADRLEN = 0x04;       ///< 下位机返回的状态数据的头长度

        private const byte FRAME_HEAD = 0xF5;
        private const byte FRAME_TAIL = 0x5A;
        private const byte INPUT_CMMND_SIZE = 12;           //定义下载数据帧长度     
        private const byte OUTPUT_CMMND_SIZE = 14;          //定义上传数据帧长度


        //函数操作返回值
        private const byte FUNRES_OK = 0x1;
        private const byte FUNRES_ERR = 0x2;

        // 公有常量信息
        private const byte MAX_AXIS_NUM = 4;   ///< 定义最大轴数
        private const byte PARA_NUM_PER_AXIS = 8;  ///< 每个轴的参数个数
        private const byte PARA_SUM = MAX_AXIS_NUM * PARA_NUM_PER_AXIS; ///< 参数总数
        //定义使用得到的变量
        byte CmmndIndex;                                                    //指令编号，该变量在下发指令后自增，并且在返回数据时，将该指令返回，以检查指令是否发送成功

        struct stMcCmndType
        {
            public byte u8SpCom_Headr;
            public byte u8SpCom_DtLen;
            public byte u8SpCom_Dumy;
            public byte u8SpCom_Check;

            public byte u8SpDt_CheckSum;
            public byte u8SpDt_CmndType;
            public byte u8SpDt_Len;
            public byte u8SpDt_CmndNo;
            //public byte[] u8Buf;          
            unsafe public fixed byte u8SpDt_Buf[0x80];
            public stMcCmndType(byte para1)
            {
                u8SpCom_Headr = 0xAA;
                u8SpCom_DtLen = 0x00;
                u8SpCom_Dumy = 0x00;
                u8SpCom_Check = 0x00;
                u8SpDt_CheckSum = 0;
                u8SpDt_CmndType = 0;
                u8SpDt_Len = 0;
                u8SpDt_CmndNo = 0;
            }
        }

        struct stUploadNcSttType
        {
            public byte u8CmndtType;		///< 下发的命令
            public byte u8SubCmnd;			///< 下发的子指令
            public byte u8Len;				///< 要上传的数据长度
            public byte u8CmndStt;			///< 下发的命令的执行结果            
            unsafe public fixed byte u8Buf[0x80];	        ///< 数据区

            public stUploadNcSttType(byte para1)
            {
                u8CmndtType = 0;
                u8SubCmnd = 0;
                u8Len = 0;
                u8CmndStt = 0;
            }
        }

        struct stMcCmnd_ManulOpTyp
        {
            public byte SubCmnd;
            /// @var	SubCmnd
            /// @brief	手动运动方式
            /// @note	0x00	手动移动各个轴参数规定的距离
            /// @note	0x01	增量方式移动，用内部速度和加速度参
            /// @note	0x02	约对方式移动，用内部速度和加速度参
            /// @note	0x03	增量方式移动，用下传的速度和参数
            /// @note	0x04	约对方式移动，用下传的速度和参数
            public byte AxisID;		///< 轴号
            public sbyte SpdDir;	///< 速度方向
            public byte dummy;
            public float PosCmd;	///< 手动操作时，目标位置
            public float VelCmd;	///< 指令速度
            public float AccCmd;	///< 指令加速度
        }

        struct stMcCmnd_ReadParaTyp
        {
            public byte AxisId;     ///< 轴序号（0-5为轴参数，6为系统参数）
            public byte ParaIndex;  ///< 参数序号
        }

        struct stMcCmnd_SetParaTyp
        {
            public byte AxisId;     ///< 轴序号（0-5为轴参数，6为系统参数）
            public byte ParaIndex;  ///< 参数序号
            public Single fVal;     ///< 参数值
        }

        struct stMcCmnd_AskSttInfoTyp
        {
            public byte AxisId;     ///< 轴序号（0-5为轴参数，6为系统参数）
            public byte ParaIndex;  ///< 参数序号
        }

        struct stMcCmnd_RestPosTyp
        {
            public byte SubCmnd;
            /// @var	SubCmnd
            /// @brief	清零方式
            /// @note	0x01	以PosCmnd值按绝对方式修改轴位置
            /// @note	0x02	以PosCmnd值按相对方式修改轴位置
            public byte AxisID;		///< 轴号
            public UInt16 dummy;
            public Single PosCmd;	///< 手动操作时，目标位置
        }

        struct stMcCmnd_SetOutputTyp
        {
            public byte SubCmnd;
            /// @var	SubCmnd
            /// @brief	管理类指令子指令类型
            /// @note	0x01	
            public byte OutputIndex;	///< 输出序号
            public UInt16 dummy;		///< 点位
        }

        struct stMcCmnd_SetAnaEnTyp
        {
            public byte AxisID;		///< 轴号
            public byte bEn;		///< 是否使能0去使能，1使能
        }

        struct stMcCmnd_InputLevTyp
        {
            public byte AxisID;		///< 轴号
            public byte InputTyp;	///< 信号类型，限位或原点;.0_负限位，.1_正限位，.2_左右限位，.3_原点
            public byte bEn;		///< 是否使能0常开，1常闭
            public byte dummy;
        }

        struct tMcCmnd_SetEncoderCmndTyp
        {
            public byte AxisID;		///< 轴号
            public byte dummy1;
            public UInt16 dummy2;
            public Int32 EnCoderVal;	///< 编码器的值
        }

        //下传的G指令结构体
        struct stGCodeCmmndTyp
        {
            public UInt16 GCode;
            public UInt16 AxisEn;

            public float TargtPosX;
            public float TargtPosY;
            public float TargtPosZ;
            public float TargtPosA;

            public float CmmndVelX;
            public float CmmndVelY;
            public float CmmndVelZ;
            public float CmmndVelA;

            public float TargtAccX;
            public float TargtAccY;
            public float TargtAccZ;
            public float TargtAccA;

            public float Radius;
            public float DelayTime;
        }

        stMcCmndType stMcCmnd = new stMcCmndType(1);
        stUploadNcSttType stNcStt = new stUploadNcSttType(1);


        byte[] CommSendBuffer = new byte[256];                         //串口发送缓冲区
        byte[] CommReceiveBuffer = new byte[256];                         //串口接收缓冲区

        //定义串口
        private System.IO.Ports.SerialPort serlPrt;
        static EventWaitHandle EventSPDataR;
        static EventWaitHandle EventSPDataS;
        private System.Byte[] SPReceiveDataBuffer;
        UartReceiveSttType UartReceiveStt;

        private int SpIntrThrdHod;      // 串口接收数据产生中断的阈值，也就是需要下位机返回的数据的长度
        private int DtLenWanted;        // 欲读取的数据的长度
        //公有成员
        public System.Boolean CommLinkEnable;
        public System.Boolean UartPortUsed;
        public System.Byte MaxAxisNum;
        public System.Byte ParaNumPerAxis;
        public System.Byte ParaSum;

        public SPLibClass()
        {
            /*
             * 
             */
            serlPrt = new System.IO.Ports.SerialPort();
            EventSPDataR = new AutoResetEvent(false);
            EventSPDataS = new AutoResetEvent(false);
            SPReceiveDataBuffer = new System.Byte[20];// { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            //初始化公共成员变量
            CommLinkEnable = false;
            UartPortUsed = false;
            SpIntrThrdHod = 1;
            MaxAxisNum = MAX_AXIS_NUM;
            ParaNumPerAxis = PARA_NUM_PER_AXIS;
            ParaSum = PARA_SUM;

            Debug.Print("###DEBUG###\t运动控制卡类初始化!");
        }

        ~SPLibClass()
        {

            if (serlPrt.IsOpen)
            {
                serlPrt.Close();
            }
        }

        private void Clear_stPCDownToMCUCmmndTyp()
        {
            // 帧头数据
            stMcCmnd.u8SpCom_Headr = SPCOMFRAMEHEADR;
            stMcCmnd.u8SpCom_DtLen = 0x00;
            stMcCmnd.u8SpCom_Dumy = 0x00;
            stMcCmnd.u8SpCom_Check = 0x00;

            // 帧数据区数据
            stMcCmnd.u8SpDt_CmndType = 0x00;
            stMcCmnd.u8SpDt_Len = 0x00;
            stMcCmnd.u8SpDt_CheckSum = 0x00;
            if (++CmmndIndex > 250) CmmndIndex = 1;
            stMcCmnd.u8SpDt_CmndNo = CmmndIndex;
        }

        unsafe private void FillCommSendBuffer()
        {
            UInt16 Tmpi;
            UInt32 TmpJiaoYanHe = 0;

            // 填写帧头数据
            stMcCmnd.u8SpCom_Headr = 0xAA;
            stMcCmnd.u8SpCom_DtLen = (byte)(stMcCmnd.u8SpDt_Len);
            stMcCmnd.u8SpCom_Check = (byte)~stMcCmnd.u8SpCom_DtLen;
            stMcCmnd.u8SpCom_Dumy = 0x00;

            fixed (stMcCmndType* pstSendData = &stMcCmnd)
            {
                byte* pbyte = (byte*)pstSendData;

                for (Tmpi = 0; Tmpi < stMcCmnd.u8SpDt_Len + SPCOMFRAMEHEADRLEN; Tmpi++)      // 此处加的SPCOMFRAMEHEADRLEN个字节为帧头的SPCOMFRAMEHEADRLEN个字节
                {
                    CommSendBuffer[Tmpi] = *(pbyte + Tmpi);
                    if (Tmpi > SPCOMFRAMEHEADRLEN) TmpJiaoYanHe += CommSendBuffer[Tmpi];    // 此处>SPCOMFRAMEHEADRLEN，是为了去除数据结构中的帧头的4个字节
                }
                CommSendBuffer[SPCOMFRAMEHEADRLEN] = (byte)TmpJiaoYanHe;
            }
            stMcCmnd.u8SpDt_CheckSum = CommSendBuffer[SPCOMFRAMEHEADRLEN];

            SpIntrThrdHod = DtLenWanted + 5;    // 确定串口断数阈值，状态信息头4个字节，加校验和1个字节
            if (serlPrt.IsOpen)
            {
                serlPrt.ReceivedBytesThreshold = SpIntrThrdHod;
            }
        }

        unsafe private byte Fun_DeCodeCmmndState()
        {
            UInt16 TmpI;
            UInt32 TmpJiaoYanHe = 0;

            fixed (stUploadNcSttType* pNcStt = &stNcStt)
            {
                byte* pbyte1 = (byte*)pNcStt;

                for (TmpI = 0; TmpI < DtLenWanted + SPCOMNCSTTHEADRLEN; TmpI++)
                {
                    *(pbyte1 + TmpI) = CommReceiveBuffer[TmpI];
                    TmpJiaoYanHe += CommReceiveBuffer[TmpI];
                }
                TmpJiaoYanHe = (byte)TmpJiaoYanHe;

                if (CommReceiveBuffer[TmpI] == TmpJiaoYanHe)
                {
                    if (MCU_CMND_EXECUTE_OK == stNcStt.u8CmndStt) return FUNRES_OK;
                    else return FUNRES_ERR;
                }
                else return FUNRES_ERR;
            }
        }

        unsafe private byte DeCodeCmmndState(out System.Single ResSingleVal, out System.UInt32 ResUint32Val)
        {
            UInt16 TmpI;
            UInt32 TmpJiaoYanHe = 0;

            fixed (stUploadNcSttType* pNcStt = &stNcStt)
            {
                byte* pbyte1 = (byte*)pNcStt;

                for (TmpI = 0; TmpI < SpIntrThrdHod; TmpI++)
                {
                    *(pbyte1 + TmpI) = CommReceiveBuffer[TmpI];
                    if (TmpI > 0)
                    {
                        TmpJiaoYanHe += CommReceiveBuffer[TmpI];
                    }
                }
                TmpJiaoYanHe = (byte)TmpJiaoYanHe;
            }

            if (stNcStt.u8CmndStt == TmpJiaoYanHe)
            {
                if (CMND_EXECUTE_OK == stNcStt.u8CmndStt)
                {
                    ResSingleVal = 0.0f;
                    ResUint32Val = 0;
                    return FUNRES_OK;
                }
                else
                {
                    ResSingleVal = 0.0f;
                    ResUint32Val = 0;
                    return FUNRES_ERR;
                }
            }
            else
            {
                ResSingleVal = 0.0f;
                ResUint32Val = 0;
                return FUNRES_ERR;
            }
        }


        public System.UInt16 MoCtrCard_SendMDICommand(System.String MDICmndStr)
        {
            string[] GCodeFlag = new string[] { "^G[ ]*[0-9]*", 
                    "[Q]?X[ ]*-?[0-9]+\\.?[0-9]*", "FX[ ]*-?[0-9]+\\.?[0-9]*", "AX[ ]*-?[0-9]+\\.?[0-9]*", 
                    "[Q]?Y[ ]*-?[0-9]+\\.?[0-9]*", "FY[ ]*-?[0-9]+\\.?[0-9]*", "AY[ ]*-?[0-9]+\\.?[0-9]*", 
                    "[Q]?Z[ ]*-?[0-9]+\\.?[0-9]*", "FZ[ ]*-?[0-9]+\\.?[0-9]*", "AZ[ ]*-?[0-9]+\\.?[0-9]*", 
                    "[Q]?T[ ]*-?[0-9]+\\.?[0-9]*", "FT[ ]*-?[0-9]+\\.?[0-9]*", "AT[ ]*-?[0-9]+\\.?[0-9]*", 
                    "[Q]?F[ ]*-?[0-9]+\\.?[0-9]*", 
                    "[Q]?A[ ]*-?[0-9]+\\.?[0-9]*", 
                    "R[ ]*-?[0-9]+\\.?[0-9]*", 
                    "D[ ]*-?[0-9]+\\.?[0-9]*",
                    "CLEAR[ ]*[0-9]+",
                    "SET[ ]*[0-9]+",
                    "HOME[ ]*([X|Y|Z|T][0|1|2|3])+"};
            string DigitalFlag = "[^a-z|A-Z][0-9|\\.]*";
            System.UInt16 TmpI = 0;
            System.String TmpDigitStr;

            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_GCODECMND;

            unsafe
            {
                stGCodeCmmndTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stGCodeCmmndTyp*)pByte;
                    stManOp->AxisEn = 0x00;

                    for (TmpI = 0; TmpI < GCodeFlag.Length; TmpI++)
                    {
                        MatchCollection myCol = Regex.Matches(MDICmndStr, GCodeFlag[TmpI]);
                        if (myCol.Count > 0)
                        {
                            Debug.Print("TMPI = {0:d3}:" + myCol[0].Value.ToString(), TmpI);
                            MatchCollection myDigit = Regex.Matches(myCol[0].Value.ToString(), DigitalFlag);
                            TmpDigitStr = myDigit[0].Value.ToString();
                            Debug.Print("TMPI = {0:d3}:" + TmpDigitStr, TmpI);

                            if (0 == TmpI)          //提取G指令
                            {
                                stManOp->GCode = byte.Parse(TmpDigitStr.Trim());
                                if (stManOp->GCode >= 80)
                                {
                                    stManOp->GCode -= 80;
                                    stManOp->GCode += 0x80;
                                }
                            }
                            else if (1 == TmpI)     //提取X位置
                            {
                                stManOp->TargtPosX = Single.Parse(TmpDigitStr.Trim());
                                stManOp->AxisEn |= 0x01;
                            }
                            else if (2 == TmpI)     //提取X速度
                            {
                                stManOp->CmmndVelX = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (3 == TmpI)     //提取X加速度
                            {
                                stManOp->TargtAccX = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (4 == TmpI)     //提取Y位置
                            {
                                stManOp->TargtPosY = Single.Parse(TmpDigitStr.Trim());
                                stManOp->AxisEn |= 0x02;
                            }
                            else if (5 == TmpI)     //提取Y速度
                            {
                                stManOp->CmmndVelY = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (6 == TmpI)     //提取Y加速度
                            {
                                stManOp->TargtAccY = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (7 == TmpI)     //提取Z位置
                            {
                                stManOp->TargtPosZ = Single.Parse(TmpDigitStr.Trim());
                                stManOp->AxisEn |= 0x04;
                            }
                            else if (8 == TmpI)     //提取Z速度
                            {
                                stManOp->CmmndVelZ = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (9 == TmpI)     //提取Z加速度
                            {
                                stManOp->TargtAccZ = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (10 == TmpI)    //提取T位置
                            {
                                stManOp->TargtPosA = Single.Parse(TmpDigitStr.Trim());
                                stManOp->AxisEn |= 0x08;
                            }
                            else if (11 == TmpI)    //提取T速度
                            {
                                stManOp->CmmndVelA = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (12 == TmpI)    //提取T加速度
                            {
                                stManOp->TargtAccA = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (13 == TmpI)    //提取F
                            {
                                stManOp->CmmndVelX = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (14 == TmpI)    //提取Acc
                            {
                                stManOp->TargtAccX = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (15 == TmpI)    //提取R
                            {
                                stManOp->Radius = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (16 == TmpI)    //提取延时时间 
                            {
                                stManOp->DelayTime = Single.Parse(TmpDigitStr.Trim());
                            }
                            else if (17 == TmpI)//清输出口
                            {
                                stManOp->GCode = 0x05;
                                stManOp->TargtPosX = Single.Parse(TmpDigitStr.Trim());
                                stManOp->CmmndVelX = 0.0f;
                            }
                            else if (18 == TmpI)//置输出口
                            {
                                stManOp->GCode = 0x05;
                                stManOp->TargtPosX = Single.Parse(TmpDigitStr.Trim());
                                stManOp->CmmndVelX = 1.0f;
                            }
                            else if (19 == TmpI)//回零
                            {
                                stManOp->GCode = 0x06;
                                //判断预回零的轴号
                                if (TmpDigitStr.IndexOf("0") >= 0)
                                {
                                    stManOp->AxisEn |= 0x01;
                                }
                                if (TmpDigitStr.IndexOf("1") >= 0)
                                {
                                    stManOp->AxisEn |= 0x02;
                                }
                                if (TmpDigitStr.IndexOf("2") >= 0)
                                {
                                    stManOp->AxisEn |= 0x04;
                                }
                                if (TmpDigitStr.IndexOf("3") >= 0)
                                {
                                    stManOp->AxisEn |= 0x08;
                                }
                                if (TmpDigitStr.IndexOf("4") >= 0)
                                {
                                    stManOp->AxisEn |= 0x10;
                                }
                                if (TmpDigitStr.IndexOf("5") >= 0)
                                {
                                    stManOp->AxisEn |= 0x20;
                                }
                            }
                        }
                    }

                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stGCodeCmmndTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        private System.UInt16 MoCtrCard_SendCmmndBufViaComm_1()
        {
            UInt16 TmpI = 0;
            UartPortUsed = true;

            if (serlPrt.IsOpen)
            {
                for (TmpI = 0; TmpI < 1; TmpI++)
                {
                    UartReceiveStt = UartReceiveSttType.OtherEx;

                    serlPrt.Write(CommSendBuffer, 0, stMcCmnd.u8SpDt_Len + SPCOMFRAMEHEADRLEN);

                    EventSPDataR.WaitOne(200);

                    if (UartReceiveSttType.DataArrived == UartReceiveStt)
                    {
                        if (FUNRES_OK == Fun_DeCodeCmmndState())
                        {
                            CommLinkEnable = true;
                            return FUNRES_OK;
                        }
                    }
                    else
                    {
                        if (TmpI > 3)
                        {
                            CommLinkEnable = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                CommLinkEnable = false;
            }
            UartPortUsed = true;
            return FUNRES_ERR;
        }

        private System.UInt16 MoCtrCard_SendCmmndBufViaComm(out System.UInt32 ResUInt32, out System.Single ResFloat)
        {
            UInt16 TmpI = 0;

            ResUInt32 = 0;
            ResFloat = 0.0f;

            UartPortUsed = true;

            if (serlPrt.IsOpen)
            {
                for (TmpI = 0; TmpI < 5; TmpI++)
                {
                    UartReceiveStt = UartReceiveSttType.OtherEx;

                    serlPrt.Write(CommSendBuffer, 0, stMcCmnd.u8SpDt_Len + SPCOMFRAMEHEADRLEN);

                    EventSPDataR.WaitOne(20000);

                    if (UartReceiveSttType.DataArrived == UartReceiveStt)
                    {
                        if (FUNRES_OK == Fun_DeCodeCmmndState())
                        {
                            CommLinkEnable = true;
                            return FUNRES_OK;
                        }
                    }
                    else
                    {
                        if (TmpI > 3)
                        {
                            CommLinkEnable = false;
                            break;
                        }
                    }
                }
            }
            else
            {
                CommLinkEnable = false;
            }
            UartPortUsed = true;
            return FUNRES_ERR;
        }

        public System.UInt16 MoCtrCard_Initial(System.String SerialPortName)
        {
            /*
             * 函数作用：根据串口号初始化要使用的串口
             * 串口参数：波特率115200，无校验，8个数据位，1个停止位，GB2312编码
             * 返回参数：0. 初始化有误；1. 正确初始化；
             */
            try
            {
                serlPrt.PortName = SerialPortName;

                if (serlPrt.IsOpen)
                {
                    serlPrt.Close();
                }

                serlPrt.ReadTimeout = 500;
                serlPrt.ReceivedBytesThreshold = INPUT_CMMND_SIZE;
                serlPrt.RtsEnable = true;
                serlPrt.WriteTimeout = 500;
                serlPrt.ReadBufferSize = 1024;
                serlPrt.WriteBufferSize = 1024;
                serlPrt.BaudRate = 115200;
                serlPrt.Parity = Parity.None;
                serlPrt.DataBits = 8;
                serlPrt.StopBits = StopBits.One;
                serlPrt.Encoding = Encoding.GetEncoding("Gb2312");
                //串口接收中断函数
                serlPrt.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(SeriaPortDataReceiveHandler);
                Debug.Print("###DEBUG###\t初始化串口[{0}]成功!", SerialPortName);
                //串口状态标置
                CommLinkEnable = false;             //进行一次通讯，正确后再置该标志
                UartPortUsed = false;

                serlPrt.Open();     //打开串口
                Debug.Print("###DEBUG###\t打开串口[{0}]成功!", SerialPortName);

                //测试链接是否建立
                if (FUNRES_OK == MoCtrCard_QuiteMotionControl())
                {
                    Debug.Print("###DEBUG###\t已联接运动控制卡!");
                    CommLinkEnable = true;
                    return FUNRES_OK;
                }
                else
                {
                    Debug.Print("###DEBUG###\t运动控制卡没有联接!");
                    return FUNRES_ERR;
                }
            }
            catch (Exception)
            {
                Debug.Print("###DEBUG###\t打开串口失败!");
                return FUNRES_ERR;
            }
        }

        private void SeriaPortDataReceiveHandler(object sender, SerialDataReceivedEventArgs e)
        {
            int count = serlPrt.BytesToRead;
            Debug.Print("serial port data received handler {0}\n", count);
            if (count >= SpIntrThrdHod)
            {
                try
                {
                    serlPrt.ReceivedBytesThreshold = 100;
                    serlPrt.Read(CommReceiveBuffer, 0, count);
                    UartReceiveStt = UartReceiveSttType.DataArrived;			//串口正确接收到数据
                    EventSPDataR.Set();

                    Debug.Print("Receive thread, Received Data already \n");
                }
                catch (InvalidOperationException ex)
                {
                    UartReceiveStt = UartReceiveSttType.PortNotOpen;		//串口接收异常 		
                    Debug.Print("Exception in Serial Port Received Process \n");
                }
                catch (TimeoutException ex)
                {
                    UartReceiveStt = UartReceiveSttType.NoDataToRead;		//串口接收异常 	
                    Debug.Print("Uart Exception, No data to read \n");
                }
                //                 finally
                //                 {
                //                     UartReceiveStt = UartReceiveSttType.OtherEx;		//其它非处理异常		
                //                 }
            }
        }

        public System.UInt16 MoCtrCard_Unload()
        {
            serlPrt.Close();
            return FUNRES_OK;
        }
        public System.UInt16 MoCtrCard_GetAxisPos(System.Byte AxisId, System.Single[] ResPos)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = AxisId;
                    if (AxisId < MAX_AXIS_NUM)
                    {
                        stManOp->ParaIndex = AskInfo_Pos_Each;
                        DtLenWanted = 4;
                    }
                    else if (0xFF == AxisId)
                    {
                        if (ResPos.GetLength(0) < MaxAxisNum)
                        {
                            return FUNRES_ERR;
                        }
                        else
                        {
                            stManOp->ParaIndex = AskInfo_Pos_Grp;
                            DtLenWanted = 4 * MaxAxisNum;
                        }
                    }
                    else return FUNRES_ERR;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.Single pRes = 0.0f;
                System.Byte tmpI = 0;
                System.Byte tmpJ = 0;
                unsafe
                {
                    System.Single* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;
                        if (AxisId < MAX_AXIS_NUM)
                        {
                            for (tmpI = 0; tmpI < 4; tmpI++)
                            {
                                *(pDestByte + tmpI) = *(pByte + tmpI);
                            }
                            ResPos[0] = pRes;
                        }
                        else if (0xFF == AxisId)
                        {
                            for (tmpJ = 0; tmpJ < MaxAxisNum; tmpJ++)
                            {
                                for (tmpI = 0; tmpI < 4; tmpI++)
                                {
                                    *(pDestByte + tmpI) = *(pByte + tmpI + 4 * tmpJ);
                                }
                                ResPos[tmpJ] = pRes;
                            }
                        }

                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt16 MoCtrCard_GetAxisSpd(System.Byte AxisId, System.Single[] ResSpd)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = AxisId;
                    if (AxisId < MAX_AXIS_NUM)
                    {
                        stManOp->ParaIndex = AskInfo_Spd_Each;
                        DtLenWanted = 4;
                    }
                    else if (0xFF == AxisId)
                    {
                        if (ResSpd.GetLength(0) < MaxAxisNum)
                        {
                            return FUNRES_ERR;
                        }
                        else
                        {
                            stManOp->ParaIndex = AskInfo_Spd_Grp;
                            DtLenWanted = 4 * MaxAxisNum;
                        }
                    }
                    else return FUNRES_ERR;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.Single pRes = 0.0f;
                System.Byte tmpI = 0;
                System.Byte tmpJ = 0;
                unsafe
                {
                    System.Single* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;
                        if (AxisId < MAX_AXIS_NUM)
                        {
                            for (tmpI = 0; tmpI < 4; tmpI++)
                            {
                                *(pDestByte + tmpI) = *(pByte + tmpI);
                            }
                            ResSpd[0] = pRes;
                        }
                        else if (0xFF == AxisId)
                        {
                            for (tmpJ = 0; tmpJ < MaxAxisNum; tmpJ++)
                            {
                                for (tmpI = 0; tmpI < 4; tmpI++)
                                {
                                    *(pDestByte + tmpI) = *(pByte + tmpI + 4 * tmpJ);
                                }
                                ResSpd[tmpJ] = pRes;
                            }
                        }

                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }
        public System.UInt16 MoCtrCard_GetRunState(System.Int32[] ResStt)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = 0;
                    stManOp->ParaIndex = AskInfo_NcStt;
                }
                DtLenWanted = 4;
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.Int32 pRes = 0;
                System.Byte tmpI = 0;
                unsafe
                {
                    System.Int32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;

                        for (tmpI = 0; tmpI < 4; tmpI++)
                        {
                            *(pDestByte + tmpI) = *(pByte + tmpI);
                        }
                        ResStt[0] = pRes;
                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt16 MoCtrCard_GetAdVal(System.Byte AxisId, System.Int32[] ResAd)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = AxisId;
                    if (AxisId < MAX_AXIS_NUM)
                    {
                        stManOp->ParaIndex = AskInfo_Ana_Each;
                        DtLenWanted = 4;
                    }
                    else if (0xFF == AxisId)
                    {
                        if (ResAd.GetLength(0) < MaxAxisNum)
                        {
                            return FUNRES_ERR;
                        }
                        else
                        {
                            stManOp->ParaIndex = AskInfo_Ana_Grp;
                            DtLenWanted = 4 * MaxAxisNum;
                        }
                    }
                    else return FUNRES_ERR;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.Int32 pRes = 0;
                System.Byte tmpI = 0;
                System.Byte tmpJ = 0;
                unsafe
                {
                    System.Int32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;
                        if (AxisId < MAX_AXIS_NUM)
                        {
                            for (tmpI = 0; tmpI < 4; tmpI++)
                            {
                                *(pDestByte + tmpI) = *(pByte + tmpI);
                            }
                            ResAd[0] = pRes;
                        }
                        else if (0xFF == AxisId)
                        {
                            for (tmpJ = 0; tmpJ < MaxAxisNum; tmpJ++)
                            {
                                for (tmpI = 0; tmpI < 4; tmpI++)
                                {
                                    *(pDestByte + tmpI) = *(pByte + tmpI + 4 * tmpJ);
                                }
                                ResAd[tmpJ] = pRes;
                            }
                        }

                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt16 MoCtrCard_GetOutState(System.Byte OutGrpIndx, System.UInt32[] ResOut)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = OutGrpIndx;
                    if (OutGrpIndx == 0) stManOp->ParaIndex = AskInfo_Output_L;
                    else if (OutGrpIndx == 1) stManOp->ParaIndex = AskInfo_Output_H;
                    else return FUNRES_ERR;
                }
                DtLenWanted = 4;
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.UInt32 pRes = 0;
                System.Byte tmpI = 0;
                unsafe
                {
                    System.UInt32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;

                        for (tmpI = 0; tmpI < 4; tmpI++)
                        {
                            *(pDestByte + tmpI) = *(pByte + tmpI);
                        }
                        ResOut[0] = pRes;
                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt16 MoCtrCard_GetInputState(System.Byte InGrpIndx, System.UInt32[] ResIn)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = InGrpIndx;
                    if (InGrpIndx == 0) stManOp->ParaIndex = AskInfo_Input_L;
                    else if (InGrpIndx == 1) stManOp->ParaIndex = AskInfo_Input_H;
                    else return FUNRES_ERR;
                }
                DtLenWanted = 4;
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.UInt32 pRes = 0;
                System.Byte tmpI = 0;
                unsafe
                {
                    System.UInt32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;

                        for (tmpI = 0; tmpI < 4; tmpI++)
                        {
                            *(pDestByte + tmpI) = *(pByte + tmpI);
                        }
                        ResIn[0] = pRes;
                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt16 MoCtrCard_GetIntInputState(System.Byte InGrpIndx, System.UInt32[] ResIntIn)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = InGrpIndx;
                    if (InGrpIndx == 0) stManOp->ParaIndex = AskInfo_Intrupt_L;
                    else if (InGrpIndx == 1) stManOp->ParaIndex = AskInfo_Intrupt_H;
                    else return FUNRES_ERR;
                }
                DtLenWanted = 4;
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.UInt32 pRes = 0;
                System.Byte tmpI = 0;
                unsafe
                {
                    System.UInt32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;

                        for (tmpI = 0; tmpI < 4; tmpI++)
                        {
                            *(pDestByte + tmpI) = *(pByte + tmpI);
                        }
                        ResIntIn[0] = pRes;
                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt16 MoCtrCard_SendPara(System.Byte AxisId, System.Byte ParaIndx, System.Single ParaVal)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_SETPARA;
            unsafe
            {
                stMcCmnd_SetParaTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_SetParaTyp*)pByte;
                    stManOp->AxisId = AxisId;
                    stManOp->ParaIndex = ParaIndx;
                    stManOp->fVal = ParaVal;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_SetParaTyp));
            }

            DtLenWanted = 0;

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1()) return FUNRES_OK;
            else return FUNRES_ERR;
        }

        private System.UInt16 MoCtrCard_SendGCodePara(UInt16 ParaIndex1, Single TmpPara1)
        {
            Single ResFp;
            UInt32 ResLong;

            Clear_stPCDownToMCUCmmndTyp();
            //stSendData.CommandType = CMND_SETGCODEPARA;
            //stSendData.u8Para1 = (byte)(ParaIndex1);
            // stSendData.fPara1 = TmpPara1;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm(out ResLong, out ResFp);
        }

        public System.UInt16 MoCtrCard_MCrlAxisMove(System.Byte AxisId, System.SByte SpdDir)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_MOVEINNERDIST;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = SpdDir;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = 0.0f;
                    stManOp->AccCmd = 0.0f;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_MCrlAxisRelMove(System.Byte AxisId, System.Single DistCmnd, System.Single VCmnd, System.Single ACmnd)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_MOVERELDIST;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = DistCmnd;
                    stManOp->VelCmd = VCmnd;
                    stManOp->AccCmd = ACmnd;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_MCrlAxisRelMove(System.Byte AxisId, System.Single DistCmnd)
        {
            return MoCtrCard_MCrlAxisRelMove(AxisId, DistCmnd, 0.0f, 0.0f);
        }

        public System.UInt16 MoCtrCard_MCrlAxisAbsMove(System.Byte AxisId, System.Single PosCmnd, System.Single VCmnd, System.Single ACmnd)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_MOVEABSDIST;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = PosCmnd;
                    stManOp->VelCmd = VCmnd;
                    stManOp->AccCmd = ACmnd;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_MCrlAxisAbsMove(System.Byte AxisId, System.Single PosCmnd)
        {
            return MoCtrCard_MCrlAxisAbsMove(AxisId, PosCmnd, 0.0f, 0.0f);
        }

        public System.UInt16 MoCtrCard_SeekZero(System.Byte AxisId, System.Single VCmnd, System.Single ACmnd)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_HOMING;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = VCmnd;
                    stManOp->AccCmd = ACmnd;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }



        public System.UInt16 MoCtrCard_SeekZero(System.Byte AxisId)
        {
            return MoCtrCard_SeekZero(AxisId, 0.0f, 0.0f);
        }

        public System.UInt16 MoCtrCard_CancelSeekZero(System.Byte AxisId)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_HOMINGCANCLE;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = 0.0f;
                    stManOp->AccCmd = 0.0f;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_ResetCoordinate(System.Byte AxisId, System.Single RestPos)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_RESETPOS;
            unsafe
            {
                stMcCmnd_RestPosTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_RestPosTyp*)pByte;
                    stManOp->SubCmnd = RESETPOS_ABS;
                    stManOp->AxisID = AxisId;
                    stManOp->PosCmd = RestPos;
                    stManOp->dummy = 0x00;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_RestPosTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }
        public System.UInt16 MoCtrCard_ReStartAxisMov(System.Byte AxisId)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_RESTARTMOVE;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = 0.0f;
                    stManOp->AccCmd = 0.0f;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_PauseAxisMov(System.Byte AxisId)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_PAUSE;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = 0.0f;
                    stManOp->AccCmd = 0.0f;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }


        public System.UInt16 MoCtrCard_StopAxisMov()
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_PARASTOP;
                    stManOp->AxisID = 0;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = 0.0f;
                    stManOp->AccCmd = 0.0f;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_ChangeAxisMovPara(System.Byte AxisId, System.Single fVel, System.Single fAcc)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_MOVECHAGEVEL;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = fVel;
                    stManOp->AccCmd = fAcc;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_EmergencyStopAxisMov(System.Byte AxisId)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_EMGERNCYSTOP;
                    stManOp->AxisID = AxisId;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = 0.0f;
                    stManOp->AccCmd = 0.0f;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_ReadPara(System.Byte AxisId, System.Byte ParaIdndx, System.Single[] ParaVal)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_ASKPARA;
            unsafe
            {
                stMcCmnd_ReadParaTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ReadParaTyp*)pByte;
                    stManOp->AxisId = AxisId;
                    stManOp->ParaIndex = ParaIdndx;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ReadParaTyp));
            }

            if (AxisId < 6 && (ParaIdndx < 0x0A)) DtLenWanted = 4;       // 单一变量数据长度
            else return FUNRES_ERR;     // 其它参数时出错

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.Single pRes = 0;
                unsafe
                {
                    System.Single* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;
                        *pDestByte = *pByte;
                        *(pDestByte + 1) = *(pByte + 1);
                        *(pDestByte + 2) = *(pByte + 2);
                        *(pDestByte + 3) = *(pByte + 3);
                    }

                    ParaVal[0] = pRes;
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }
        public System.UInt16 MoCtrCard_SaveSystemParaToROM()
        {

            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_SAVESYSPARA;
            stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN);

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }
        public System.UInt16 MoCtrCard_SetOutput(System.Byte OutputIndex, System.Boolean OutputVal)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_SETOUTPUT;
            unsafe
            {
                stMcCmnd_SetOutputTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_SetOutputTyp*)pByte;
                    stManOp->SubCmnd = OutputVal ? OutputSet : OutputReset;
                    stManOp->OutputIndex = OutputIndex;
                    stManOp->dummy = 0;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_SetOutputTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }
        public System.UInt16 MoCtrCard_SetJoyStickEnable(System.Byte AxisId, System.Byte bEnable)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_ANALOGOP;
            unsafe
            {
                stMcCmnd_SetAnaEnTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_SetAnaEnTyp*)pByte;
                    stManOp->AxisID = AxisId;
                    stManOp->bEn = bEnable;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_SetAnaEnTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_SetAxisRealtiveInputPole(System.Byte AxisId, System.Byte InputType, System.Byte OpenOrClose)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_ANALOGOP;
            unsafe
            {
                stMcCmnd_InputLevTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_InputLevTyp*)pByte;
                    stManOp->AxisID = AxisId;
                    stManOp->InputTyp = InputType;
                    stManOp->bEn = OpenOrClose;
                    stManOp->dummy = 0x00;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_InputLevTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_QuiteMotionControl()
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_MANUAL_OP;
            unsafe
            {
                stMcCmnd_ManulOpTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_ManulOpTyp*)pByte;
                    stManOp->SubCmnd = MANOP_QUITEMOVE;
                    stManOp->AxisID = 0;
                    stManOp->SpdDir = 0;
                    stManOp->dummy = 0;
                    stManOp->PosCmd = 0.0f;
                    stManOp->VelCmd = 0.0f;
                    stManOp->AccCmd = 0.0f;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_ManulOpTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt32 MoCtrCard_GetDLLVersion()
        {
            return DLL_VER;
        }

        public System.UInt32 MoCtrCard_GetCardVersion(System.UInt32[] ResVersion)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = 0x00;
                    stManOp->ParaIndex = AskInfo_Version;
                    DtLenWanted = 4;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.UInt32 pRes = 0;
                System.Byte tmpI = 0;
                unsafe
                {
                    System.UInt32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;

                        for (tmpI = 0; tmpI < 4; tmpI++)
                        {
                            *(pDestByte + tmpI) = *(pByte + tmpI);
                        }
                        ResVersion[0] = pRes;
                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt32 MoCtrCard_GetEncoderVal(System.Byte AxisId, System.Int32[] EncoderPos)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = AxisId;
                    if (AxisId < MAX_AXIS_NUM)
                    {
                        stManOp->ParaIndex = AskInfo_Encdr_Each;
                        DtLenWanted = 4;
                    }
                    else if (0xFF == AxisId)
                    {
                        if (EncoderPos.GetLength(0) < MaxAxisNum)
                        {
                            return FUNRES_ERR;
                        }
                        else
                        {
                            stManOp->ParaIndex = AskInfo_Encdr_Grp;
                            DtLenWanted = 4 * MaxAxisNum;
                        }
                    }
                    else return FUNRES_ERR;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.Int32 pRes = 0;
                System.Byte tmpI = 0;
                System.Byte tmpJ = 0;
                unsafe
                {
                    System.Int32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;
                        if (AxisId < MAX_AXIS_NUM)
                        {
                            for (tmpI = 0; tmpI < 4; tmpI++)
                            {
                                *(pDestByte + tmpI) = *(pByte + tmpI);
                            }
                            EncoderPos[0] = pRes;
                        }
                        else if (0xFF == AxisId)
                        {
                            for (tmpJ = 0; tmpJ < MaxAxisNum; tmpJ++)
                            {
                                for (tmpI = 0; tmpI < 4; tmpI++)
                                {
                                    *(pDestByte + tmpI) = *(pByte + tmpI + 4 * tmpJ);
                                }
                                EncoderPos[tmpJ] = pRes;
                            }
                        }

                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }

        public System.UInt32 MoCtrCard_SetEncoderPos(System.Byte AxisId, System.Int32 EncoderPos)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_SETENCODER;
            unsafe
            {
                tMcCmnd_SetEncoderCmndTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (tMcCmnd_SetEncoderCmndTyp*)pByte;
                    stManOp->AxisID = AxisId;
                    stManOp->dummy1 = 0;
                    stManOp->dummy2 = 0;
                    stManOp->EnCoderVal = EncoderPos;
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(tMcCmnd_SetEncoderCmndTyp));
            }

            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt32 MoCtrCard_RstZ(System.UInt16 AxisIndex)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMMND_RESETZ;
            stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN);
            DtLenWanted = 0;
            FillCommSendBuffer();
            return MoCtrCard_SendCmmndBufViaComm_1();
        }

        public System.UInt16 MoCtrCard_Test(System.Int32[] EncoderPos)
        {
            EncoderPos[0] = 1;
            EncoderPos[1] = 2;
            EncoderPos[2] = 3;
            EncoderPos[3] = 4;

            return 55;
        }

        public System.UInt32 MoCtrCard_GetBoardHardInfo(System.UInt32[] HardInfo)
        {
            Clear_stPCDownToMCUCmmndTyp();
            stMcCmnd.u8SpDt_CmndType = CMND_ASKMOVEINFO;
            unsafe
            {
                stMcCmnd_AskSttInfoTyp* stManOp;

                fixed (byte* pByte = stMcCmnd.u8SpDt_Buf)
                {
                    stManOp = (stMcCmnd_AskSttInfoTyp*)pByte;
                    stManOp->AxisId = 0x00;
                    stManOp->ParaIndex = AskInfo_McuId;
                    DtLenWanted = 12;

                    if (HardInfo.GetLength(0) < 3)
                    {
                        return FUNRES_ERR;
                    }
                }
                stMcCmnd.u8SpDt_Len = (byte)(SPCOMDATAHEADRLEN + sizeof(stMcCmnd_AskSttInfoTyp));
            }

            FillCommSendBuffer();
            if (FUNRES_OK == MoCtrCard_SendCmmndBufViaComm_1())
            {
                System.UInt32 pRes = 0;
                System.Byte tmpI = 0;
                System.Byte tmpJ = 0;
                unsafe
                {
                    System.UInt32* pSingle = &pRes;
                    fixed (System.Byte* pByte = stNcStt.u8Buf)
                    {
                        System.Byte* pDestByte = (System.Byte*)pSingle;
                        for (tmpJ = 0; tmpJ < 3; tmpJ++)
                        {
                            for (tmpI = 0; tmpI < 4; tmpI++)
                            {
                                *(pDestByte + tmpI) = *(pByte + tmpI + 4 * tmpJ);
                            }
                            HardInfo[tmpJ] = pRes;
                        }
                    }
                }
                return FUNRES_OK;
            }
            else
            {
                return FUNRES_ERR;
            }
        }
    }
}
