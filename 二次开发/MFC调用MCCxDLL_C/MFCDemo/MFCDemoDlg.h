
// MFCDemoDlg.h : 头文件
//

#pragma once
#include "afxwin.h"


// CMFCDemoDlg 对话框
class CMFCDemoDlg : public CDialogEx
{
// 构造
public:
	CMFCDemoDlg(CWnd* pParent = NULL);	// 标准构造函数

// 对话框数据
	enum { IDD = IDD_MFCDEMO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV 支持


// 实现
protected:
	HICON m_hIcon;

	// 生成的消息映射函数
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBnOpenport();
	// 串口号
	UINT mEditPortNum;
private:
	// 运动控制器参数
	float mMCtrlSysPara[100];
public:
	// X轴指令位置
	float CmndPosX;
	// Y轴指令位置
	float CmndPosY;
	// Z轴指令位置
	float CmndPosZ;
	// T轴指令位置
	float CmndPosT;
	afx_msg void OnTimer(UINT_PTR nIDEvent);

	// 输入静态控件变量值
	CString sInputVar;
	CString sOutputVal;
	afx_msg void OnBnClickedBnXneg();
	afx_msg void OnBnClickedBnXpos();
	afx_msg void OnBnClickedBnYneg();
	afx_msg void OnBnClickedBnYpos();
	afx_msg void OnBnClickedBnZneg();
	afx_msg void OnBnClickedBnZpos();
	afx_msg void OnBnClickedBnTneg();
	afx_msg void OnBnClickedBnTpos();
	afx_msg void OnBnClickedBnHomex();
	afx_msg void OnBnClickedBnHomey();
	afx_msg void OnBnClickedBnHomez();
	afx_msg void OnBnClickedBnHomet();
	afx_msg void OnBnClickedBnHomeall();
	afx_msg void OnBnClickedBnHomecancel();
	CString sRunStateVal;
	UINT mOutputIndx;
	UINT mOutVal;
	afx_msg void OnBnClickedBnHomey2();
	CString sMdiCmnd;
	afx_msg void OnBnClickedBnMdi();
	afx_msg void OnBnClickedBnResetcord();
	afx_msg void OnBnClickedBnQuitemove();
	afx_msg void OnEnChangeEditPost2();
	afx_msg void OnEnChangeEditPost3();
};
