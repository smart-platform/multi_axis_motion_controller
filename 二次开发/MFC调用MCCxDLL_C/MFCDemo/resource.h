//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by MFCDemo.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_MFCDEMO_DIALOG              102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDR_MAINFRAME                   128
#define IDC_BN_OPENPORT                 1000
#define IDC_EDIT_COMPORT                1001
#define IDC_STATIC_POSX                 1003
#define IDC_STATIC_POSX2                1004
#define IDC_STATIC_POSX3                1005
#define IDC_STATIC_POSX4                1006
#define IDC_EDIT_POSX                   1007
#define IDC_EDIT_POSY                   1008
#define IDC_EDIT_POSZ                   1009
#define IDC_EDIT_POST                   1010
#define IDC_STATIC_InputTitle           1011
#define IDC_STATIC_InputVal             1012
#define IDC_STATIC_Output               1013
#define IDC_STATIC_OutputVal            1014
#define IDC_BN_XNEG                     1015
#define IDC_BN_XPOS                     1016
#define IDC_BN_YNEG                     1017
#define IDC_BN_YPOS                     1018
#define IDC_BN_HOMEX                    1019
#define IDC_STATIC_RUNSTATE             1020
#define IDC_BN_ZNEG                     1021
#define IDC_BN_ZPOS                     1022
#define IDC_BN_TNEG                     1023
#define IDC_BN_TPOS                     1024
#define IDC_BN_HOMEZ                    1025
#define IDC_BN_HOMEY                    1026
#define IDC_BN_HOMET                    1027
#define IDC_BN_HOMEALL                  1028
#define IDC_BN_HOMECANCEL               1029
#define IDC_STATIC_RunStateVal          1030
#define IDC_STATIC_OUTPUTID             1031
#define IDC_STATIC_OUTPUTVAL1           1032
#define IDC_EDIT_POST2                  1033
#define IDC_EDIT_MDICMND                1034
#define IDC_EDIT_POST3                  1035
#define IDC_BN_HOMEY2                   1036
#define IDC_BN_MDI                      1037
#define IDC_BN_RESETCORD                1040
#define IDC_BN_HOMEZ2                   1041
#define IDC_BN_QUITEMOVE                1041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1035
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
