
// MFCDemoDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "MFCDemo.h"
#include "MFCDemoDlg.h"
#include "afxdialogex.h"
#if false
#include "../x64/Release/MCC4DLL.h"
#pragma comment(lib, "../x64/Release/MCC4DLL.lib")
#else
#include "../Release/MCC4DLL.h"
#pragma comment(lib, "../Release/MCC4DLL.lib")
#endif

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define IDC_TIMER1 1

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CMFCDemoDlg 对话框




CMFCDemoDlg::CMFCDemoDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CMFCDemoDlg::IDD, pParent)
	, mEditPortNum(0)
	, CmndPosX(0)
	, CmndPosY(0)
	, CmndPosZ(0)
	, CmndPosT(0)
	, sInputVar(_T(""))
	, sOutputVal(_T(""))
	, sRunStateVal(_T(""))
	, mOutputIndx(0)
	, mOutVal(0)
	, sMdiCmnd(_T(""))
{
	UINT tmpI = 0;
	
	for (tmpI = 0; tmpI < 100; tmpI ++){
		mMCtrlSysPara[tmpI] = 0.0;
	}
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CMFCDemoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_COMPORT, mEditPortNum);
	DDV_MinMaxUInt(pDX, mEditPortNum, 0, 255);
	DDX_Text(pDX, IDC_EDIT_POSX, CmndPosX);
	DDX_Text(pDX, IDC_EDIT_POSY, CmndPosY);
	DDX_Text(pDX, IDC_EDIT_POSZ, CmndPosZ);
	DDX_Text(pDX, IDC_EDIT_POST, CmndPosT);
	DDX_Text(pDX, IDC_STATIC_InputVal, sInputVar);
	DDX_Text(pDX, IDC_STATIC_OutputVal, sOutputVal);
	DDX_Text(pDX, IDC_STATIC_RunStateVal, sRunStateVal);
	DDX_Text(pDX, IDC_EDIT_POST2, mOutputIndx);
	DDV_MinMaxUInt(pDX, mOutputIndx, 0, 31);
	DDX_Text(pDX, IDC_EDIT_POST3, mOutVal);
	DDV_MinMaxUInt(pDX, mOutVal, 0, 1);
	DDX_Text(pDX, IDC_EDIT_MDICMND, sMdiCmnd);
}

BEGIN_MESSAGE_MAP(CMFCDemoDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BN_OPENPORT, &CMFCDemoDlg::OnBnClickedBnOpenport)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BN_XNEG, &CMFCDemoDlg::OnBnClickedBnXneg)
	ON_BN_CLICKED(IDC_BN_XPOS, &CMFCDemoDlg::OnBnClickedBnXpos)
	ON_BN_CLICKED(IDC_BN_YNEG, &CMFCDemoDlg::OnBnClickedBnYneg)
	ON_BN_CLICKED(IDC_BN_YPOS, &CMFCDemoDlg::OnBnClickedBnYpos)
	ON_BN_CLICKED(IDC_BN_ZNEG, &CMFCDemoDlg::OnBnClickedBnZneg)
	ON_BN_CLICKED(IDC_BN_ZPOS, &CMFCDemoDlg::OnBnClickedBnZpos)
	ON_BN_CLICKED(IDC_BN_TNEG, &CMFCDemoDlg::OnBnClickedBnTneg)
	ON_BN_CLICKED(IDC_BN_TPOS, &CMFCDemoDlg::OnBnClickedBnTpos)
	ON_BN_CLICKED(IDC_BN_HOMEX, &CMFCDemoDlg::OnBnClickedBnHomex)
	ON_BN_CLICKED(IDC_BN_HOMEY, &CMFCDemoDlg::OnBnClickedBnHomey)
	ON_BN_CLICKED(IDC_BN_HOMEZ, &CMFCDemoDlg::OnBnClickedBnHomez)
	ON_BN_CLICKED(IDC_BN_HOMET, &CMFCDemoDlg::OnBnClickedBnHomet)
	ON_BN_CLICKED(IDC_BN_HOMEALL, &CMFCDemoDlg::OnBnClickedBnHomeall)
	ON_BN_CLICKED(IDC_BN_HOMECANCEL, &CMFCDemoDlg::OnBnClickedBnHomecancel)
	ON_BN_CLICKED(IDC_BN_HOMEY2, &CMFCDemoDlg::OnBnClickedBnHomey2)
	ON_BN_CLICKED(IDC_BN_MDI, &CMFCDemoDlg::OnBnClickedBnMdi)
	ON_BN_CLICKED(IDC_BN_RESETCORD, &CMFCDemoDlg::OnBnClickedBnResetcord)
	ON_BN_CLICKED(IDC_BN_QUITEMOVE, &CMFCDemoDlg::OnBnClickedBnQuitemove)
	ON_EN_CHANGE(IDC_EDIT_POST2, &CMFCDemoDlg::OnEnChangeEditPost2)
	ON_EN_CHANGE(IDC_EDIT_POST3, &CMFCDemoDlg::OnEnChangeEditPost3)
END_MESSAGE_MAP()


// CMFCDemoDlg 消息处理程序

BOOL CMFCDemoDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码
	CmndPosX = 0.0;
	CmndPosY = 0.0;
	CmndPosZ = 0.0;
	CmndPosT = 0.0;

	sInputVar.Format(_T("%#010x"), 0);
	sOutputVal.Format(_T("%#010x"), 0);
	sRunStateVal.Format(_T("%#010x"), 0);
	sMdiCmnd = "G01 X100 Y100 F100";

	UpdateData(FALSE);

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CMFCDemoDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CMFCDemoDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CMFCDemoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CMFCDemoDlg::OnBnClickedBnOpenport()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	TRACE("PortNum = %d \r\n", mEditPortNum);

	if(funResOk == MoCtrCard_Initial(mEditPortNum)){
		if(funResOk == MoCtrCard_QuiteMotionControl()){
			UINT tmpAxisId = 0;
			UINT tmpParaId = 0;

			TRACE("运动控制卡初始化成功，从控制器上读取相关参数\r\n");
#if 1
			for (tmpAxisId = 0; tmpAxisId < MAX_AXIS_NUM; tmpAxisId ++){
				for (tmpParaId = 0; tmpParaId < PARA_CNT_PER_AXIS; tmpParaId ++){

					if(funResOk == MoCtrCard_ReadPara(tmpAxisId, tmpParaId, &mMCtrlSysPara[tmpAxisId * PARA_CNT_PER_AXIS + tmpParaId])){
						TRACE("读取轴[%d]参数[%d]成功，Val = %f \r\n", tmpAxisId, tmpParaId, mMCtrlSysPara[tmpAxisId * PARA_CNT_PER_AXIS + tmpParaId]);
					}
					else{
						TRACE("读取轴[%d]参数[%d]失败，Val = %f \r\n", tmpAxisId, tmpParaId);
					}
				}				
			}

#else
			tmpAxisId = 3;
			tmpParaId = 9;
			if(funResOk == MoCtrCard_ReadPara(tmpAxisId, tmpParaId, &mMCtrlSysPara[tmpAxisId * PARA_CNT_PER_AXIS + tmpParaId])){
				TRACE("读取轴[%d]参数[%d]成功，Val = %f \r\n", tmpAxisId, tmpParaId, mMCtrlSysPara[tmpAxisId * PARA_CNT_PER_AXIS + tmpParaId]);
			}
			else{
				TRACE("读取轴[%d]参数[%d]失败，Val = %f \r\n", tmpAxisId, tmpParaId);
			}
#endif

			// 启动定时器
			SetTimer(IDC_TIMER1, 300, 0);
		}
		else{
			TRACE("运动控制卡初始化失败！\r\n");
		}		
	}
	else{
		TRACE("初始化串口失败！\r\n");
	}
}


void CMFCDemoDlg::OnTimer(UINT_PTR nIDEvent)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if(IDC_TIMER1 == nIDEvent){
		// 更新位置定时器
		float tmpPos[MAX_AXIS_NUM] = {0.0};
		if(funResOk == MoCtrCard_GetAxisPos(0xFF, tmpPos)){
			CmndPosX = tmpPos[0];
			CmndPosY = tmpPos[1];
			CmndPosZ = tmpPos[2];
			CmndPosT = tmpPos[3];
		}

		McCard_UINT32 tmpInput = 0x00;
		if(funResOk == MoCtrCard_GetInputState(0, &tmpInput)){
			sInputVar.Format(_T("%#x"), tmpInput);
		}

		if(funResOk == MoCtrCard_GetOutState(0, &tmpInput)){
			sOutputVal.Format(_T("%#x"), tmpInput);
		}

		McCard_INT32 tmpRunState = 0x00;
		if(funResOk == MoCtrCard_GetRunState(&tmpRunState)){
			sRunStateVal.Format(_T("%#x"), tmpRunState);
		}

		UpdateData(FALSE);
	}
	CDialogEx::OnTimer(nIDEvent);
}


void CMFCDemoDlg::OnBnClickedBnXneg()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(0, -1);
}


void CMFCDemoDlg::OnBnClickedBnXpos()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(0, 1);
}


void CMFCDemoDlg::OnBnClickedBnYneg()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(1, -1);
}


void CMFCDemoDlg::OnBnClickedBnYpos()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(1, 1);
}


void CMFCDemoDlg::OnBnClickedBnZneg()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(2, -1);
}


void CMFCDemoDlg::OnBnClickedBnZpos()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(2, 1);
}


void CMFCDemoDlg::OnBnClickedBnTneg()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(3, -1);
}


void CMFCDemoDlg::OnBnClickedBnTpos()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_MCrlAxisMove(3, 1);
}


void CMFCDemoDlg::OnBnClickedBnHomex()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_SeekZero(0, 0.0, 0.0);
}


void CMFCDemoDlg::OnBnClickedBnHomey()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_SeekZero(1, 0.0, 0.0);
}


void CMFCDemoDlg::OnBnClickedBnHomez()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_SeekZero(2, 0.0, 0.0);
}


void CMFCDemoDlg::OnBnClickedBnHomet()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_SeekZero(3, 0.0, 0.0);
}


void CMFCDemoDlg::OnBnClickedBnHomeall()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_SeekZero(0xFF, 0.0, 0.0);
}


void CMFCDemoDlg::OnBnClickedBnHomecancel()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_CancelSeekZero(0xFF);
}


void CMFCDemoDlg::OnBnClickedBnHomey2()
{
	// TODO: 在此添加控件通知处理程序代码
	UpdateData(TRUE);
	TRACE("设置输出函数，Indx = %d, Val = %d \r\n", mOutputIndx, mOutVal);
	MoCtrCard_SetOutput(mOutputIndx, mOutVal);
}


void CMFCDemoDlg::OnBnClickedBnMdi()
{
	// TODO: 在此添加控件通知处理程序代码
	float tmpX[6] = {0};
	McCard_UINT8 axisEn[6];
	MoCtrCard_MCrlGroupAbsMove(axisEn, tmpX, 100.0);
}


void CMFCDemoDlg::OnBnClickedBnResetcord()
{
	// TODO: 在此添加控件通知处理程序代码
#if false
	MoCtrCard_ResetCoordinate(0xFF, 0.0);
#else
	McCard_UINT8 bAxisEn[4];
	McCard_FP32 tPos[4];
	McCard_FP32 tSpd[4];

	bAxisEn[0] = 1;
	bAxisEn[1] = 1;
	bAxisEn[2] = 0;
	bAxisEn[3] = 0;

	tPos[0] = 100.0;
	tPos[1] = 100.0;
	tPos[2] = 100.0;
	tPos[3] = 100.0;

	tSpd[0] = 10.0;
	tSpd[1] = 10.0;
	tSpd[2] = 10.0;
	tSpd[3] = 10.0;

	MoCtrCard_MCrlGroupRelMovePTP(bAxisEn, tPos, tSpd);
#endif
}


void CMFCDemoDlg::OnBnClickedBnQuitemove()
{
	// TODO: 在此添加控件通知处理程序代码
	MoCtrCard_QuiteMotionControl();


}


void CMFCDemoDlg::OnEnChangeEditPost2()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
}


void CMFCDemoDlg::OnEnChangeEditPost3()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	UpdateData(TRUE);
}
