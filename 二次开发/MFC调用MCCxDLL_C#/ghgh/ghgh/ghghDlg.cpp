
// ghghDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ghgh.h"
#include "ghghDlg.h"
#include "afxdialogex.h"

// 判断使用哪个DLL文件,DLL文件要放在Debug目录下
#using "..\debug\MCC4DLL.dll"

using namespace SerialPortLibrary;

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// 用于应用程序“关于”菜单项的 CAboutDlg 对话框
public ref class Globals abstract sealed {
public:
	static SerialPortLibrary::SPLibClass Mccard;
};


class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// 对话框数据
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// CghghDlg 对话框




CghghDlg::CghghDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CghghDlg::IDD, pParent)
	, mtest(0)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CghghDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CghghDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CghghDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CghghDlg::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CghghDlg::OnBnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &CghghDlg::OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &CghghDlg::OnBnClickedButton5)
	ON_BN_CLICKED(IDC_BUTTON6, &CghghDlg::OnBnClickedButton6)
	ON_BN_CLICKED(IDC_BUTTON7, &CghghDlg::OnBnClickedButton7)
	ON_BN_CLICKED(IDC_BUTTON8, &CghghDlg::OnBnClickedButton8)
	ON_BN_CLICKED(IDC_BUTTON9, &CghghDlg::OnBnClickedButton9)
END_MESSAGE_MAP()


// CghghDlg 消息处理程序

BOOL CghghDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 将“关于...”菜单项添加到系统菜单中。

	// IDM_ABOUTBOX 必须在系统命令范围内。
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// 设置此对话框的图标。当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

void CghghDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CghghDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CghghDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CghghDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa;
	//
	array<int, 1> ^tmpPos = gcnew array<int, 1>(4){1,2,3,4};		//定义托管类型变量，<类型，维数>

	aa = Globals::Mccard.MoCtrCard_Initial("COM3");
	if(0x02 == aa){
		MessageBox(_T("加载失败"));
	}
}


void CghghDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa;
	aa = Globals::Mccard.MoCtrCard_Unload();
}


void CghghDlg::OnBnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa;
	aa = Globals::Mccard.MoCtrCard_MCrlAxisAbsMove(0, 200, 50, 0.1);  
	if(0x02 == aa){
		MessageBox(_T("下发指令失败"));
	}
	// 下面是相对
	//aa = Globals::Mccard.MoCtrCard_MCrlAxisRelMove(0, 200, 50, 0.1);  
}


void CghghDlg::OnBnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa;
	aa = Globals::Mccard.MoCtrCard_MCrlAxisAbsMove(0, 0.0, 50, 0.1);  
	if(0x02 == aa){
		MessageBox(_T("下发指令失败"));
	}
}



void CghghDlg::OnBnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa;
	aa = Globals::Mccard.MoCtrCard_SeekZero(0); 
	if(0x02 == aa){
		MessageBox(_T("下发指令失败"));
	}
}


void CghghDlg::OnBnClickedButton6()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa = Globals::Mccard.MoCtrCard_CancelSeekZero(0xFF);
	if(0x02 == aa){
		MessageBox(_T("下发指令失败"));
	}
}


void CghghDlg::OnBnClickedButton7()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa = Globals::Mccard.MoCtrCard_QuiteMotionControl();
	if(0x02 == aa){
		MessageBox(_T("下发指令失败"));
	}
}


void CghghDlg::OnBnClickedButton8()
{
	// TODO: 在此添加控件通知处理程序代码
	// TODO: 在此添加控件通知处理程序代码
	int aa;
	array<float, 1> ^tmpPos1 = gcnew array<float, 1>(4){0.0,0.0,0.0,0.0};		//定义托管类型变量，<类型，维数>
	array<int, 1> ^tmpPos = gcnew array<int, 1>(4){1,2,3,4};		//定义托管类型变量，<类型，维数>
	//aa = Globals::Mccard.MoCtrCard_Test(tmpPos);
	
	aa = Globals::Mccard.MoCtrCard_GetAxisPos(0, tmpPos1);
	if(0x01 == aa){
		// TODO: 在此添加控件通知处理程序代码
		CEdit* pBoxOne;
		CString str;
		pBoxOne = (CEdit*) GetDlgItem(IDC_EDIT1);
		//pBoxOne->GetWindowText(str);
		str.Format(_T("%f"), tmpPos1[0]);
		pBoxOne->SetWindowText(str.GetBuffer(0));
		str.ReleaseBuffer();
	}
	else if(0x02 == aa){
		MessageBox(_T("下发指令失败"));
	}
}


void CghghDlg::OnBnClickedButton9()
{
	// TODO: 在此添加控件通知处理程序代码
	int aa;
	aa = Globals::Mccard.MoCtrCard_SendMDICommand(_T("G00X1000.0FX100.0AX0.5D0"));  
	if(0x02 == aa){
		MessageBox(_T("下发MDI指令失败"));
	}
}
