﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Diagnostics;

namespace csharp
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // 利用串口名称初始化运动控制卡
            if (0x01 == mMCcard.MoCtrCard_Initial("COM1"))
            {
                Debug.Print("初始化硬件成功 \r\n");
            }
            else
            {
                Debug.Print("初始化硬件失败 \r\n");
            }

            // 调用测试函数
            System.UInt16 tmpVal;
            System.Int32 []tmpInput = new System.Int32 [4];
            tmpVal = mMCcard.MoCtrCard_Test(tmpInput);

            // 输出测试函数
            Debug.Print("返回值={0}，输入参数返回值为={1}\t{2}", tmpVal, tmpInput[0], tmpInput[1]);

            mMCcard.MoCtrCard_Unload();
        }
     }
}
